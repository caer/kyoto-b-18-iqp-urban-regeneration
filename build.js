// Load dependencies.
const fs = require('fs');
const execSync = require('child_process').execSync;
const compress_images = require('compress-images'); 
const pdf_merge = require('easy-pdf-merge');

// Distribution folder.
const distFolder = "_site";

// Helper functions to iterate files by extensions.
function getFilesFromPath(path, extension) {
    let dir = fs.readdirSync(path);
    return dir.filter(elm => elm.match(new RegExp(`.*\.(${extension})`, 'ig')));
}

// Clear the distribution folder.
execSync("rm -rf " + distFolder + " && mkdir " + distFolder);

// Copy all web assets to the distribution folder.
execSync("cp -r src/* " + distFolder + "/");

// Remove the assets symlink from the distribution folder.
execSync("rm -rf " + distFolder + "/assets");

// Minify assets.
compress_images("assets/**/*.{jpg,png,JPG,PNG}", distFolder + "/assets/", 
    {compress_force: false, statistic: true, autoupdate: true},
    false,
    {jpg: {engine: 'mozjpeg', command: ['-quality', '60']}},
    {png: {engine: 'pngquant', command: ['--quality=20-50']}},
    {svg: {engine: 'svgo', command: '--multipass'}},
    {gif: {engine: 'gifsicle', command: ['--colors', '64', '--use-col=web']}}, 

    function(err, completed) {

        // Do not run if there was an error.
        if (err) {
            console.err("Image compression failed exceptionally: " + err);
            return;
        }

        // Do not run if compression has not finished.
        if (!completed) {
            return;
        }

        // Copy presentation and template data to the distribution directory.
        var exitCode = execSync("cp -r presentations/template " + distFolder + "/");
        exitCode = execSync("cp -r presentations/template ./");
        exitCode = execSync("cp -r presentations/presentation.md " + distFolder + "/");
    
        // Run backslide to generate the presentation.
        exitCode = execSync("bs export " + distFolder + "/presentation.md --output " +
                            distFolder + "/");

        // Cleanup build artifacts.
        exitCode = execSync("rm -rf " + distFolder + "/template");
        exitCode = execSync("rm -rf ./template");

        // Copy report data to the distribution directory.
        // We do not copy Markdown files (.md) because they
        // are processed and converted to PUG files (.pug).
        exitCode = execSync("cp -r report/*.css " + distFolder + "/");
        exitCode = execSync("cp -r report/*.scss " + distFolder + "/");
        exitCode = execSync("cp -r report/*.pug " + distFolder + "/");
        exitCode = execSync("cp -r report/*.pdf " + distFolder + "/");

        // Preprocess Markdown files to swap ![ALT-TEXT](...) for PUG figures.
        getFilesFromPath("./report", ".md").forEach(function(file) {

            // Strip file extension.
            file = file.split('.').slice(0, -1).join('.');

            // Read all lines from the markdown file.
            var markdownLines = fs.readFileSync("report/" + file + ".md")
                                  .toString()
                                  .split("\n");

            // Prepare a string that will represent the equivalent PUG markdown.
            var pugdown = ":markdown-it(html=true)\n";

            // Iterate over all lines from the markdown file.
            markdownLines.forEach(function(line) {

                // Remove trailing spaces.
                line = line.replace(/\s*$/,"");

                // Check to replace images with pretty images.
                if (/!\[(.*)\]\((.*)\)-full/gm.test(line)) {
                    pugdown += line.replace(/!\[(.*)\]\((.*)\)-full/gm, ".hero-img-container\n  <img src=\"$2\">\n  p\n    :markdown-it(html=true)\n      $1\n");
                    pugdown += ":markdown-it\n";
                } else if (/!\[(.*)\]\((.*)\)/gm.test(line)) {
                    pugdown += line.replace(/!\[(.*)\]\((.*)\)/gm, ".ui.segment.piled.figure\n  .image\n    img.full-width(src=\"$2\")\n  .figure-caption\n    :markdown-it(html=true)\n      <br>$1\n");
                    pugdown += ":markdown-it(html=true)\n";
                } else {
                    pugdown += ("  " + line + "\n");
                }	
            });

            // Replace all `---` with `—` (em-dash).
            // Ignores tables (start or end with |).
            pugdown = pugdown.replace(/(?<!\|)\h*---\h*(?!\|)/gm, "—");

            // Write the equivalent PUG content to the dist foler.
  		    fs.writeFileSync(distFolder + "/" + file + ".pug", pugdown);
        });

		// Run relaxed to build full report.
		exitCode = execSync("relaxed " + distFolder + "/report-template.pug --bo");

        // Merge proposal with title page to create merged PDF.
        pdf_merge([distFolder + '/report-front-matter.pdf', 
                   distFolder + '/report-template.pdf'], 
                  distFolder + '/urk-report-complete.pdf', function(err) {
      	    if (err) {
      		    return console.log(err);
      	    }
        });
    }
);