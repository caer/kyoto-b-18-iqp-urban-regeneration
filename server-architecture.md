# EFFURT Service Architecture

![](server-architecture-diagram.png)

This document outlines the service architecture for deploying the EFFURT application and database.

## The EFFURT Application

The EFFURT application is built entirely as static content for serving from a typical web (HTTP/S) server. We recommend hosting EFFURT on Netlify ([https://www.netlify.com](https://www.netlify.com)), a **free** web content delivery network that supports custom domains and automatic deployments from version control systems like GitLab (which hosts the EFFURT application code).

## The EFFURT Database

EFFURT uses Apache's CouchDB ([http://couchdb.apache.org/](http://couchdb.apache.org/)) to synchronize data between clients and provide a truly interactive experience. Deploying EFFURT without a database is possible, but users will not be able to easily share their updates with other users.

Because Netlify cannot host a database (only static content), we recommend installing and running CouchDB on the Google Cloud Platform via https://console.cloud.google.com/marketplace/details/bitnami-launchpad/couchdb. Monthly costs can be scaled based on resource usage, but low-traffic applications (which we see as the most typical) will cost less than $10/month to support.

## Domains

Both Netlify and the Google Cloud Platform can be aliased to custom domains. We recommend either [Google Domains](https://domains.google.com) (due to their ease-of-use) or [Netlify's Managed DNS](https://www.netlify.com/) (due to their tight integration with Netlify).