# Appendices

## Appendix A
### Experience Evaluation Framework

The Experience Evaluation Framework is a questionnaire that is designed to help us analyse and determine the attractiveness of an experience. The major pieces of information that we gathered from this questionnaire was what experience based aspects are commonly seen in experiences, what esthetics are commonly seen in experiences, and satisfaction with the experience. Since satisfaction with the experience can be biased, we collected the visitor’s opinions of the experience before and after to examine if there was any change in their attitude.

![Question 1](assets/experience form/experience1.png)

![Question 2](assets/experience form/experience2.png)

![Question 3](assets/experience form/experience3.png)

![Question 4](assets/experience form/experience4.png)

Describe the experience in 3 words.

![Question 5](assets/experience form/experience5.png)

![Question 6](assets/experience form/experience6.png)

What would you do to make this experience better?

Suggestions | Mentions
---|---
More English menu/signs/words/service | 7
More seating/space | 4
More gluten-free food/menu/vendors | 3
Cheaper food prices | 3
Go when less crowded | 3
Go during the happy hour | 2
More variety of food | 2

![Question 8](assets/experience form/experienceq8.png)

![Question 9](assets/experience form/experienceq7.png)

![Question 10](assets/experience form/experience7.png)

![Question 11](assets/experience form/experience8.png)

![Question 12](assets/experience form/experience9.png)

![Question 13](assets/experience form/experience10.png)

![Question 14](assets/experience form/experience11.png)

![Question 15](assets/experience form/experience12.png)

![Question 16](assets/experience form/experience13.png)

<div style="page-break-after: always;"></div>

## Appendix B
### Yokai Street Tourist Survey

In order to gain reliable information about visitor interest and travel methods, we created a Tourist Survey. This survey was tested upon English speaking tourists to gauge their interest in yokai pop-culture, Meiji-Era yokai, and Yokai branded Shotengai. The survey also asked tourists to explain why they came to Japan and Kyoto specifically and how they planned their trip. With this information we could create recommendations that align with their interests and figure out if Yokai Street’s online presence was attracting tourists.

![Question 1](assets/tourist survey/tourist1.png)

Why did you decide to visit Kyoto?

Reasons | Mentions
---|---
Old city/history | 5
Culture | 3
Tradition | 3
Fall foliage/tress | 3
Sightseeing | 3
Part of a travel plan | 3
Temples | 2
World Heritage Sites | 2

![Question 3](assets/tourist survey/tourist3.png)

![Question 4](assets/tourist survey/tourist4.png)

![Question 5](assets/tourist survey/tourist5.png)

![Question 6](assets/tourist survey/tourist6.png)

![Question 7](assets/tourist survey/tourist7.png)

![Question 8](assets/tourist survey/tourist8.png)

<div style="page-break-after: always;"></div>

## Appendix C
### Tourist Map User Testing

In order to help us create a decent tourist map, we conducted preliminary user testing for the two different drafts of the tourist map that we created for Yokai Street. This testing was conducted upon English speaking tourists to see if they can navigate through Yokai Street using each draft and afterwards getting their feedback on which of the drafts they think is better and clearer, and which parts of each draft they would keep the same or improve. This user testing was very helpful in creating the final tourist map for Yokai Street.

![Question 1](assets/tourist map/map1.png)

![Question 2](assets/tourist map/map2.png)

![Question 3](assets/tourist map/map3.png)

![Question 4](assets/tourist map/map4.png)

![Question 5](assets/tourist map/map5.png)

![Question 6](assets/tourist map/map6.png)

![Question 7](assets/tourist map/map7.png)

![Question 8](assets/tourist map/map8.png)

![Question 9](assets/tourist map/map9.png)

![Question 10](assets/tourist map/map10.png)

![Question 11](assets/tourist map/map11.png)

![Question 12](assets/tourist map/map12.png)

![Question 13](assets/tourist map/map13.png)

![Question 14](assets/tourist map/map14.png)

![Question 15](assets/tourist map/map15.png)

![Question 16](assets/tourist map/map16.png)

![Question 17](assets/tourist map/map17.png)

![Question 18](assets/tourist map/map18.png)

![Question 19](assets/tourist map/map19.png)

![Question 20](assets/tourist map/map20.png)

![Question 21](assets/tourist map/map21.png)

<div style="page-break-after: always;"></div>

## Appendix D
### QR codes

These QR codes contain links to both sample designs that we created within Zazzle's Customization feature. The first QR code is linked to the sample design for Yokai Magnets and the second QR code is linked to the sample design for Yokai Candy Tins.

![Yokai Magnet](assets/Zazzle-Sticker.png)

![Yokai Candy Tins](assets/ZazzleTin.png)