# Understanding Tourists

When building experiences, it is vital to understand who will be consuming the experience and what they want. The main target of improving experiences will be tourists. Therefore it will be instrumental to gain an understanding of the tourists themselves: What motivates tourists? How are tourists being drawn to tourist locations? What could be done to draw tourists to Yokai Street?
In this section we will cover the following topics:
	
- The typical motivations of tourists visiting the Kyoto area
- How Yokai Street is being advertised to tourists
- Tensions between tourists and residents of Kyoto
- Tourism potential of Yokai Street

## Methods for Gauging Tourist Opinions

In order to understand the tourist market that Yokai Street will be advertised to, we created a survey to administer to tourists visiting Kyoto. The survey (see Appendix A), is intended to ascertain the following information:

- Reason for visiting Japan
- Reason for visiting Kyoto
- Trip planning resources utilized
- If they have plans to visit to Northwest Kyoto (Kinkakuji, Kitano-Tenmangu Shrine)
- Interest in Yokai influenced pop culture franchises
- Interest in Yokai
- Interest in visiting yokai street
- Expectations for visiting yokai street

The anonymous survey was administered to willing foreign tourist participants in the Kyoto Station area. Only foreign tourists were surveyed due to the our team’s lack of proficiency with Japanese language, which prevented us from effectively communicating with domestic tourists. Survey results from 30 participants were collected in total.

## Tourist Motivations for Visiting Japan

The most popular reason for visiting Japan was tourism, which was cited by 23 respondents, as seen in [FIG]. Other reasons included work-related reasons (1 response), study-related reasons (3 responses), honeymoon (2 reponses), family visit (1 response), and culture (1 response). The significant amount of foreigners visiting for purely tourism reasons is unsurprising, and demonstrates that there is a significantly sized potential market for Yokai Street tourism.

![Pie chart depicting why people are visiting Japan.](assets/Japan_visit_piechart.png)

Reasons for visiting Kyoto were more diverse than reasons for coming to Japan. The most popular response was culture tourism (14 responses), as seen in [FIG], which was followed by sightseeing (8 responses), history (6 reponses), autumn foliage (3 responses), part of a recommended travel plan (3 responses), family visit (2 responses), study-related reasons (2 responses), enjoyed a previous visit to Kyoto (2 responses), and work-related reasons (1 response).

![Pie chart depicting why people are visiting Kyoto.](assets/Kyoto_visit_piechart.png)

The top three reasons cited for visiting Kyoto- culture tourism, sightseeing, and history could all be applied to Yokai Street as a tourist destination. Yokai Street as a themed shotengai incorporates both local Japanese shopping culture and Japanese yokai culture. The handcrafted yokai statues have been praised as quirky and unique by internet reviews and blog posts about visits to Yokai Street, incorporating a unique sightseeing element to visiting Yokai Street. In addition, the street has an interesting history as the site of ancient Yokai myths and as the once bustling transportation center of Northwest Kyoto. The local shrines and temples also have their own unique history, contributing to the potentially rich historical offerings of the street. 

## Availability of Information about Yokai Street in Trip Planning Resources

In order for tourists to visit Yokai Street, they need to be able to learn about Yokai Street from the resources they are utilizing to plan their trip. To find out if information about Yokai Street is available in well-known trip planning resources, we performed a survey of travel websites to find how many advertised information about Yokai Street.

Travel sites surveyed were found through searches on both Google and DuckDuckGo, to account for any bias search engines may have from previous searches. The keywords “Things to do in Kyoto” were used, and only the first page of search results were utilized. Websites found were cross-referenced against lists of top travel websites from Alexa and SimilarWeb to put emphasis on websites with the most traffic. We identified 14 websites that are likely to be used by tourists planning a trip to Kyoto, and found that only 4 of them offered specific information about Yokai Street, as seen in [FIG].

![Dot matrix depicting the proportion of online sites that have information about Yokai Street.](assets/online-dot-matrix.png)

Using similar methods, we also did a survey of Kyoto-specific travel and news sites to find out if Kyoto based websites were more likely to have information in Yokai Street than general travel information sites. Keywords “Kyoto news”, “Kyoto tourism”, and “Kyoto tourist information” were used on both Google and DuckDuckGo to identify websites that had Kyoto focused news and travel information. In total, seven such websites were identified and only one of them contained any mention of Yokai Street, as seen in [FIG]. 

![Dot matrix depicting the proportion of Kyoto-specific news and travel sources that have information about Yokai Street.](assets/local-dot-matrix.png)

Based on this research, it seems that Kyoto specific websites are in fact, less likely to contain information about Yokai Street than general travel websites. This may occur because Kyoto is known as the cultural capital of Japan, and websites with information about Kyoto are more focused on traditional Kyoto culture sights, such as Kyoto’s 17 UNESCO World Heritage Sites, rather than small local shotengai that are lesser known attractions. 

Finally, we wanted to determine what trip planning resources Kyoto tourists were actually utilizing. Question 3 of the tourist survey questionnaire [APPENDIX] asked participants if they used any tourists planning resources in the list provided and also had a write-in space for other answers. The list provided contained several of the most prominent travel websites found in our internet presence survey, several travel websites that contained information on Yokai Street, and several of the most popular travel guidebooks for Japan. 

![Planning resources utilized by Kyoto Tourists](assets/planning_resources_piechart.png)


As seen in FIG, TripAdvisor and Lonely Planet Guidebooks were the most widely used resources, with 15 responses and 13 responses, respectively. Neither of these resources have specific information available about Yokai Street. Atlas Obscura was the only website with information about Yokai Street that was utilized by the tourists surveyed, with one response. The rest of respondents used other websites and resources that either do not have information on Yokai Street, or are unlikely to have information on Yokai Street. Based upon these results, it is likely that the vast majority of foreign tourists visiting Kyoto are not using travel planning resources that have available information on Yokai Street, and therefore do not know about Yokai Street. 

## Tourism Pollution

Yokai Street is currently not a hugely popular tourist destination, however Japan’s popularity as a tourist destination is growing and Kyoto is one of the most popular tourist cities in Japan. In 2003, approximately 5 million tourists visited Japan. In response to low numbers of foreign tourists visiting, the government launched a widely successful campaign called "Visit Japan" to boost tourism. As of August 2018, the number of foreign tourists that have visited Japan in 2018 has exceeded 20 million and is projected to go over the 30 million barrier by the end of the year, beating last year’s record of 28.7 million foreign tourists (The Telegraph, 2018). The number of foreign tourists visiting annually is expected to rise even higher in anticipation of the Tokyo 2020 Olympics and is projected to reach 40 million by 2020 (Ryall, 2018). 

The growing number of foreign tourists visiting Japan has resulted in tourism pollution. Tourism pollution or “kankō kōgai,” is a situation where a city or parts of a city become influxed with tourists, valued property and monuments become corrupted by tourists, and various hardships are imposed on residents and businesses due to the overflow of tourists (The Invisible Tourist, 2018). Yokai Street has mostly avoided effects of tourism pollution so far due to lack of foreign visitors, however reports of tourism pollution in other parts of Kyoto may leave Yokai Street’s residents reluctant to open their home to more tourists.

In ancient capitals of Japan such as Kyoto, locals are increasingly expressing their unenthusiasm and frustration toward the surges in tourists and are anxiously complaining to the authorities regarding the inconvenience of overcrowded public transport along the routes of famous attractions, loud and disrespectful foreigners, and poor mannerism among visitors (Ryall, 2018). A survey conducted in 2016 reported that 35 percent of shops, restaurants and accommodation facilities are not willing to offer multilingual services to foreign tourists due to multiple reasons such as language difficulties, limited resources, and poor etiquette among customers (The Japan Times, 2016). Many shops along Yokai Street do not currently offer foreign language accessibility, and poor impressions of foreign tourists may create reluctance to change.

Reports of disrespectful foreign tourists are numerous. Tourists have tried to take selfies with maikos (trainee geisha) on their way to their job, blocking their path and leaving some of them in tears. Inconsiderate visitors have carved names into trees at the bamboo forest of Arashiyama, a UNESCO world heritage site. Restaurant owners have also complained about reservations cancelled without considerate warning, or of people coming in to take pictures and then leave without buying anything (McCurry, 2018).

Another ramification of the tourism pollution is illegal accommodation. The daily lives of Kyoto residents are being significantly influenced by the surges in accommodations caused by the tourism boom. Tenants have been pushed out of their leased properties by real estate agencies in plots to transform them into travel accommodations. Some landlords are even renting out unlicensed properties. There have been many incidents where tourists entered private houses, mistaking them for vacation rentals and leaving the residents in terror (The Kyoto Shimbun, 2018). These incidents have created tension between many Kyoto residents and visiting tourists, and may contribute to negative perceptions of foreign tourists in the surrounding neighborhood of Yokai Street. 

While Yokai Street has not yet experienced tourism pollution, it is important to consider tourism pollution in other parts of Kyoto for several reasons. Rising tensions between Kyoto residents and foreign tourists may become an issue along Yokai Street in the future if Yokai Street achieves more success in attracting tourists to the shotengai. Therefore, it is vital to consider the effect on residents when making recommendations to improve Yokai Street.

In addition, many popular tourist areas in Kyoto have become very crowded and congested due to the increasing amounts of tourists visiting Japan. The quiet atmosphere of Yokai Street can be marketed as a selling point of Yokai Street, targeted at tourists who are tired of constant crowds at Kyoto’s more popular tourist spots.

## Government Efforts to Improve Tourism Pollution

The Kyoto Tourism Bureau has been working on policies intended to reduce friction between foreign tourists and residents. The concepts behind these policies can be applied to Yokai Street to accomplish the same goals on a smaller scale.

A partnership between the Kyoto Tourism Bureau and TripAdvisor has produced a pamphlet to educate visitors on social taboos, and the Bureau is encouraging events to be scheduled in the morning or evening to reduce congestion. In addition, the bureau also hopes to reduce congestion through promoting travel to Kyoto during non-peak months such as January, May, and June. Through these initiatives, officials from the tourism bureau hope to achieve harmony between tourists and local residents in order to continue promoting Kyoto as a tourism destination without ignoring the well-being of residents (Ryall, 2017). As a street with less traffic, Yokai Street would be able to contribute to decreasing congestion in busy tourist areas by drawing tourists away from congested spots.

In addition, experts have suggested that increasing the amount of tourist information centers, improving multilingual signage, and providing more information on lodging accommodations will improve infrastructure for tourists. Workers that are properly trained to assist foreign visitors and have good language abilities would also be invaluable to assisting tourists in Kyoto (The Japan Times, 2016). Yokai Street is currently not very welcoming or accessible to foreign tourists due to lack of foreign language signage and lack of English speaking ability. More accessible signage would go a long way towards making the street more welcoming and inviting to foreign tourists.

## Tourism Potential of Yokai Street

Over the past few decades, Japanese culture exports have gained a large following internationally. Nearly every American child in the past 30 years have been influenced by Hello Kitty, Pokemon, Power Rangers, or Nintendo (Hoskin, 2015). According to a survey conducted by the Japanese government, Japanese cuisine is the most popular in the world (Inada, 2017). Schools dedicated to traditional Japanese arts such as flower arranging and tea ceremonies have spread over the globe to locations such as South Africa and Nova Scotia, and the Japanese film “Spirited Away”, which won the 2003 Oscar for best animated feature, is now a common household name (Faiola, 2003).

Japanese culture is cool, and Japan’s culture has been influencing its rise to prominence since the 1990’s (McGray, 2001). Even the Japanese Government has recognized this, and has created an entire department dedicated to the promotion of “Cool Japan” (Amano, 2010, “Promoting ‘Cool Japan’”, 2010, Inada, 2017). 

Yokai Street has a lot of potential for cool. The “Cool Japan” TV show has previously aired episodes on both yokai and shotengai, and while the show doesn’t always hit the mark on what foreigners consider to be cool about Japan, our research indicates that Yokai have a lot of potential marketability.

Yokai folklore is widely popular in Japan. They were most famously popularized by Shigeru Mizuki’s 1959 manga Gegege no Kitaro, which was adapted into an anime show in 1968, which is continued to be released today (Shamoon, 2013). While Gegege no Kitaro is only popular among domestic tourists, there are also many internationally popular pop culture franchises that draw influence from Yokai myths. These include Pokemon, Yokai Watch, and Hayao Miyazaki’s famous film, Spirited Away (Boyd, 2015). 

Yokai Street is easily marketable to fans of supernaturally inspired animes and video games. A folklore professor at Indiana State University says many students studying Japanese literature and folklore become interested in the subject through manga and anime (Ito, 2015). The same concept may apply to tourists, who become interested in Japan through pop culture.

Questions 5, 6, and 7 of the tourist survey [APPENDIX] were intended to gauge tourists’ interest in yokai inspired pop culture, yokai culture, and visiting a yokai themed shotengai. When asked to indicate interest in a list of yokai inspired pop culture exports, a majority of respondents (27 out of 30) said they were interested in at least one of the animes listed, as seen in FIG.

![Tourist interest in pop culture](assets/pop_culture_interest.png)

While interest in yokai inspired pop culture among respondents did not always translate to an interest in yokai, the majority of respondents, 18 out of 30 said they had in interest in Japanese spirits and ghosts, and there was a direct correlation between those who said they were interested in at least one of the pop culture franchises listed and those who said they were interested in Japanese spirits and ghost, demonstrating that Yokai have a lot of potential among fans of yokai inspired pop culture.

In addition, every respondent that said they were interested in Japanese spirits and ghosts said they would be interested in visiting a yokai themed shotengai located near one of their already planned destinations. Overall, 21 out of 30 respondents said they would be interested in visiting such a shotengai, demonstrating there is a clear potential market for Yokai Street among foreign tourists. 
