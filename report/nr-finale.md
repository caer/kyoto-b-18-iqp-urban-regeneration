# A New Northwestern Kyoto Tourism Plan

In the previous chapters, we learned that Yokai Street has a powerful brand and ample cultural and infrastructural assets and opportunities ripe for the taking. However, a majority of these recommendations are Yokai-specific; they look *inward* at what Yokai Street can do to achieve regeneration on its own, instead of *outward* at how Yokai Street could achieve regeneration as part of a greater plan across the Northwestern Kyoto region.

It is our conclusion that, on its own, Yokai Street is unlikely to achieve its tourism goals through *only* the Yokai-specific recommendations in this document. Collaboration with outside organizations was a central theme in the thirty different shotengai regeneration case studies we reviewed --- nearly every one of these regeneration case studies stressed the importance of collaboration with community organizations, student organizations, non-profit organizations, neighboring shotengai, or the local government.

The city of Kyoto has proposed a "Northern Kyoto Tourism Plan" that promises to bring more tourists to areas of northern Kyoto that are often overlooked by visiting tourists. However, this plan is anchored on the Kitano-Tenmangu Shrine. This is a problem if Yokai Street's goal is to participate in a plan in that will capitalize on the roughly 28 million foreign visitors to Japan (Japan Tourism Statistics, 2018) --- our research has shown that Kitano-Tenmangu Shrine is primarily a destination for *domestic* tourism, and is not particularly well-known by *foreign* tourists.

Therefore, we believe a new Northwestern Kyoto Tourism plan is needed. This tourism plan would include Yokai Street and other significant domestic *and* foreign tourism draws like Kamishichiken, Kinkaku-Ji, Ryoan-Ji, Kitano-Tenmangu Shrine, and Daishogun Hachijinja Shrine. 

In this final chapter of our report, we discuss this new Northwestern Kyoto Tourism and how it could be formed around a single statement:

> Visit Heian-Kyo, the Original Kyoto

## The Need for a New Northwestern Kyoto Tourism Plan

A Northwestern Kyoto tourism plan would bring more visibility to historical assets such as Yokai Street, improve accessibility of Yokai Street and other assets to foreign tourists, and reduce tourism pollution in other regions of Kyoto. 

![Fig. 5.1: Bus routes from Kyoto Station to Kinkakuji Temple.](assets/kinkakuji-bus-routes.png)

For tourists staying in other regions of Kyoto, it can take a lot of time to travel to Kinkakuji. Traveling from Kyoto Station to Kinkakuji via public transportation can take anywhere from 35-50 minutes in one direction, as seen in Fig. 5.1. It would be advantageous for tourists to explore other areas of Northwestern Kyoto when they’re already traveling far to see Kinkakuji. Most tourists, however, are not taking advantage of this opportunity. 

A “Hidden Northern Kyoto” tourism plan will educate tourists on hidden historical assets in the Northwestern Kyoto area and encourage tourists to visit less crowded areas such as Kitano-Tenmangu Shrine and Yokai Street. 

### Underwhelming Popularity of Northern Kyoto's Historical Assets

Assets in Northwestern Kyoto are very underutilized, despite their beauty and strong history.  Due to this underutilization, certain historical assets in Northwestern Kyoto lack visibility and therefore also lack accessibility to tourists. Yokai Street gains very little traffic from tourists. Daishogun Hachijinja Shrine and Kamishichiken also gets very little traffic from tourists, and Kitano-Tenmangu Shrine is only popular among domestic tourists. 

In addition, the current plan to revitalize tourism in the Kitano region of Northwestern Kyoto is insufficient. According to our sponsor, Benoit Jacquet, the local government's plan to promote tourism in the Kitano region is centered around Kitano-Tenmangu as a central anchor, which makes complete sense from a Japanese perspective. Kitano-Tenmangu is very popular with domestic tourists, and is visited by students from all over the country who come to pray for academic success.

The drawback of this plan is that it ignores the thousands of foreign tourists that visit Kyoto every year. While Kitano-Tenmangu is a great asset and definatly has a place in a Northwestern Kyoto tourism plan, it is not an effective anchor point for foreign tourists, as demonstrated by the underwhelming popularity of Kitano area tourism assets. 

![Fig. 5.2: Demographics of foreign tourists entering Japan in 2017 (Japan Tourism Statistics, 2018).](assets/tourist-demographics.png)

The foreign tourist demographic should not be overlooked. As shown in Fig. 5.2, 28 million foreign tourists visited Japan in 2017. Approximately 75% of these tourists were from East Asian countries and 11% were from Western cultures (Japan Tourism Statistics, 2018). By failing to market to foreign tourists, Northwestern Kyoto is ignoring millions of potential visitors. A demographic this large cannot be overlooked.

Northwestern Kyoto tourism plan needs to cater to the foreign tourist demographic as well as the domestic tourist demographic. One way to do this is to emphasize the **history** of Northwestern Kyoto. As detailed in our chapter *"The Aura of Yokai Street"*, Northwestern Kyoto is the entrance to **Heian-kyo**, the old capital of Japan. The area is rich with historical assets, including Yokai Street, Daishogun Hachi Jinja Shrine, Kitano Tenmangu Shrine, and Kamishichiken. As indicated by our research on experiences, tourists seek out experiences. Kitano-Tenmangu by itself is not an experience, however it could be an experience if combined with other historical assets in the area as a rediscovery of old Kyoto.

<div style="page-break-after: always;"></div>

### Low Accessibility of Historical Assets

As mentioned in our previous chapter *"The Aura of Yokai Street"*, accessibility of certain historical assets in Northwestern Kyoto is low. This is, in part, due to the low visibility of the area. In addition, many of them do not have much foreign language accessibility, such as the Daishogun Hachi Jinja Shrine museum. Many restaurants in the area, including restaurants along Yokai Street, also have low accessibility to foreigners, which decreases the likelihood that a tourist will stay and consume Northwestern Kyoto for an extended period of time.

Outside Restaurant Inoue, which is the most popular restaurant in the shop association according to the Taishogun website (“Restaurant Inoue”, 2018), the majority of advertised menu items are completely in Japanese, and completely inaccessible to foreigners as seen in Fig. 5.3. Foreign tourists may be discouraged to linger in Northwestern Kyoto due to the lack of infrastructure available to support foreign language accessibility.

![Fig. 5.3: Signs in the window of Restaurant Inoue are almost completely in Japanese ("Restaurant Inoue", 2018).](assets/inoue-storefront.png)

<div style="page-break-after: always;"></div>

### A Countermeasure to Tourism Pollution

A new Northwestern tourism plan will encourage tourists to visit less popular areas of the city, relieving tourism pollution and improving experiences for tourists. As discussed in the introduction, tourism pollution is an issue that has been plaguing Kyoto City in recent years. Popular tourist attractions such as Kinkakuji, Gion, and Nishiki Market have become very congested with tourists. Excessive congestion damages the atmosphere of a location and reduces the pleasure of the experience for tourists. The Kyoto Tourism Bureau is attempting to encourage dispersion of tourists to less popular areas to relieve congestion. A Northwestern Kyoto tourism plan can help serve as part of this solution.

### Current Tourism Paths in Northwestern Kyoto

Tourists traveling to Northwestern Kyoto skip the over the area containing Kitano-Tenmangu Shrine and Yokai Street. Instead, they visit Kinkakuji, Ryoanji, and Ninnaji Temples, which are located more than 1 kilometer north-northwest of Yokai Street. These 3 landmarks are protected UNESCO World Heritage Sites and are located within a 15-minute walk from each other, connected by Kinukake-no-michi Road. 

Tourists are unlikely to stray from this path because the current tourism infrastructure is designed to keep tourists along this path. The promotion association of Kinukake-no-michi Road has a tourism plan designed for tourists to promote the 3 World Heritage Sites and the shops and restaurants between them. They have an English accessible website (<https://kinukake.com/en>) with information for tourists, including a map of the area as seen in Fig. 5.4. The map displays the location of local landmarks, transportation, local shops and restaurants, toilets, and convenience stores, as well as anecdotal information about notable locations. Kitano-Tenmangu Shrine is located on the edge of the map, while Yokai Street is cut off. Due to the tourism infrastructure built up around Kinkakuji, Ryoanji, and Ninnaji, tourists will need to be incentivized to stray from this path and visit the area surrounding Yokai Street and Kitano-Tenmangu Shrine. 

![Fig. 5.4: English accessible version of the Kinukake-no-michi Road area (Kinukake-no-michi Road Association, 2018).](assets/kinukake-road-area.png)

### Yokai Street’s Current Tourism Plan

The shop owners association already has infrastructure in place to create a Hidden Kyoto tourism plan. However, it needs to be heavily modified to be accessible to foreign tourists. Their brochure, as seen in Fig. 5.5, shows recommended historical assets in Northern Kyoto, however, it is completely in Japanese.

![Fig. 5.5: Picture of current Yokai tourism plan](assets/current-yokai-street-pamphlet.jpg)

Translations of the 12 locations highlighted are as follows:

1. Jobon-Rendai-ji Temple
2. Senbon Rimakado Shogoji Temple
3. Syakuzō-ji Temple (Kuginuki-jizo)
4. Shonenji Temple (Cat Temple)
5. Iwakami Shrine
6. Seimei-Jinja Shrine
7. Ichijo Modoribashi (Ichijo Bridge)
8. Kitano Tenmangu Shrine
9. Daishogun Hachi Jinja Shrine
10. Ryuhonji
11. En-no Matsubara
12. Ichijo-dori (Yokai Street)

The current brochure establishes a strong foundation to build on, with excellent visual appeal and a good focus on Yokai Street. However, we see a wide range of opportunities to improve this brochure for the benefit of foreign tourists, with some of our preliminary recommendations including:

1. Add English and Chinese language support; the current map is only in Japanese.
2. Add pictures and short anecdotes for each location, similar to those found in the "Hidden Gems of Kyoto" booklet, which is more extensively discussed in our chapter *"The Aura of Yokai Street."*
3. Add information about urban assets surrounding yokai Street, similar to those found in the Kinkake map in Fig. 5.4. These could include convenience stores, public restrooms, transportation, foreign-friendly restaurants, and shops nearby Yokai Street.

If the current Yokai Street map were modified to incorporate these recommendations, it would be off to an incredibly strong start in implementing a Hidden Northern Kyoto Tourism plan.

## A Hidden Northern Kyoto Guidebook

We recommend creating a *"Hidden Northern Kyoto: Visit Heian-Kyo, the Original Kyoto"* guidebook similar to the "Hidden Gems of Kyoto" pamphlet we discussed in our chapter *"The Aura of Yokai Street"*. This guidebook would market market historical assets in Northwestern Kyoto as a rediscovery of the original **Heian-Kyo** capital, building upon Northwestern Kyoto's history as the entrance to Heian-Kyo.

The Hidden Northern Kyoto guidebook should contain the following information, inspired by the “Hidden Gems of Kyoto” booklet and the map of the Kinukake road area.

- Pictures of each historical asset
- Historical significance and anecdotes about each location
- Specific activities a tourist can do at these locations
- Hours, admission fees, and bus stop information about each location
- A map of the general area, displaying locations of all assets, along with other urban assets such as convenience stores, bus stops, train lines, and public toilets

The pictures, historical anecdotes, and activities will entice tourists to visit these lesser-known tourist attractions, while the hours, admission fees, and transportation information will make them accessible to tourists. 

<div style="page-break-after: always;"></div>

### Key Historical Assets

In addition to Yokai Street, we recommend utilizing the following historical assets (some of which are covered in more detail in our chapter *"The Aura of Yokai Street"*) of Northwestern Kyoto to produce this tourism booklet:

1. **Daishogun Hachi Jinja Shrine**: This historic shrine has remained in the same location at the entrance to Heian-kyo since the Heian period. Dedicated to the ancient god of stars and direction, it was revered by ancient Daishogun and powerful members of the court. Historic artifacts from the Heian period are preserved in the shrine's museum.

2. **Kitano Tenmangu Shrine**: This shrine was built in the Heian period to appease the angry spirit of the scholar Sugawara no Michizane. Today, it is revered by students across the country who flock to it to pray for academic success. It is also known for its monthly flea market, and beautiful plum blossoms in the spring.

3. **Kamishichiken**: Located immediately outside Kitano Tenmangu Shrine, Kamishichiken is the oldest of Kyoto's licensed *Geisha* districts. This district dates back to the Muromachi era and is quieter and less crowded than Kyoto's most popular *Geisha* district, Gion. Kamishichiken can be marketed as a less crowded, more historic version of Gion.

4. **Hirano-Jinja Shrine**: This shrine is located north of Yokai Street on Nishioji dori. It was moved from its original location in Nara to Heian-kyo when the new capital was founded in 794. During the Heian period, the shrine had a close relationship with the Imperial family and is now known for its beautiful cherry trees ("Hirano Shrine", 2018).

5. **Kuginuki Jizo Shakuzoji Temple**: Located north of Imadegawa dori, this temple is dedicated to pain, or rather the alleviation of pain. It is popular with the elderly who come to pray for their ailments. The peaceful temple grounds are also popular with mothers and children as an escape from the city ("Kuginuki Jizo Shakuzoji Temple", 2018).

6. **Bonus - Nekodera (Shonenji Temple)**: While this temple is newer than the previous assets and does not completely fit the hitoric Heian-kyo narrative, its fascinating history and novel dedication to cats is likely to be a hit with many tourists. Located Northeast of Kitano-Tenmanhu, Shonenji temple was dedicated to the spirit of the 3rd head priest's cat, which as legend goes, came to the priest in a dream and told him how to save the temple from financial ruin. Today, the temple holds memorial services for deceased pets. Pet owners can visit this temple to pray for the well-being of their pets, as well as buy charms for long healthy lives of their pets ("Shonenji Temple", 2015).

Additional landmarks in the area, as well as landmarks listed in the current yokai tourism pamphlet as presented earlier may also be included, as seen fit.

## Historic Heian-Kyo: The Experience

This new tourism plan will create an **educational** and **esthetic** experience for tourists in Kyoto. The Historic Heian-Kyo experience will compliment experiences already consumed by tourists throughout the city. An example of one of these parallel experiences is as an educational audio tour of Nijo Castle about the esthetics and architecture of Tokugawa era palaces. As indicated by the existance of tours such as this one and our research on tourists visiting Kyoto as discussed in our chapter *"The Aura of Yokai Street"*, a market for these types of experiences clearly exist. The Historic Heian-Kyo tourism path will fit neatly into the market for consumption of historic Japanese culture.

The Heian-Kyo experience will also diversify the historical culture experiences offered in Kyoto, because one does not currently exist in the Kitano region of northwestern Kyoto. An experience that ties together historical assets located near the northwestern gate of historic Heian-Kyo can function as a refreshing new offering to the region. In addition, many of the historical culture experiences in Kyoto such as the Nijo Castle audio tour are one-off experiences that does not link the history and culture of the attraction to its surroundings. These experiences can become tedious when visiting in sequence because of their similarity in format. The tourist begins to tire of this discreet formula of visiting one attraction, touring it for an hour, checking it off on their list, and moving onto the next attraction. 

Our proposed tourist plan will provide a fresh take on these well-worn historical culture experiences found all over Kyoto. Instead of visiting one location, paying admission, and following a path marked by the procession of tourist crowds, the tourist is given the tools to discover a hidden path of historical assets that tell a story---the story of Heian-Kyo and northwestern Kyoto over the eras. Through this new experience, the tourist is unchained from crowds, free to explore at their own pace, and given the opportunity to discover the hidden restaurants, cafes, and souvenir shops of Yokai Street along the way. 

## Coming Together to Work Together

The Hidden Northern Kyoto experience draws from all of our research and recommendations to create a path forward for Yokai Street in the context of the Northwestern Kyoto Area. Our goal was to provide recommendations for urban regeneration within Yokai Street by focusing on tourism and experiences; however, to do this we had to look outward to the rest of Kyoto.

Historical and cultural experiences are abundant in Kyoto. The examples mentioned in this report---Gion, Nishiki Market, Kikaku-Ji, Nijo Castle, Daishogun Hachijinja Shrine, Kitano Tenmangu Shrine---only scratch the surface of the assets available within Kyoto, let alone Japan. We leveraged these assets to inform our recommendations for improving Yokai Street.

Equally as abundant, though, are Yokai Street's assets: From infrastructure to branding, and historical anecdotes to visual esthetics, Yokai Street has an incredible strong foundating to build upon to create a single, cohesive Yokai Street experience for visitors. Our proposed larger tourism plan, *"Hidden Northern Kyoto: Visit Heian-Kyo, the Original Kyoto,"* will leverage Yokai Street as a key asset among a community of assets in the greater Northwestern Kyoto Area. Through collaboration, we believe Yokai Street's Aura and experience will be transferred to tourists from across the world.

We assert change happens through small steps taken towards a greater goal over many years. We believe that, in time, and through the recommendations in this report, Yokai Street will realize its goals of urban regeneration and improved tourism.