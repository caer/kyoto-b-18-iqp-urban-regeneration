# Mapping

The applications of maps extend beyond their ability to represent the criss-crossing roads of cities: Maps can display points of interest, convey geospatial statistics, and even depict the relationships between objects. Their diverse utility makes maps an ideal medium for simultaneously visualizing the current state of Yokai Street and creating promotional materials to increase the number of tourists that visit the street.

In this section, we will be reviewing mapping and geographic information system concepts before diving into our appraoch to mapping Yokai Street and our findings. This will include:

- Examples of Data Visualization with Maps

- The Electronic Field Form for Urban RevitalizaTion (EFFURT).

- EFFURT Results and Analysis

## Examples of Data Visualization with Maps

### Charles Booth’s Poverty Maps

![Charles Booth’s map of London depicting lower-income (black) and higher-income (red) areas within the city. (London School of Economics and Political Science, 2018)](assets/lseps-2018.jpg)

Between 1889 and 1902, a shipping magnate named Charles Booth released a series of maps that visualized poverty within the city of London [FIG]. This seemingly objective “bird’s eye” view of poverty was disruptive to the contemporary visual rhetoric of the time; a rhetoric which emphasized grotesque images of the “monsters” and horrors that plagued the lower class of the time. While this map was not without faults in its clarity - particularly in regards to the consistency and ambiguity of the color scales used to differentiate middle-class from lower and upper-class income - it is emblematic of how maps can be used to disrupt cultural visual rhetoric and visualize complex problems. (Kimball, 2006)

### The London Underground Map

![A map of the London Underground and Overground depicting the relationships between different stations. (Wikimedia Commons, 2014)](assets/wikimedia-commons-2014.png)

One of the most famous non-geographic maps is the map of the London Underground [FIG]. This map traces its roots to 1926, when map-maker Fred Stingemore created the first map of the London Underground that didn’t conform to London’s true shape (Graham-Smith, 2016). By ignoring geographic accuracy, this map was able to accurately depict the relationships between stations in a condensed and easily distributable format. This map is another perfect example of how maps can be used to transform complex problems into digestible images.

### An Interactive Map

![An interactive map displaying information about points of interest within the fictional world of “Game of Thrones.” (Treist, 2017)](assets/treist-2017.jpg)

With the widespread adoption of the internet and a myriad of web-based technologies, interactive maps that contain clickable elements and rich content have become more common. One such example of these interactive maps is a map of the fictional world of “Game of Thrones” [FIG] which allows users to click on various elements of the map to learn more about their history and significance.

### A Multi-Modal Map

![A mixed-data map that displays data about restaurants (blue circles), liquor establishments (red circles), and median age (heatmap). (Bergmann, 2016)](assets/bergmann-2016.png)

In addition to making maps interactive, web-based technologies have made it incredibly easy to combine statistical data from multiple sources to create mixed-data maps. For example, this mixed-data map [FIG] combines information about restaurants, liquor-serving establishments and age demographics to help statisticians perform location analysis based on a combination of asset and census data. 

## Urban Asset Mapping

While there are many kinds of maps and techniques for creating maps, our team was particularly interested in a specific form of mapping called **Urban Asset Mapping**. As its name implies, Urban Asset Mapping visualizes the dfiferent **assets** within an urban setting via interactive multi-modal maps.

After an extensive review of existing literature, our team was not able to find a complete definition of what constitutes an Urban Asset. As a result, we compiled information from various sources (Social Value Lab, 2012, OPEN GLasgow, 2014, South Seeds, 2017, and general tourism across Japan) to create a partial list of typical assets:

- Transportation (e.g., parking, bus stops, train stations, taxi stands)
- Healtchare (e.g., clinics, hospitals, pharmacies)
- Education (e.g., libraries, schools, universities)
- Public Spaces (e.g., parks, gardens)
- Recreation (e.g., network cafes, gymnasiums, theaters)
- Food (e.g., grocery stores, restaraunts, convenience stores)
- Religion (e.g., churches, temples, shrines)
- Infrastructure (e.g., government offices, post offices)
- Property (e.g., homes, apartments, empty plots of land)

![One of the composite assets maps produced as a result of the Social Value Lab’s survey of the Scottish town of Maybole. Blue dots indicate libraries, orange dots indicate educational assets, light green dots indicate leisure assets, and dark green dots indicate transportation assets. (Social Value Lab, 2012)](assets/social-value-lab-2012.jpg)

A prominent example of Urban Asset Mapping can be seen in a report on the Scottish town of Maybole published by the Social Value Lab in 2012. We believe it is important to note that some of the maps in the Social Value Lab's full report used a combination of asset mapping and on-site surveys to describe issues with *accessing or utilizing* assets; as Urban Asset Map is, therefore, not *just* a map of physical Urban Assets. The Social Value Lab was able to make extensive use of these composite asset maps [FIG] - generated in part from community surveys - to create a detailed list of recommendations to improve the community's health and safety. (Social Value Lab, 2012)

![An interactive map of prominent bell towers in Venice. Some of bell towers are clickable, allowing users to hear the sound of the bell and see a picture of the tower. The colors of the towers indicate how much data was collected for each tower. (Heinricher, Kahn, Maitland, and Manor, 2013)](assets/heinricher-kahn-maitland-manor-2013.png)

A more recent example of Urban Asset Mapping can be seen in a 2013 Worcester Polytechnic Institute (WPI) Interactive Qualifying Project (IQP) which mapped bell towers across Venice (Heinricher, Kahn, Maitland, and Manor, 2013). Their leveraged open-source mapping technology to produce an interactive map of Venice's bell towers [FIG] which can be used by members of the public to learn more about local history.

## The Electronic Field Form for Urban RegeneraTion (EFFURT)

![View of the EFFURT application where the user is creating a new Urban Asset marker for the Yuseji Temple.](assets/effurt-asset-create.png)

To ensure the data gathered during our research of the area around Yokai Street could be normalized, stored, and visualized across multiple digital and physical mediums, our team created a novel mapping tool called the Electronic Field Form for Urban RegeneraTion (**EFFURT**) [FIG].

Our team created a bespoke tool (EFFURT) because there are few (if any) existing tools that would allow our team to quickly collect, store, and visualize data for Urban Assets without paying exorbitant sums for a professional Geographic Information System (GIS). The EFFURT application, its source code, and its documentation, are made publically available on Gitlab for peer-review and reuse by future research teams.

### Features

![View of the EFFURT application where the user is viewing information about an urban asset.](assets/effurt-asset-view.png)

1. **Urban Asset Visualization and Analysis**: Users can view detailed information about urban assets [FIG] based on our survey findings and background research. This includes information like the specific urban assets offered at a given urban asset, imagery of the asset, and brief analysis of the state of the asset.

![View of the EFFURT application where the user is editing the information for a specific urban asset.](assets/effurt-asset-edit-form.png)

2. **Urban Asset Surveying**: Users can quickly conduct on-the-ground surveys and create new urban assets within EFFURT [FIG].

3. **Public Dataset Integration**: EFFURT integrates with external GIS services to display topographic, satellite, and street map data, in addition to urban asset information provided by external services like OverPass.

![View of the EFFURT application where the user is editing the location of existing urban assets.](assets/effurt-asset-edit-handles.png)

4. **Collaborative Editing**: EFFURT uses a robust, distributed data storage system that allows surveyors to gather data on mobile devices while offline, and then synchronize that data with one another while online. This allows users to easily create, share, and edit [FIG] existing urban assets.

### Implementation

To create EFFURT, we employed a conventional single-page web application (SPA) tool stack, including:

1. **HTML5 and JavaScript**: EFFURT's visual components are written using HTML5, and its logic is written in JavaScript. This allows EFFURT to run within a user's web browser across a wide variety of platforms, including laptops, desktops, and smart phones. 

2. **PouchDB and CouchDB** (https://pouchdb.com/): To easily distribute and share data between users, EFFURT uses a lightweight JavaScript database called PouchDB which synchronizes with a remotely-hosted CouchDB database.

![View of the EFFURT application where the user is changing between different visible layers.](assets/effurt-layers.png)

3. **Leaflet** (https://leafletjs.com/): To render and annotate maps, EFFURT uses Leaflet, an open-source HTML5 and JavaScript mapping platform that has been proven to work by multiple other research groups (Bergmann, 2016, South Seeds, 2017, Heinricher, Kahn, Maitland, and Manor, 2013). Leaflet allows EFFURT to display maps as *layers* [FIG] on top of one another; for example, it can display a layer for a satellite map of Kyoto, a layer for a street map of Kyoto, and a separate layer for urban assets in kyoto.

4. **OpenStreetMap** (https://www.openstreetmap.org/): By itself, Leaflet does not provide any **map tiles** (images of an actual two-dimensional map). As a result, EFFURT uses OpenStreetMap, a freely available repository of maps that are constantly updated by communities worldwide, to display map tiles.

5. **Overpass** (http://overpass-api.de/): OpenStreetMap provides a large amount of data beyond just map tiles—data like urban asset locations. To filter through all of this data and help our team quickly visualize urban assets within a region, EFFURT uses Overpass—a search tool for OpenStreetMap—to rapidly query and display urban assets that we are interested in on maps.

The complete EFFURT source code and implementation is available on GitLab at [https://gitlab.com/crahda/kyoto-b-18-iqp-urban-regeneration](https://gitlab.com/crahda/kyoto-b-18-iqp-urban-regeneration).

### Data Population

To populate EFFURT with data about current urban assets in and around Yokai Street, we sent pairs of surveyors up to 800m down major roads in the cardinal directions (North, South, East, and West) from the Yokai-SOHO building. During each survey outing, the surveyors were tasked with identifying any *commercial* urban assets. For the purpose of our surveying, we did not take residences into consideration.
