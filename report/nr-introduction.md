# Yokai Street and Regenerating Shotengai

Yokai Street is a decaying **shotengai** in Northwestern Kyoto that is looking for regeneration. During the Japanese Economic Miracle (the 1960s to 1980s), commercial streets and districts called shōtengai became widespread. Over time, many of these shōtengai were modernized with the introduction of public transportation, entertainment services, and various shops, creating vibrant communities for the neighborhoods that bordered these shōtengai.

When the Economic Miracle came to an end in 1991, large supermarkets and department stores—which provided most of a shōtengai's services in one place—rose to prominence, and retail stores and small businesses—like those found within shōtengai—began closing down en masse. Since then, many shotengai have undertaken regeneration projects in response to an increasingly harsh business environment. Yokai Street is one such shotengai that has undergone decay and attempted regeneration through creation of a yokai brand targeted at tourists.

Regeneration of Yokai Street through the yokai brand has had limited success, and shops have continued to disappear and be replaced by residential buildings. Restaurants and cafes along the street have continued to fluorish, while many shops continue to struggle. The goal of this project is to provide recommendations to regenerate Yokai Street.

This report begins with an examination of the history of Yokai Street and the role of tourism and experiences in regeneration. In our first Chapter, "Urban Assets", we introduce our novel mapping platform for mapping urban assets along Yokai Street, and discuss how the current assets of Yokai Street can inform regeneration efforts. In our second chapter, "The Aura of Yokai Street", we discuss the historical assets on and around Yokai Street, with a focus on how these can be used to an advantage. Our third chapter, "The Yokai Brand", we discuss the history of the yokai brand, current brand assets, and opportunities to strengthen the yokai brand. In our fourth chapter, "Visual Appeal", we examine the importance of visual aesthetics and provide recommendations to improve the visual aesthetic of Yokai Street. In our fifth and final chapter, "The Northwestern Kyoto Tourism Plan", we discuss a long-term solution to integrate Yokai Street into a larger, Northwestern Kyoto tourism plan emphasizing relics from the historic Heian-kyo capital.

<div style="page-break-after: always;"></div>

## What is a Shotengai?

Shotengai, also known as “shopping streets” or “shopping arcades,” are Japanese commercial streets that have long been an integral component of urban Japanese communities. Shotengai are ubiquitous in Japanese society; rough estimates suggest there are between 14,000-16,000 shōtengai across Japan (Larke, 1992), while a 1997 survey suggested there are over 18,000 shōtengai across Japan (Balsas, 2016). 

The concept of the shotengai dates back to pre-modern Japan, before the invention of the automobile, when shopping was a daily necessity and everyday life required shops located within walking distance (Balsas, 2016). The origins of shotengai may date back as far as the late 1500’s, when new free-market policies in Japan evolved commercial districts into linear shopping streets (Hani, 2005).

![Fig. 0.1: Togoshi Ginza shotengai in Tokyo, Japan, one of the largest and most popular shotengai in Japan. (Ito, 2018)](assets/ito-2018.png)

The shotengai is a Japanese commercial district, and differs from a Western-style shopping mall in several key characteristics (Balsas, 2016):

- The shops are typically built separately from one another, leading to an organic diversity of storefront appearances.
- Although the shops are owned and managed separately, shop owners form associations to manage the shotengai as a whole.
- Shotengai associations allow the shotengai to decide on a unified theme or feature for the entire street, including paving, lighting, and even roofs  over the street which unifies all of the shops as a single shopping destination. 

An example of a shōtengai with an archway and lighting can be seen in Fig. 0.1.


## Taishogun Shotengai (*"Yokai Street"*)

Taishogun Shotengai --- now called *"Yokai Street"* --- is a traditional Japanese shopping street located along Ichijo dori in Northwestern Kyoto. The location of Yokai Street is shown in Fig. 0.2

![Fig. 0.2: The location of Yokai Street in modern-day Kyoto on Ichijo-Dori, just south of Kitano-Tenmangu Temple and Imadegawa-Dori.](assets/yokai-street-map-point.jpg)

According to information from local residents, Taishogun Shotengai was a pleasure district full of **ryokan** (guesthouses), bars, and other forms of entertainment that served visitors to Tenjin-San in the 1800s. In the early 1960s, Taishogun transformed into a Shotengai, and became well-known for selling foodstuffs and sundries, at one point being nicknamed the *"Kitchen of Kitano"* (similar to Nishiki Market in central Kyoto). Because of its location in front of the gates of Tenjin-San, and its role as the primary traffic pathway to enter and exit the western side of Kyoto, Taishogun Shotengai experienced great affluence during this time.

<div class="ui segment piled figure"><div class="image"><img class="full-width" src="assets/kyoto-planning-map.jpg" style="max-height: 65vh"></div><div class="figure-caption"><p><br>Fig. 0.3: Showa 4 (A.D. 1929) city planning map (Yano, 2016) of the area of North-Northwestern Kyoto before the Imadegawa-Bypass was created, annotated to highlight key points of interest: The deep blue area denotes Kitano-Tenmangu, the orange line denotes the current location of Imadegawa-Dori, the green line denotes the current location of Yokai Street, and the red area denotes a region that, according to locals, Kitano-Tenmangu may have occupied before the creation of the Imadegawa-Bypass.</p>
</div></div>

When the Imadegawa-Dori bypass was created in the 1930s, as seen in Fig. 0.3, and Tenjin-San's front gates were moved from the area immediately adjacent to Taishogun, Taishogun began to see a slight decline in visitors since it was now out of the way of traffic. Over the next several decades, Taishogun experienced more and more decay, which peaked during Japan's economic crash in the late 1990s.

To counter this decay. the Taishogun Shop Owners' Association rebranded the entire street as "Yokai Street" in 2005, drawing from Taishogun's rich history as an area directly linked to **Yokai**. Yokai are monsters, spirits, and demons from Japanese folklore. Yokai take many diverse forms, ranging from dragons and foxes to old women.

![Fig. 0.4: A visual depiction of Tsukumogami. (/Users/crahda/work/kyoto-b-18-iqp-urban-regeneration/report/assets/kyotohyakki-nd.jpg)](assets/kyotohyakki-nd.jpg)

> The link between Ichijo dori and Yōkai (Japanese monsters/folklore) was derived from its location. Ichijo dori lies on the northern tip of Heian-Kyo (one of the Japanese former capitals), that is, the border between the internal and external. In Heian-kyo, some also said it was the route between the human world and the non-human world. 
>
> (Life & Culture Information Newsletter, n.d.).

Yokai Street is inspired by the legend of **Tsukumogami**, which are abandoned tools and belongings that transformed into Yokai after 100 years of neglect (Fig. 0.4). This legend stems from a ritual practice during the Koho period (A.D. 964 to A.D. 968) called **Susuharai** when people would dispose of damaged or unwanted items at the end of every year. (Kyotohyakki, n.d.)

![Fig. 0.5: Yokai Street at the height of the annual Yokai Parade on October 20th, 2018.](assets/yokai-street-parade.jpg)

Legends say these Yokai fought back against the humans who invaded them by parading from west to east along Ichijo-Dori, the northernmost street of Heian-Kyu (old kyoto) and the current location of Yokai Street. This parade became known as the Hyakkiyagyo Parade (*"The Night Parade of a Hundred Demons"*), and is the origin of Yokai Street's annual Yokai Parade (Fig. 0.2). (Kyotohyakki, n.d.)

> Yokai Street is unique, because we sell things that are meant to be bought for life; the legend of Tsukumogami reminds us of what happens when belongings are carelessly thrown away and neglected.
>
> Paraphrased from the owner of Yoshida Kimono Store on Yokai Street

By linking Taishogun Shotengai's location to its history, the Yokai Street Shop Owners' Association was able to launch a campaign to rebrand Taishogun around the legends of Tsukumogami and Susharai (Murakami & Yoshikura, 2015). During this campaign, shop owners created unique Yokai from old household items (symbolic of the Tsukumogami) and placed them in front of their stores, adopting a unified brand of Yokai all along Yokai Street.

<div style="page-break-after: always;"></div>

## Yokai Culture, Pop Culture, and Tourism

Yokai Street's goal, as communicated by the head of the shop owner's association, is tourism. Tourism is a powerful force that can bring new traffic and economic activity to an area. The Yokai Street brand has a lot of potential to become a tourist attraction due to the popularity of yokai culture.

Yokai Street has a lot of potential marketability. Japanese culture is cool, and Japan’s culture has been influencing its rise to prominence since the 1990’s (McGray, 2002). The Japanese Government has recognized this, and has created an entire department dedicated to the promotion of “Cool Japan” (Amano, 2010, “Promoting ‘Cool Japan’”, 2010, Inada, 2017). The “Cool Japan” TV show on NHK has previously aired episodes on both yokai and shotengai, and while the show doesn’t always hit the mark on what foreigners consider to be cool about Japan, our research indicates that Yokai have a lot of potential marketability. Many students that study Japanese literature and folklore become interested in the subject through manga and anime (Ito, 2015). The same concept may apply to tourists, who become interested in Japan through pop culture.

Yokai culture has a lot of potential to do well both domestically and internationally. Over the past few decades, Japanese culture exports have gained a large following internationally. Nearly every American child in the past 30 years have been influenced by Hello Kitty, Pokemon, Power Rangers, or Nintendo (Hoskin, 2015). In Japan, yokai were most famously popularized by Shigeru Mizuki’s 1959 manga Gegege no Kitaro, which was adapted into an anime show in 1968 and is continued to be released today (Shamoon, 2013). While Gegege no Kitaro is only popular among domestic tourists, there are also many internationally popular pop culture franchises that draw influence from yokai myths. These include Pokemon, Yokai Watch, and Hayao Miyazaki’s famous film, *Spirited Away*, which won the 2003 Oscar for best animated feature (Boyd, 2015). The popularity of yokai inspired culture both domestically and abroad demonstrates a potential tourist market for yokai culture consumption.

## Tourism in Kyoto

In order to understand how yokai culture can be applied to bring more tourism to Yokai Street, we must first understand the tourism industry in Japan and Kyoto.

<div style="page-break-after: always;"></div>

### Tourism Pollution in Kyoto

Yokai Street is currently not a hugely popular tourist destination, however Japan’s popularity as a tourist destination is growing and Kyoto is one of the most popular tourist cities in Japan. In 2003, approximately 5 million tourists visited Japan. In response to low numbers of foreign tourists visiting, the government launched a widely successful campaign called "Visit Japan" to boost tourism. As of August 2018, the number of foreign tourists that have visited Japan in 2018 has exceeded 20 million and is projected to go over the 30 million barrier by the end of the year, beating last year’s record of 28.7 million foreign tourists. The number of foreign tourists visiting annually is expected to rise even higher in anticipation of the Tokyo 2020 Olympics and is projected to reach 40 million by 2020 (Ryall, 2018). 

![Fig. 0.6: Tourists near the entrance to Kiyomizu-Dera in Gion, Kyoto.](assets/kiyomizudera-pollution.jpg)

The growing number of foreign tourists visiting Japan has resulted in tourism pollution, as seen in Fig. 0.6 at Kiyomizu-Dera in Gion, Kyoto. Tourism pollution or “kankō kōgai,” is a situation where a city or parts of a city become influxed with tourists, valued property and monuments become corrupted by tourists, and various hardships are imposed on residents and businesses due to the overflow of tourists (The Invisible Tourist, 2018). Yokai Street has mostly avoided effects of tourism pollution so far due to lack of foreign visitors, however reports of tourism pollution in other parts of Kyoto may leave Yokai Street’s residents reluctant to open their home to more tourists.

In ancient capitals of Japan such as Kyoto, locals are increasingly expressing their unenthusiasm and frustration toward the surges in tourists and are anxiously complaining to the authorities regarding the inconvenience of overcrowded public transport along the routes of famous attractions, loud and disrespectful foreigners, and poor mannerism among visitors (Ryall, 2018). A survey conducted in 2016 reported that 35 percent of shops, restaurants and accommodation facilities are not willing to offer multilingual services to foreign tourists due to multiple reasons such as language difficulties, limited resources, and poor etiquette among customers (The Japan Times, 2016). Many shops along Yokai Street do not currently offer foreign language accessibility, and poor impressions of foreign tourists may create reluctance to change.

Reports of disrespectful foreign tourists are numerous. Tourists have tried to take selfies with maikos (trainee geisha) on their way to their job, blocking their path and leaving some of them in tears. Inconsiderate visitors have carved names into trees at the bamboo forest of Arashiyama, a UNESCO world heritage site. Restaurant owners have also complained about reservations cancelled without considerate warning, or of people coming in to take pictures and then leave without buying anything (McCurry, 2018).

Another ramification of the tourism pollution is illegal accommodation. The daily lives of Kyoto residents are being significantly influenced by the surges in accommodations caused by the tourism boom. Tenants have been pushed out of their leased properties by real estate agencies in plots to transform them into travel accommodations. Some landlords are even renting out unlicensed properties. There have been many incidents where tourists entered private houses, mistaking them for vacation rentals and leaving the residents in terror (The Kyoto Shimbun, 2018). These incidents have created tension between many Kyoto residents and visiting tourists, and may contribute to negative perceptions of foreign tourists in the surrounding neighborhood of Yokai Street. 

While Yokai Street has not yet experienced tourism pollution, it is important to consider tourism pollution in other parts of Kyoto for several reasons. Rising tensions between Kyoto residents and foreign tourists may become an issue along Yokai Street in the future if Yokai Street achieves more success in attracting tourists to the shotengai. Therefore, it is vital to consider the effect on residents when making recommendations to improve Yokai Street.

In addition, many popular tourist areas in Kyoto have become very crowded and congested due to the increasing amounts of tourists visiting Japan. The quiet atmosphere of Yokai Street can be marketed as a selling point of Yokai Street, targeted at tourists who are tired of constant crowds at Kyoto’s more popular tourist spots.

### Government Efforts to Improve Tourism Pollution

The Kyoto Tourism Bureau has been working on policies intended to reduce friction between foreign tourists and residents. The concepts behind these policies can be applied to Yokai Street to accomplish the same goals on a smaller scale.

A partnership between the Kyoto Tourism Bureau and TripAdvisor has produced a pamphlet to educate visitors on social taboos, and the Bureau is encouraging events to be scheduled in the morning or evening to reduce congestion. In addition, the bureau also hopes to reduce congestion through promoting travel to Kyoto during non-peak months such as January, May, and June. Through these initiatives, officials from the tourism bureau hope to achieve harmony between tourists and local residents in order to continue promoting Kyoto as a tourism destination without ignoring the well-being of residents (Ryall, 2017). As a street with less traffic, Yokai Street would be able to contribute to decreasing congestion in busy tourist areas by drawing tourists away from congested spots.

In addition, experts have suggested that increasing the amount of tourist information centers, improving multilingual signage, and providing more information on lodging accommodations will improve infrastructure for tourists. Workers that are properly trained to assist foreign visitors and have good language abilities would also be invaluable to assisting tourists in Kyoto (The Japan Times, 2016). Yokai Street is currently not very welcoming or accessible to foreign tourists due to lack of foreign language signage and lack of English speaking ability. More accessible signage would go a long way towards making the street more welcoming and inviting to foreign tourists.

<div style="page-break-after: always;"></div>

## Experiences as a Vector for Tourism

Modern tourists actively seek out new "experiences"; **Airbnb**, for example, has expanded to offer not just unique bed and breakfast accomodations, but also all-inclusive **experiences that tourists can easily book online**. 

We believe that enriching Yokai Street with experiences will lead to improved tourism and opportunities for tourism. However, in order to apply experience design to Yokai Street, we must first understand what an experience is and how they are created.

### What is an Experience?

Traditionally, businesses in commercial districts offer commodities, goods, or services. In the traditional shotengai, produce shops sell produce; clothing shops sell clothes; restaurants serve food, and so on. An experience is something distinct from products and services—it offers a memorable experience for the consumer (Pine & Gilmore, 1998). The production of an experience for the consumer drives consumers to pay more for a service that could be obtained elsewhere—it’s the reason customers are willing to pay exponentially higher prices at Tokyo’s Aragawa Steakhouse for the same service they could receive at an Outback Steakhouse in the United States (Pizam, 2010).

Tourists, and especially young tourists actively seek out experiences. As a tourist destination, Japan offers many different experiences, from owl cafes to traditional geisha tea ceremonies. Kyoto is home to many of these unique experiences, including Fire Ramen—a restaurant where the ramen they serve is lit on fire, a hedgehog cafe, Toei Studio Park, and a Starbucks hidden inside a traditional tea house in the district of Gion. 

### Experiences and Younger Generations

Research has shown that the millennial generation is the driving force behind this “experience economy” that has erupted both globally and in Japan (Lewis & Jacobs, 2018, Cataldo, 2018, Kohler, 2017). Millennials are more likely to spend money on experiences rather than desirable objects. (Lewis & Jacobs, 2018, Kohler, 2017). Based upon these findings, we believe that Yokai Street will be more likely to attract young visitors and tourists if it offers unique experiences that cannot be found elsewhere.

Regeneration projects centered around unique activities and experiences may be more likely to attract involvement of local youth and entrepreneurs. Due to Japan’s aging population one of the challenges shared by many shotengai, including Yokai Street, is that many shop owners are aging without successors to take over their shop when they reach retirement (METI, 2014; Brumann & Shulz, 2012). Without successors or young entrepreneurs to take over storefronts, shops gradually become vacant and the shotengai may collapse, as did a once bustling shotengai in the Izumisano shopping district of Osaka prefecture (Nashima, 1997). Yokai Street currently has several vacant storefronts, and the at least one shop has closed in the past year alone. 

We examined case studies of 30 shotengai that have undertaken regeneration projects in the past decade. Fifteen of these shotengai cited vacant shops as an issue on their street, and four cited aging shop owners and lack of successors as a contributing factor to vacant storefronts. All of these studies said getting young people involved with the shotengai was important to regeneration. Many shopping association representatives said involving young entrepreneurs and community members brought more life and energy into the shopping association and the district as a whole (METI, 2014). 

It is clear that a balance of young people in a district is vital to the health of a shotengai; this is especially true in a time where the whole of Japan's population is woefully unbalanced, with a significant number of the population at or beyond retirement age. If the Yokai Street district wants to continue to survive, more young people need to become involved in the street. Adding exciting new experiences and activities to the street may be one strategy to increase visitors, traffic, and involvement of young shop owners and community members. 

### Defining Experiences

To evaluate existing and proposed experiences within Yokai Street, our team used a formal system that divides experiences into four categories Fig. 0.7:

![Fig. 0.7: Visualization of Experience Aspects and how they are determined (Pine & Gilmore, 1998).](assets/the-four-realms.jpg)

1. **Entertainment**: Does the visitor *watch* or *observe* the experience for entertainment? An example of an entertainment experience is Kyoto's Fire Ramen restaraunt, where visitors watch as their ramen is set on fire before they eat it.

1. **Education**: Does the visitor *learn* something from the experience? An example of an educational experience is an audio tour of Nijo Castle in Kyoto where visitors can learn about Tokugawa-Era architecture and aesthetics.

2. **Esthetic**: Does the visitor enjoy the beauty or the esthetic of the experience itself? An example of an esthetic experience is visiting Kyoto's Gion district, which adheres to a strong and consistent visual identity that brings visitors into the heart of historic Kyoto.

3. **Escapism**: Does the visitor enjoy the experience by *participating* in it? An example of this is the Mameshiba cafe in Kyoto, where visitors can interact with and pet Shiba Inu for half an hour at a time.

Most experiences fall into more than just one of these categories. For example, a tour of Nijo castle could be considered both educational and esthetic. However, the ideal experience simultaneously falls into all of these categories. (Pine, J. & Gilmore, J. 1998 and Pizam, A. 2010)

Because it proved cumbersome to use all of these categories when attempting to explain experiences to members outside of our time, we created a simplified definition of an experience that applies to any experience designed *for-profit*:

> An economic offering that evokes an emotional response from consumers, creating a unique discourse community and personal memories.

Throughout our report, we will broadly refer back to experiences and these experience categories in order to describe infrastructural opportunities and recommendations for regeneration in and around Yokai Street.

## The Format of this Report

This report examines how to regenerate Yokai Street utilizing a combination of asset mapping, tourism, and experience design. 

Each section (or "chapter") of this report covers a major area of our research, establishing a set of concrete recommendations to Yokai Street in order to implement plans for urban regeneration. These chapters include:

1. **Urban Assets**: Major findings and recommendations related to the *physical* assets and infrastructure along Yokai Street.
2. **The Aura of Yokai Street**: Major findings and recommendations related to the *historical* assets and opportunities around Yokai Street.
3. **The Yokai Brand**: Research and recommendations related to leveraging and integrating Yokai Street with its unique Yokai brand.
4. **Visual Appeal**: Research and recommendations related to improving Yokai Street's visual appeal and continuity.

The final chapter of this report, **A Northwestern Kyoto Tourism Plan**, draws from all previous chapters to propose a larger future work effort to integrate Yokai Street with the greater Northwestern Kyoto region.

### A Note on Stakeholders

This project had numerous stakeholders at play, including:

- Benoit Jacquet, the project sponsor and proprietor of Yokai SOHO.
- Members of the residential community near Yokai Street.
- The Yokai Street Shop Owners' Association.
- Shop owners within Yokai Street, including those not a part of the Yokai Street Shop Owners' Association.
- The Kyoto City Government.

Because of the kind and variety of stakeholders present in this project, we refer to this collection of stakeholders as **Yokai Street** (except when we are discussing the history ***of*** Yokai Street). This allows us to simplify our recommendations to focus on the *what* instead of the *who*.