# Recommendations

Based on our research findings, our team has created a series of recommendations to answer the problem statement given by our sponsor: ***How can we create new activities in and improve the attractiveness of a decaying urban district?***

## Improve Yokai Street's Visual Appearance

During our walkthrough's of Yokai Street, one issue with Yokai Street consistently stood out to our team: It was not immediately apparen that the street was a shopping street. Or any kind of cohesive street. This is due in good part to the entrance of Yokai Street, which is flanked by empty housing and dilapidated government property. We believe that to make Yokai Street more welcoming to visitors, establishing a more apparent brand identity at the very entrance of Yokai Street --- similar to how many other Shotengai have gates or entrances --- would make it clearer to visitors, and more welcoming.

## Enrich Urban Assets in Yokai Street with Experiences

Yokai Street is a unique Shotengai, particularly compared to its neighbords: While many Shotengai adopt a **theme** to help coordinate disparate shops under a single brand identity (e.g., the neighboring Kitano Shotengai), much like a mall, Yokai Street adopted a **brand**. The problem with a brand is that it has to be *sold* through *merchandise and experiences*; Yokai Street is actively treating its brand like a theme, trying to unify disparate shops under the guise of Japanese folklore and monsters.

Our team's recommendation to Yokai Street is to have individual shops more deeply integrate with the Yokai Street brand, prevalently offering themed goods and experiences that tie the very services and goods of the shops into the Yokai Street brand identity.

## Use International Yokai Culture to Attract Tourists to Yokai Street

Yokai are surprisingly well-known outside of Japan: From Yokai Watch to Pokemon, Spirited Away to Natsume's Book of Friends, many cultures have been exposed directly or indirectly to Yokai. Our team believes that Yokai Street could capitalize on this overseas awareness of Yokai in some way and use it to attract tourists to the Yokai Street area.

## Create an Experience-Focused Map for Tourists

One of the biggest problems our team perceived with Yokai Street is a lack of up-to-date information about activities and shops in and around Yokai Street. To counter this problem, our team created a tourism map [FIG] which we believe could be used by Yokai Street to help promote tourism.

![A draft of a simplified map of Taishogun that displays high-level information about shops and services.](assets/tourism-map-draft-1.png)

For this tourism map, our team leveraged a series of visual-rhetorical methods to entice domestic and foreign tourists to visit Yokai Street:

1. By distorting the depicted physical relationships between different landmarks to make them appear closer, we can visually integrate Yokai Street with its surroundings like Nishioji Dori and the Kitano-Tenmangu Temple.

2. Simplified visualization of Yokai Street and its shops using inviting colors and simple iconography. By reducing the visual clutter found on the current map of Yokai Street (which attempts to remain visually accurate), we can make shops stand out and appear more inviting to visitors.

3. A legend depicting "metadata" about the street (background, history, shops, and big seasonal events).

While creating this tourism map, we de-emphasized out the activities, shops, and events that are not particularly relevant to tourists, such as furniture shops or shops that cater to a very narrow demographic that lives in the local area. We took special care to include non-geographic data, such as seasonal events like the annual Yokai Parade or the monthly Kitano-Tenmangu Flea Market. This allows tourists to easily view all of the information about the local area in one location.

