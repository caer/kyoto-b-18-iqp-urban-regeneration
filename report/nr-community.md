# Visual Appeal: Regeneration through Aesthetics and Community Spaces

## Introduction: The Power of Aesthetic Immersion Experiences

The visual appearance of Yokai Street is, in many ways, very unreflective of the purpose of Yokai Street. Visual appearances matter, especially to tourists. Tourists seek out experiences, and aesthetics are a major contributing component to an experience. Appropriate experience aesthetics result in immersion of the participant, and can improve the richness and quality of the experience (Pine & Gilmore, 1998).

### Theme Parks as Exemplary Immersion Experiences

Lessons learned from well designed aesthetic immersion experiences at popular theme parks can inform the aesthetic design of Yokai Street. Aesthetic immersion is a powerful tool, the effects of which can be seen at successful theme parks such as Disney and Universal Studios. The Disney Sea resort in Tokyo has many different immersive, themed areas that are designed to make guests feel as if they have entered another universe. In the resort’s Venice themed area (Fig. 4.1), everything from the buildings, canals, and gondolas to the cobblestone paving makes a guest truly feel as if they are in Venice instead of Tokyo. If an experience has a theme, such as the yokai theme of Yokai Street, a visitor needs to be immersed in that theme. The guest should be constantly bombarded with visual cues that are tuned to the experience. Visitors of Yokai Street need to be immersed in a visual yokai theme to enrich the Yokai Street experience.

![Fig. 4.1: View from the Venetian Gondola in the Venice themed area of the Tokyo Disney Sea Resort.](assets/Disney-Venice.jpg)

Another strong visual tool used by theme parks are large thematic landmarks, such as Hogwarts castle at the Wizarding World of Harry Potter in Universal Studios. The sight of the replica of Hogwarts set into the hill (Fig. 4.2) sends fans straight back to their childhood, evoking strong emotions and memories of the first time they saw the movie The Sorcerer's Stone. The rest of the area is designed to match the Hogwarts aesthetic to immerse visitors in the world of Harry Potter. The cohesive design of the entire park around Hogwarts castle allows guests to be transported to a world where magic is real for one day. Like Hogwarts, a central yokai landmark located on Yokai Street could help bring the theme and the entire street together.

![Fig. 4.2: Replica of Hogwarts Castle at Universal Studios Resort in Orlando, Florida (Cruz, 2015)](assets/Hogwarts.jpg)

### Cultural Experiences in Kyoto

Yokai Street is located in Kyoto, which contains many competing tourist attractions that utilize visual aesthetics to sell the experience of traditional Kyoto. One of these attractions is the historic Geiko district of Gion. Gion is the 4th most visited site in Kyoto, according to Japan Guide (“Gion”, 2018), and is praised by visitors as “charming and beautiful”, “gorgeous”, “magical”, “traditional”, and “lovely” by reviewers on TripAdvisor (2018). 

The streets of Gion, as seen in Fig. 4.3, are narrow, paved with stone, and surrounded by traditional wooden machiya, or townhouses. Lucky tourists will get to see Geikos and Maikos in full costume on their way to their evening appointments. The traditional setting and an abundance of tourists strolling around in rental kimonos immerses a visitor in the aesthetics of old Kyoto. Tourists visiting Yokai Street will have likely visited other attractions in Kyoto such as Gion, and will expect the shotengai to immerse them in yokai, similar to how Gion immerses them in traditional Kyoto.

![Fig. 4.3: A Street in Gion, Kyoto, a popular tourist spot. (Sorasak, 2017)](assets/Gion.jpg)

The cultivated aesthetic of a place like Gion requires maintenance and regulation. Gion is beautiful, but their beauty has been maintained through regulation of the neighborhood. Even the Starbucks in Gion has to comply with the aesthetic. The Gion location of the international coffee chain is set up to look and feel like a traditional Japanese tea house, as seen in Fig. 4.4 Results from the experience survey conducted by our team (see Appendix A for more details), described the Gion Starbucks aesthetic as “traditional”, “dark”, “wooden”, “Japanese”, “small”, and “cute”, demonstrating that the Gion Starbucks has been completely and successfully integrated into Gion. 

Gion's aesthetic is derived from the *machiya* buildings of the district, which is fundamentally different from Yokai Street's aesthetic that is derived from the storefronts and yokai decorations. However, the end result needs to be the same. Tourists will take notice of the cohesive aesthetic of a location, or lack thereof. The shops and activities of Yokai Street need to be fully integrated into the theme in order to completely immerse visitors into the theme. 

![Fig. 4.4: A tatami mat seating area in the Gion Starbucks that is disguised as a traditional Japanese tea house.](assets/gion-starbucks-seating.jpg)

Other shotengai in Kyoto also provide stiff competition for Yokai Street. One shotengai that is very popular with tourists is Nishiki Market, a historic, traditional Japanese food market. This shotengai is the 14th most visited tourist site in Kyoto according to Japan Guide (“Nishiki Market”, 2018). As seen in Fig. 4.5, Nishiki Market is a narrow shotengai with a colorful arcade covering. When a visitor walks through Nishiki Market, they are bombarded with Japanese language, paper lanterns, and the sights and smells of a historic Japanese food market. Many visitors coming to Yokai Street will expect to be immersed in the shotengai, just as visitors are immersed in Nishiki Market.

![Fig. 4.5: The entrance to Nishiki Market in Kyoto. (Toshiyuki, 2007).](assets/Nishiki.jpg)

## The Current Aesthetics of Yokai Street

As seen by the examples of Disney Sea, Universal Studios, Gion, and Nishiki Market, tourist experiences need to have a cohesive, unified aesthetic to provide an immersive experience for visitors. Yokai Street is currently lacking this continuous aesthetic theme. Yokai Street needs to improve its visuals and become more immersive to compete with other Kyoto tourist attractions such as Gion and Nishiki Market.

![Fig. 4.6: The entrance to Yokai Street from the East side of the street, just South of Kitano-Tenmangu Shrine.](assets/yokai-street-bleak.jpg)

The current entrance to Yokai Street, as seen in Fig. 4.6, is not welcoming to tourists. Aside from the small banner on the lightpost to the left, proclaiming the words “Yokai Street”, the first big sight along the shotengai is not of yokai, bustling shops, or customers, but of dilapidated residential buildings on the right side of the street. These buildings do not evoke a welcoming feeling or invite customers to continue walking down the street, and the small banner is easy to miss and only accessible to Japanese language readers.

In addition, unlike the Starbucks in Gion, the shops along Yokai Street are not fully integrated into their theme to their full potential. Yokai imagery and art is fragmented and discontinuous along the shotengai. Not every shop is part of the Yokai Street Shop owners association and the theme, and even some of the shops that are part of the association don’t display Yokai statues or Yokai artwork. While the Yokai theme is present, it is not immersive. It is easy to miss, and it does not create a cohesive experience for the visitor.

## Opportunities for Aesthetic Improvement on Yokai Street

Despite its current state, Yokai Street has many opportunities to improve the visual appearance and aesthetic of the shotengai created by infrastructure that is already in place. These opportunities include:
- Creation of a more obvious entrance banner. This would improve the street’s visibility and create a more welcoming entrance.
- Creation of a community resting space in an under-utilized sidewalk area. This would beautify the entrance area of the shotengai, as well as offer a space for locals and shoppers to sit and rest, filling a need for more public spaces in the area.
- Integrate visually into the Yokai theme. Yokai Street has a theme with a very strong potential which has been limitedly utilized by the shop owners.

This chapter covers recommendations to improve upon these opportunities.

### A New Entrance Banner

**We recommend suggesting to the shop owners association to improve their current banner sign at the entrance of Yokai Street.**

While Yokai Street currently has banners marking the start and end of the street, as seen in Fig. 4.6, reviewers have stated that the street was difficult to find, despite the Yokai Street banners hanging on the street’s many light poles (Foster, n.d.). The current large entrance banners on Yokai Street are subtle and only located on one side of the street. They have not been refreshed in several years and have become faded from sunlight. New entrance banners or signs will make the entrances to the street more visible and welcoming.

#### Current Banners of Yokai Street

<div class="ui segment piled figure"><div class="image"><img class="full-width" src="assets/yokai_current_banners.png" style="max-height: 75vh"></div><div class="figure-caption"><p><br>Fig. 4.7: <strong>Top Left:</strong> Banners hanging at the Nakadachiuri entrance of Yokai Street; <strong>Top right:</strong> Street signs at the Gozen dori entrance of Yokai Street, which connects Yokai Street to Kitano-Tengmangu Shrine; <strong>Bottom:</strong> Banners hanging at the Nishioji dori entrance of Yokai Street.</p>
</div></div>

Yokai Street currently has 3 major entrances that bring foot traffic to the street: Nakadachiuri intersection, Nishioji dori intersection, and Gozen dori intersection.

**Nakadachiuri Intersection:** This side of Ichijo dori, as seen in the top left of Fig. 4.7 opens to a large intersection with Nakadachiuri and is open to car traffic. It currently has one large side entrance banner on the side near Yokai SOHO. This entrance already has street light infrastructure that may benefit the installation of new banners.

**Nishioji Intersection:** This side of Ichijo dori, as seen in the bottom of Fig. 4.7 connects Ichijo dori to Nishioji dori, a major street. It currently has one large side entrance banner next to Jizoin temple. This entrance already has street light infrastructure that may benefit the installation of new banners.

**Gozen dori Intersection:** Gozen dori is a small side street that connects Ichijo dori to Kitano-Tenmangu Shrine. It is a small intersection and currently contains direction signs to the two nearby shrines, but no Yokai Street banners, as seen in the top right of Fig. 4.7 This entrance does not have current infrastructure to support new banners.

#### Entrance Signs and Banners in Other Shotengai

To understand what type of entrance sign would be best at these three entrances, we examined examples of what other shotengai have done. Many shotengai have overhanging entrance signs or banners to welcome shoppers and let them know they’re entering the shotengai district. These signs take many forms, including complete arcades such as the one at Nishiki Market (Fig. 4.5). Yokai Street does not have an arcade, and does not have the infrastructure to create an arcade because it is a street with car traffic, so we focused on shotengai without arcades.

Other streets without arcades have many forms of entrance banners, such as permanent archways, overhanging signs and banners, and more subtle side banners like Yokai Street’s current signs (Fig. 4.7). An example of an archway can be seen at Harakuju’s Takeshita Street on the left of Fig. 8, and an example of an overhanging banner can be seen on the bottom right of Fig. 4.8 at Nishijin Chuo Shotengai in Fukuoka. Another example of entrance signs can be seen at Cinema Street in Kyoto (Fig. 4.8, top right), a cinema themed shotengai similar to Yokai Street. This shotengai has installed permanent “Cinema Street” signs on either sides of the street.

![Fig. 4.8: **Left:** Archway at the entrance of Harajuku’s Takeshita Street in Tokyo; **Top Right:** Permanent signs on either side of the Street at the entrance of Cinema Street in Kyoto. (Let’s Live in Kyoto, 2018); **Bottom Right:** Banner at the entrance of Nishijin Chuo Shotengai in Fukuoka. (Nissy-KITAQ, 2008)](assets/archways_resize.png)

#### The Options for Yokai Street

The following options are the most feasible possibilities for Yokai Street to refresh its entrance banners and are inspired from Yokai Street's current banners, Cinema Street, and Nishijin Chuo Shotengai.

<div class="ui segment piled figure"><div class="image"><img class="full-width" src="assets/side_banners_rec.png" style="max-height: 50vh"></div><div class="figure-caption"><p><br>Fig. 4.9: Option 1: New Side Banners Summary. Cost estimates from (Mojoprint, 2018).</p>
</div></div>

Option 1 (Fig. 4.9): Create 2 new banners similar to the current banners as seen in Fig. 4.6 and Fig. 4.7, and install them on either side of the street at each entrance (similar to the layout of the Cinema Street signs, Fig. 12). Infrastructure is already in place for this option on one side of the street at the Nishioji and Nakadachiuri intersections. The current banners are old, faded and hard to spot. New banners would update Yokai Street’s look, and adding a second one on the other side of the street would increase visibility. This option is the lowest cost of the proposed upgrades and would have medium impact compared to the current banners. The banners would need to be replaced in an estimated 3-5 years due to fading from sun damage, as seen in the current banners in Fig. 4.7.

<div class="ui segment piled figure"><div class="image"><img class="full-width" src="assets/overhanging_banner_rec.png" style="max-height: 50vh"></div><div class="figure-caption"><p><br>Fig. 4.10: Option 2: Overhanging Banner Summary. Cost estimates from (Mojoprint, 2018).</p>
</div></div>

Option 2 (Fig. 4.10): Create an overhanging banner to hang over the street similar to Nishijin Chuo Shotengai on the bottom right of Fig. 4.8 The light poles down Yokai street provide partial infrastructure to support these banners. This option has a mid-range cost estimate compared to the other two options and would have high impact. The banners would need to be replaced in 3-5 years due to fading from sun damage, as seen in the current banners in Fig. 4.7.

<div class="ui segment piled figure"><div class="image"><img class="full-width" src="assets/paermanent_signs_rec.png" style="max-height: 50vh"></div><div class="figure-caption"><p><br>Fig. 4.11: Option 3: Permanent Sign Installation Summary.</p>
</div></div>

Option 3 (Fig. 4.11): Create permanent signs, similar to those of cinema street on the top right of Fig. 4.8. Current light pole infrastructure may be able to be utilized as supports to offset the cost of this option. This option would be the highest cost of the proposed options, and would have medium impact. This option would be the longest lasting as it would require less maintenance than the canvas banners.

**Our Recommendation:**  Option 2: Overhanging Banner
Overhanging banners such as those in Nishijin Chuo Shotengai will refresh Yokai Street’s entrance signs and increase their visibility. While this option would cost more than refreshing the current side banners, it is not the most expensive option and would have the most impact relative to the cost.

### A New Community Space

**We recommend petitioning the Kyoto City Government to add a park to Yokai Street.**

Adding public space to Yokai Street would be beneficial for regeneration. Northern Kyoto has a lack of green space and parks, and the area surrounding Yokai Street specifically has no parks within short walking distance. As seen in Fig. 4.12, there are only two small parks near Yokai Street. The park south of Yokai Street near Tenjin River is approximately 400m from Yokai SOHO; the park to the north of Yokai Street is approximately 800m from Yokai SOHO. These parks are sandy, small, and out of the way. The largest green space near Yokai Street is Kitano Tenmangu Shrine, which is an inappropriate place for eating or playing because it is a religious site. The area immediately surrounding Yokai Street contains no public spaces for locals or visitors to relax and socialize.

![Fig 4.12: Annotated map showing parks near Yokai Street, which is highlighted in light blue. Existing parks are highlighted with dark green and proposed park on Yokai Street is highlighted in dark blue.](assets/park-map-annotations.png)

In Tokyo, there are small parks on nearly every other city block, that are similar in size to spaces available on Yokai Street. The Tokyo park shown in Fig. 4.13 is a small park with a children’s slide and seating areas. Small parks like this one are not large or extravagant, but they provide an essential function: open space for residents that live in the congested city of Tokyo. This park demonstrates that even small parks can have a high impact on the community.

![Fig. 4.13: A small park in Tokyo set into a city block.](assets/tokyo-park.jpg)

<div style="page-break-after: always;"></div>

#### Public Spaces in Revitalization

Public spaces have huge potential to contribute to revitalization along Yokai Street. Beautification of public spaces has been used around the world as a component of revitalization plans. In Seoul, South Korea, the government cleaned up the Cheonggyecheon stream (Fig. 4.14) as part of a revitalization of downtown Seoul, which had fallen into decay after a new highway created noise pollution in the area, driving residents and businesses away. Cleaning up this public space made the area more attractive and contributed to its rejuvenation; it is now a desired location within the city (Amirtahmasebi R., Orloff M., Wahba S., & Altman A., 2016). 

![Fig. 4.14: The renovated Cheonggyecheon stream in 2015 (Pegrum, 2015).](assets/Cheonggyecheon_stream.jpg)

Johannesburg is another city that has utilized public spaces to drive revitalization. To revitalize the inner city area, which fell into decay after the city council moved to another location, taking many jobs and residents with it, the city created City Improvement Districts (CIDs) to manage public spaces (Amirtahmasebi R., Orloff M., Wahba S., & Altman A., 2016). The city has since created and renovated many parks, and continues to do so today. This past month, 3 new parks were launched in the inner city district, one of which is seen in Fig. 4.15 (“Three new parks launched in Jan Hofmeyer”, 2018). In part due to creation of public spaces, areas managed by CIDs have become the most coveted living areas in the inner city (Amirtahmasebi R., Orloff M., Wahba S., & Altman A., 2016). Creation of public spaces increases the value of both residence buildings and businesses in the surrounding area.

![Fig. 4.15: A new park in the inner city district of Johannesburg, launched in December 2018 (Joburg City Parks and Zoo, 2018).](assets/joburg_park.png)

The addition of public spaces is also common in shotengai revitalization projects. Many shotengai that have undertaken revitalization projects have created some form of community space as a component of their revitalization plan. We examined 30 case studies of shotengai revitalization and found that 8 out of the 30 case studies added some type of community space intended to serve as a space where shoppers can rest and socialize, as seen in Fig. 4.16. 

![Table. 4.1: Shopping districts that created some form of community space as part of a revitalization project (METI, 2014).](assets/case_studies_table.png)

These case studies emphasize the importance of community resting spaces and events. Many of these shotengai as seen in Table. 4.1 used vacant shops—underutilized spaces on the street to create community spaces for shoppers to relax and socialize. Minamisanriku Sansan Shopping District, Shin Tottori Ekimae Area Shopping District, Daiei-dori Shopping District, Takanabe-cho Shopping District, and Miyako-shi Suehiro-cho Shopping District added some form of space to serve as a resting space for shoppers, which is essentially the same function provided by a park. In addition, Utsunomiya Orion-dori Shopping District, Taketamachi Shopping District, Miyako-shi Suehiro-cho Shopping District, and Izumi-cho 2-chome Shopping District all created spaces intended to be used for events. As demonstrated by these shotengai, community resting spaces and events spaces are important. Yokai Street already has assets that provide event space. What Yokai Street lacks is a public space where shoppers, students, or locals can sit and relax without having to purchase an item at a restaurant. A park would recfity this need.

#### Opportunity on Yokai Street

Yokai Street already has infrastructure that could support a park. Yokai Street currently has an underutilized asset right at the entrance of the street—a wide stretch of sidewalk in front of the government owned housing buildings that does not get much traffic because there are no shops along this stretch of sidewalk. The space was utilized for temporary stalls in the past, but these stalls are no longer in business and this space is only used once a year during the Yokai Parade. Additionally, there is space for the park to be extended into the street to match the width of the flower beds that are currently extended into the street. This extension would not affect the functionality of the street as it would maintain the same functional width for cars as it currently has. The site of the proposed park, as seen in Fig. 4.16 is approximately 400 square meters.

![Fig. 4.16: A satellite view of the area where the proposed park would be implemented. Site of the park is an area of approximately 400 square meters, highlighted in green (Google Maps, 2018).](assets/satellite_yokai_overlay.png)

As seen in the left image of Fig. 4.17, this stretch of sidewalk along Yokai Street is visually unappealing and uninviting to visitors walking down the street. The dilapidated housing building in the background dominates the scene, and the overgrown shrubs on top of the rock wall also detracts from the appearance. Replacing this sidewalk with a park would add more value to the area. 

![Fig. 4.17: Current state of the proposed park site (left) compared to an artist's mock-up of what the proposed park could look like (right).](assets/yokai_park_comparison_resize.png)

A mock-up of the proposed park could look like can be seen on the right of Fig. 4.17 with grass, benches, and Yokai statues and photo board. This is only one possibility of what the park could look like. Other features could include, but are not limited to:
- dirt paving similar to many children’s playgrounds in Japan
- paving such as the Tokyo park as seen in Fig. 4.13
- a small swing set or slide similar to the Tokyo park in Fig. 4.13
- permanent Yokai statues
- fencing with Yokai murals

The designer of this park should rely in input from the shop owners association and the chonakai as to what fetures these organizations would in the park.

#### A Note About Noise and Events

This proposed park would be beneficial for many reasons. It would provide a public space for tourists and locals to sit and relax while visiting Yokai Street. A child-friendly park would benefit families and may attract younger families to the area. A park would also improve the visual appearance of Yokai Street’s entrance and make the street more welcoming to visitors. In addition, a park petition is a low-risk action. If the petition to build the park succeeded, the government would be in charge of building and maintaining the park, requiring no funds and little responsibility beyond the initial petition. The installation of a park would be a home run for Yokai Street.

Proposing a new park may still have roadblocks, however. Informal conversations with our sponsor, Benoit Jacquet, has indicated that residents surrounding Yokai Street can be very sensitive to noise and outsiders in their neighborhood. They may be apprehensive about the addition of a park that would draw more people to the street, and resistant to the establishment of more events along the street. Several counterarguments can be presented to help make residents more receptive to the idea of a park.

First, the park is at the entrance of the street. It is far away from the Nishioji and of the street and would have low impact on residents living near this end of the street. Second, noise from the park should subside in the evening as parks are typically utilized by daytime shoppers and families with kids, both of which would likely not be out past 20:00. Third, there is a *koban* police station very close to the location of the proposed park location. Police officers could quickly and easily respond to any night time disturbance caused by juvenile delinquents or other parties.

#### Next Steps

The following actions should be taken concurrently to start the process of petitioning the Kyoto City Government to add a public park to Yokai Street:
1. Contact the Kyoto City North Greenery Management Office. They are in charge of park management in Northern Kyoto and will be able to provide information on how to petition for a new park.
2. Propose the idea to the Shop Owners Association; the support of the shop owners association would likely strengthen the petition
3. Propose the idea to the Chōnaikai. Residents surrounding Yokai Street are likely to benefit from the addition of a park and their support would also help the petition.
4. Follow up with the North Greenery Management Office to take any steps they require to petition for the creation of a new park.

<div style="page-break-after: always;"></div>

## Visual Integration of the Yokai Theme

**We recommend suggesting increased visual integration of the Yokai theme to the shop owners association.**

The Yokai theme has lots of potential. In a survey of 30 foreign tourists we conducted in the Kyoto Station area (see Appendix B for more details), 29 out of 30 respondents indicated interest in Yokai-inspired pop culture such as Pokemon or Spirited Away, and 18 out of 30 respondents indicated an interest in Yokai culture. Tourists like Yokai, and the expectation for a Yokai themed shopping street is that visitors will see lots of Yokai. While there are yokai statues and yokai artwork on yokai street, there simply isn’t enough.

One of the main issues facing Yokai Street’s visual integration is the discontinuous nature of the yokai theme. Not all the shops along the street have yokai statues or decorations, and as a result, the yokai theme is not pervasive throughout the street. Fig. 4.18 shows a stretch of Yokai Street that contains no yokai imagery. A visitor would not be able to easily tell they are on Yokai Street based on the visual imagery present in this frame. In order for tourists to fully experience Yokai Street, they need to be able to feel as if they are immersed in the yokai theme.

![Fig. 4.18: A stretch of Yokai Street with no visible Yokai imagery.](assets/yokai-street-empty.jpg)

Yokai Street is not the only location in Japan that has used yokai to attract tourism. The town of Sakaiminato in Tottori prefecture, the birthplace of the famous manga artist Shigeru Mizuki, has also used yokai and the history of Mizuki's life to revitalize the town through tourism. Permanent statues of yokai from Mizuki's art are scattered throughout the town, one of which is seen in Fig. 4.19 The yokai statues are pervasive throughout the town, which contains over 100 bronze statues in total (Waters, 2016). While Yokai Street is a shotengai and not a town, it still needs to have sufficient amounts of Yokai theming to draw tourists, and therefore needs to have more dense yokai imagery to fit a similar amount of yokai as an attraction such as Sakaiminato into one street.  

![Fig. 4.19: Statues of Shigeru Mizuki's yokai characters in Sakaiminato, Japan (bluegreen405 via Flickr, 2012).](assets/sakaiminoto.jpg)

While Yokai Street’s current aesthetic is not sufficient for a tourist attraction, Yokai Street has a really strong platform to build on to increase their visual integration. Fig. 4.20 shows a collage of all the Yokai statues and art once present on Yokai Street compiled into one frame. As seen in the collage, Yokai Street has a lot of assets to build on. They have a well-developed, quirky art style, as well as a relationship with a yokai art professor, Junya Kono and his art group at Saga University of Arts in Kyoto, which has produced art for Yokai Street in the past and could potentially create more art for the shotengai. 

![Fig. 4.20: A collage of all the past and present Yokai Art in one location on Yokai Street.](assets/yokai-collage.jpg)

Yokai Street also has a reserve of several yokai statues and art boards, including the ones seen in Fig. 4.21 below, that are not currently being used and are stored in the shop association office. Yokai Street could easily start improving their visual aesthetic by displaying their unused yokai art, with a long-term goal of creating more.

![Fig. 4.21: Yokai art boards on display outside the Hachimanya Wonder Shop in 2014. These art boards are currently in storage (Hiro - Kokoro☆Photo via Flickr, 2014).](assets/yokai_art.jpg)

Another problem blocking the visual integration of Yokai Street is that the majority of the yokai imagery disappears when shops are closed. Most of the yokai statues are brought into their respective shops while the shops are closed. The Nishiki-Kagetudo Tea Shop is one of these shops, and the drastic difference between opened and closed storefronts can be seen in Fig. 4.22 When the stores are closed, Yokai Street ceases to be Yokai Street, which is a huge continuity issue because shops have different opening and closing hours. 

![Fig. 4.22: Nishiki-Kagetudo Tea Shop when it is closed (left) (Google Maps, 2018) compared to when open (right).](assets/comparison.png)

Yokai Street may be able to draw inspiration from other shotengai to rectify this problem. An example of a shotengai that successfully maintains its aesthetic after closing hours is Takeshita Street in the Harajuku district of Tokyo. As seen in Fig. 4.23, the store shutters are covered in a mural, perpetuating the Harajuku aesthetic even after shops are closed. This concept could be applied to Yokai Street. Covering the store shutters in yokai art would keep Yokai Street looking alive, even after store hours.

![Fig. 4.23: A mural on a closed storefront in Harajuku at night (F. Kurtz, 2018, personal communication, December 11, 2018).](assets/takeshita_mural.jpg)

Like the magical sight of Hogwarts, a central landmark can also bring a theme together. On Cinema Street in Kyoto, the shop owners erected a large statue of an old Japanese movie hero in 2013 to bring the street together around their movie theme. This statue, which can be seen in Fig. 4.24, is 5 meters tall and serves as a local landmark. However, as a representative of the shop association of Cinema Street stated, it is not enough to simply have the landmark. The street has held constant movie themed activities to keep the statue current and relevant to the street (METI, 2014). Yokai Street has faced similar issues with their smaller scale yokai statues. The yokai theme needs to be pervasive in visuals and events in order to give the yokai statues relevancy. Like Cinema Street, Yokai Street may also benefit from some sort of central yokai attraction to draw the yokai theme together. As seen in Fig. 4.23, Yokai Street has a yokai souvenir shop that is located in a central intersection in the shotengai, which could serve as a centralizing yokai landmark.

![Fig. 4.24: Statue of movie hero Daisuke from a 1966 Japanese film, located in front of the supermarket on Cinema Street (Minochage via Flickr, 2013).](assets/daimajin.jpg)

The first step to further integrate Yokai Street into their theme would be to begin utilizing the reserve of yokai artboards and yokai statues to take advantage of all the resources Yokai Street currently has. To further integrate Yokai Street’s aesthetic with their yokai theme, we provide the following recommendations that could be carried out by the shop owners association in collaboration with Saga Art University students:

**Produce more artboards.**
Produce more artboards and display them along the street to create a stronger pervasive sense of the yokai theme. This recommendation draws inspiration from immersive theme parks, as well as locations like Gion and Nishiki Market, and will strengthen the aesthetic of Yokai Street.

**Create yokai statues for every shop on the street.**
Produce new yokai statues for every shop on the street, regardless of shop association membership. Shop association membership is a limiting factor to the continuity of the yokai theme, and attempting to convince all the shops to join is an unrealistic short-term goal. This recommendation draws inspiration from theme park experiences and the town of Sakaiminato, Japan.

**Create a style guide for shops.**
Create a style guide for shops in the shotengai. The purpose of this recommendation is to implement regulation of shops similar to those in Gion. This will help individual shops integrate into the theme, and has been done by other shotengai during revitalization projects to improve the overall aesthetic of the shotengai (METI, 2014). This recommendation draws inspiration from Gion, which has successfully integrated places such as Starbucks into its aesthetic through regulation.

**Create yokai murals.**
Create yokai murals on the storefront shutters of association shops. This recommendation draws inspiration from Takeshita Street in Harajuku and will allow the shops to display their yokai theme after closing hours.

**Make Hachimanya Wonder Shop a Centralizing Landmark and Community Event Space.**
Transform the souvenir shop into a centralizing yokai landmark through the addition of a large statue representing the mascot of Yokai Street. This would provide Yokai Street with a centralizing visual, as there are currently no large statues of its mascot. This recommendation draws inspiration from the movie hero statue on Cinema Street and the yokai statues of Sakaiminato, Japan.

In addition, the Hachimanya Shop can be utilized as a centralizing community event space. As discussed earlier, the case studies of revitalized shotengai emphasized the importance of community event spaces. Hachimanya has a unique position, both as a central location in the shotengai and a central force and also as part of the yokai brand. We recommend utilizing the Wonder Shop as a Yokai community center to hold yokai themed events. For example, they could transform the building into a haunted house on Halloween night. Implementing frequent events along Yokai Street will strengthen the community of Yokai Street, and by extension, the yokai aesthetic and brand.

Through these changes inspired by other experiences found in Japan, Yokai Street can incorporate concepts utilized by well designed immersive experiences such as Disney and Universal Studios to create a more cohesive experience design for visitors.