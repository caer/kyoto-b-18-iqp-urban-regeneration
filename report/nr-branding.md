# Walking in the Yokai Parade: Reviving the Yokai Brand for an Immersive Experience

In 2005, the shop owners association of Yokai Street rebranded the street from Taishogun Shotengai (its historical name drawn from the Daishogun Shrine) to Yokai Street, derived from its historical connection to Yokai mythology and the *hyakkiyagyo* yokai parade. As stated in the introduction, Taishogun shotengai was facing decay due to the removal of a nearby train station and construction of Imadegawa dori which forced Kitano Tenmangu’s entrance away from Yokai Street. Since these two assets were anchor points for the shotengai, Taishogun needed to be innovative in developing an identity that acted as its own anchor point, leading to the creation of the yokai branding.

![Fig. 3.1: Collage of the Various Yokai Along Yokai Street.](assets/YokaiCollage.png)

Branding is a powerful tool that creates cohesive identity. People connect to this identity through experiences. As discussed later in Chapter 4, Disneysea and Gion use their brands to create experiences and attractions like Tokyo DisneySea’s Venetian gondola ride and the teahouse Starbucks in Gion. Through these branded activities and events, Disney and Starbucks are able to create strong, cohesive experiences in which visitors become immersed (Hilton, 2007).

Recently, Yokai Street’s brand has begun to fade and lose its assets. In spite of its waning brand, an abundance of structural opportunities for reviving the brand and demand for yokai experiences remains. With this infrastructure and a significant demand for yokai culture, these opportunities can not only be utilized to revive the yokai brand but also for creating an immersive experience for Yokai Street's visitors. In this chapter, we will review all of the different assets that Yokai Street has created since they rebranded, what happened to these assets, and what opportunities are available to revive the yokai brand with a goal of creating an immersive Yokai Street experience.

## Structural Opportunities to Redevelop and Capitalize on the Yokai Brand

### Yokai Brand Assets

As stated in the Chapter 1, "Yokai Street and Regenerating Shotengai", experiences are commonly categorized into four experience aspects; Entertainment, Escapism, Esthetics, and Education. During the  Yokai Street created a variety of shopping, visual design, dining and snacks, and events assets that are related to yokai and provided two of the experience aspects, entertainment and esthetics. These assets include those shown in Table 3.1.

![Table. 3.1: Table of All the Known Assets Yokai Street has Created](assets/yokai-merch-table.jpg)

The majority of these assets seen in Table 3.1 stressed Yokai Street’s esthetic and entertainment aspects since most of the assets did not involve active participation. Visitors were only able to absorb or immerse themselves in the experience. An example of an asset that Yokai Street created to emphasize entertainment was the Yokai parade. Visitors are able watch and absorb the actions and movements of the Yokai, but they are not able to participate in the action themselves. To emphasize esthetics, Yokai Street added banners, statues, and speakers to the street which provided visual appeal and ambiance for visitors to immerse themselves in, creating an esthetic experience. 

![Fig. 3.2: Old Yokai Candy Tins](assets/yokai-tin.jpg)

However, some of these assets have begun to disappear, and the strength of the brand is waning. Two dining and snack assets, the yokai candy tins, as seen in FIG and the yokai ice cream were discontinued and are no longer sold. These retro candy tins seen in Fig 3.2 are examples of yokai assets that are currently being overlooked, and could be reinstated to strengthen the yokai brand. In addition, many of the assets shown in Table 3.1 have visbility and accessibility problems that are blocking tourists from consuming the yokai brand. Even though the yokai brand is weakening, the existing and previous assets provide a strong foundation to build the yokai brand.

### Demand

Even though the yokai brand is disappearing along Yokai Street, there is still demand for yokai culture among tourists. During our many walks down Yokai Street, we observed visitors searching for yokai statues and taking pictures of them. In addition to these observations, we have also found several signs translated into English to explain that the yokai figures were “Not For Sale” as seen in Fig 3.3. These observations demonstrate that visitors are not only interested in the yokai but are also willing to buy items related to them.

<div class="ui segment piled figure"><div class="image"><img class="full-width" src="assets/yokai-not-for-sale.jpg" style="max-height: 65vh"></div><div class="figure-caption"><p><br>Fig. 3.3: Small Yokai Models on Display Inside Meister Bakery</p>
</div></div>

##### Tourist Motivations for Visiting Japan

To confirm our observations that seemed to indicate interest in yokai consumption among tourists, we created a tourist survey and surveyed foreign tourists at Kyoto Station. The purpose of the survey was to determine their reasons for visiting Japan and Kyoto, their interest in yokai, and their expectations of Yokai Street. The full content and results of this survey can be found in Appendix B, and are summarized in the following text and Fig 3.4.

![Fig. 3.4: Infographic of Tourist Survey Results](assets/figures/tourist-interest-summary.png)

The most popular reason for visiting Japan was tourism, which was cited by 23 out of 30 respondents, as seen in Fig 3.5. Other reasons included work-related reasons (1 out of 30), study-related reasons (3 out of 30), honeymoon (2 out of 30), family visit (1 out of 30), and culture (1 out of 30). The significant amount of foreigners visiting for purely tourism reasons is unsurprising, and demonstrates that there is a significantly sized potential market for Yokai Street tourism.

![Fig. 3.5: Pie chart depicting why people are visiting Japan.](assets/Japan_visit_piechart.png)

Reasons for visiting Kyoto were more diverse than reasons for coming to Japan. The most popular response was culture tourism (14 out of 30), as seen in Fig 3.6, which was followed by sightseeing (8 out of 30), history (6 reponses), autumn foliage (3 out of 30), part of a recommended travel plan (3 out of 30), family visit (2 out of 30), study-related reasons (2 out of 30), enjoyed a previous visit to Kyoto (2 out of 30), and work-related reasons (1 out of 30).

![Fig. 3.6: Pie chart depicting why people are visiting Kyoto.](assets/Kyoto_visit_piechart.png)

The top three reasons cited for visiting Kyoto- culture tourism, sightseeing, and history could all be applied to Yokai Street as a tourist destination. Yokai Street as a themed shotengai incorporates both local Japanese shopping culture and Japanese yokai culture. The handcrafted yokai statues have been praised as quirky and unique by internet reviews and blog posts about visits to Yokai Street, incorporating a unique sightseeing element to visiting Yokai Street. In addition, the street has an interesting history as the site of ancient Yokai myths and as the once bustling transportation center of Northwest Kyoto. The local shrines and temples also have their own unique history, contributing to the potentially rich historical offerings of the street. 

##### Tourism Potential of Yokai Street

As discussed in the introduction chapter, "Yokai Street and Regenerating Shotengai", the yokai-inspired pop culture is abundant worldwide. Questions 5, 6, and 7 of the tourist survey [Appendix B] were intended to gauge tourists’ interest in yokai inspired pop culture, yokai culture, and visiting a yokai themed shotengai. When asked to indicate interest in a list of yokai inspired pop culture exports, a majority of respondents (27 out of 30) said they were interested in at least one of the animes listed, as seen in Fig 3.7.

![Fig. 3.7: Tourist interest in pop culture](assets/pop_culture_interest.png)

While interest in yokai inspired pop culture among respondents did not always translate to an interest in yokai, the majority of respondents, 18 out of 30 said they had in interest in Japanese spirits and ghosts, and there was a direct correlation between those who said they were interested in at least one of the pop culture franchises listed and those who said they were interested in Japanese spirits and ghost, demonstrating that Yokai have a lot of potential among fans of yokai inspired pop culture.

In addition, every respondent that said they were interested in Japanese spirits and ghosts said they would be interested in visiting a yokai themed shotengai located near one of their already planned destinations. Overall, 21 out of 30 respondents said they would be interested in visiting such a shotengai, demonstrating there is a clear potential market for Yokai Street among foreign tourists. 

##### Expectations

Tourists want to consume the history and culture of Yokai Street. In question 8 of the tourist survey [Appendix B], we asked tourists if they had any expectations for a visit to Yokai Street. Of 30 responses, 10 specificaclly asked to see yokai branded items and merchandise, 6 respondants asked to see unique culture on Yokai Street, and 4 respondants wished to learn more about the history of the yokai and Yokai Street itself. The remaining ten respondants were unsure what they expected to see within Yokai Street. These responses matched with the previous data about their motivations and interest in yokai since 29 respondants had interests in pop culture related to yokai and our previous observations showed that tourists were eager to obtain similar merchandise which explains why there were many expectations for merchandise. Fourteen respondants also came to Kyoto specifically to enjoy and immerse themselves within its culture and 6 respondants came to learn about Kyoto's history which justify why people were interested in the culture and information aabout our Yokai respectively.

These results confirmed that tourists are visiting Kyoto for its culture and sights. They have a strong interest and couriosity in Yokai, and they expect to see merchandise, unique culture, and history of the yokai on a street branded with yokai. With this information in mind, we created our recommendations to appeal to tourists' motivations and interests and match and build upon their expectations.

### Infrastructure

Yokai Street has many infrastructural assets that can act as a foundation for branding revitalization. This infrastructure includes:

- Hachimanya Wonder Shop
- Yokai Statues
- Yokai Map

This infrastructure can be used to inform our recommendations for reviving the yokai brand to create an immersive experience.

## Recommendations

When generating our recommendations we categorized yokai brand assets into four categories: shopping, visual design, dining and snacks, and events and activities. In addition, we emphasized the creation of assets that create tourist participation to add escapism and educational experience aspects to Yokai Street. Our recommendations also took into account the motivations, interests, and expectations from the responses of our tourist survey, as well as the remaining infrastructure and our observations to support our recommendations.

### Events and Activities

#### Yokai Hunt

We recommend collaborating with the shop association to create a Yokai Hunt. The Yokai Hunt would be an activity which tasks visitors of all ages with “hunting down” the various yokai along the street. As incentive for completing this activity, visitors will scan a nearby Quick Response code (QR code) on or around each yokai and receive a free Yokai Street LINE sticker. This process and an example of the Yokai Line Stickers is illustrated in Fig 3.8 and Fig 3.9.

![Fig. 3.8: Process of Locating Yokai, Finding the QR Code, and Scanning the Code with LINE](assets/yokai-qr-code.jpg)

<div class="ui segment piled figure"><div class="image"><img class="full-width" src="assets/regen2.jpg" style="max-height: 75vh"></div><div class="figure-caption"><p><br>Fig. 3.9: Yokai LINE Stickers!</p>
</div></div>

##### Quick Response Codes

A QR code, according to DigitalGov, is a two-dimensional code that can be scanned by a mobile device to provide the user with additional information. Since QR codes can be scanned with LINE, they are the fastest and easiest way to share these stickers. In Appendix D, we have several test QR codes that also link to our mapping program, Yokai Magnet Design, and Yokai Tin Design (DigitalGov, 2013).

##### LINE Stickers

LINE stickers are pictures used in LINE, a communication app popular in East Asian countries. LINE stickers are popular for being cute or funny and are often sold by businesses and communities to promote themselves. This activity is aimed to increase active participation of tourists and attract more young adults and teenagers who enjoy using LINE stickers.

To set up this activity, the organizer needs to know how to create LINE stickers and how to make a QR code that shares LINE stickers. To create the stickers, the creator needs to download an app called LINE Creators Studio. After installing the app, the creator can upload and edit pictures of the yokai and submit them for review. After receiving permission to sell the stickers, “share with friends” link can be created and attached to a QR code. To attach the link to a QR code, search up a QR code generator on google and select the “URL” QR code generator. To create the QR codes, we used a website called the “QR code generator”. After generating the QR code, it can be printed out and placed the corresponding Yokai. Additionally, we recommend working with Kono-San, local artists, or volunteers to help create the sticker designs.

The yokai hunt recommendation is based on the lack of participation on Yokai Street. As seen in Table 3.1, Yokai Street mainly has assets that provide esthetic and entertainment aspects. Both of these experience aspects do not involve the participants. The yokai hunt is an activity which will create participation and immersion with our brand. 

#### Seasonal Changes to LINE Stickers
We suggest convincing the association to change yokai LINE stickers in accordance with various seasons or months. Some examples of yokai seasonal stickers is a picture of a fish yokai swimming around Lily pads in the spring, swimming through waves by the sea shore in the summer, swimming around sinking leaves in Autumn, and resting under ice in the winter.

We recommend this event because we have observed that at around 1100 to 1300, a large crowd of high schoolers pass through Yokai Street, in addition to the swarms of students that visit Kitano-Tenmangu every day. To attract this demographic, we created the yokai hunt and LINE stickers, but in order to hold onto their interest we need to create scarcity through seasonal stickers because scarcity creates desire (Cremer, 2018). By enticing these crowds, the shops will also recieve more visitors who will browse the shops and spread information about Yokai Street. Increasing traffic will also help improve visibility of Yokai Street. Overall, this recommendation will draw customers and visitors while advertising yokai street by mouth.

### Yokai History Plaques 

We recommend convincing shop-owners to give each of their yokai statues a small plaque with a short explanation of the yokai’s origin story, similar to the sign shown in Fig 3.10. This plaque could also display the yokai’s LINE QR code from the previous recommendation. Most of the yokai statues are related to some folklore or tale from Japanese mythology, but some shops have taken a creative twist and created yokai related to the shop. 

![Fig. 3.10: Daishogun Hachijinja Historical Plaque](assets/daishogun-plaque.jpg)

The purpose of adding plaques with information about the yokais' and explaining their origin, whether they have a long or short history, will be used to meet tourists' expectations for yokai history that we defined in our discussion of the tourist survey. By adding this feature, Yokai street is able to have an asset that provides an educational aspect.

#### Junior Ranger activity	

We recommend creating an activity that is similar to the junior ranger program used by National parks within the United States. According to the National Parks Service, the Junior ranger program provides young visitors with booklets that have tasks that they need to complete by exploring the park. After bringing the booklet back and going over their experience with a senior ranger, the visitor will receive a badge and certificate for becoming a junior ranger (National Parks Service, 2018).

Similar to the Yokai Hunt, we recommend convincing the shop association to host this activity. In the booklets of tasks, the Junior Yokai Hunters should be tasked with activities such as spotting or drawing certain yokai. If the Yokai History Plaques are setup, the the booklet could also have questions about the information and history of the yokai. Examples of tasks include:
- How many Yokai are there?
- Find and draw the Lantern yokai.
- Talk about a yokai with a cool past!

These tasks should be easy, fun, and diverse so that children will be able to complete the activities and enjoy their exploration of Yokai Street.

This activity is designed to attract parents and children who want their children to learn while having fun. This activity provides Yokai street with an additional educational experience aspect. While this activity is aimed at young visitors, demographics of all ages can also participate.

### Shopping

As demonstrated by our research on the consumption of yokai culture in Kyoto, tourists want to **consume** the yokai brand through merchandise. The following recommendation examines how to create new merchandise to engage tourists with the yokai brand.

#### Partner with a Provider to Create Small Yokai Branded Items: 

In order to create additional yokai items, we recommend finding a provider that can create a variety of custom items, offer a discount bulk orders, and can ship to Japan.

Some providers include:
- Zazzle
- Cafepress
- Spreadshirt
- Teespring
- My Locker
- CustomInk
- Customan

Using Zazzle, we created a sample item that could be sold as a Yokai magnet as seen in Fig 3.11.

![Fig. 3.11: Sample of Yokai Magnet](assets/Zazzle-Magnet.jpg)

To create and customize a magnet like the sample, visit Zazzle’s main website and hover over the “create your own,” tab. After another menu appears, click upon the “All Create Your Own” link and search for “magnets”. Afterward, choose which magnet to use and start designing. As seen in Fig 3.11, we created a simple and straightforward example of a Yokai branded magnet. For other designs, we recommend working with Konno-san but creating designs though Zazzle is made to be simple. Additionally, pre-made pictures can be uploaded onto the site and used as the design and with this feature, Yokai Street’s logo can be added to any merchandise.

Additionally, we recommend restoring the Yokai candy tins. To do restore this item, we recommend convincing the tea shop and the cracker store, which are two shops along Yokai Street that sell hard candy, to repackage their candy. The tea shop even sells small hard candies that are related to yokai. If those shop owners can be convinced to change the packaging of their candies to tins, then the brand yokai candy tin can be restored. To create the tins, unless there is already known supplier to get custom candy tins, our group recommends working with the previously chosen provider to create these items. In Fig 3.12, we created another sample with Zazzle to portray a possible yokai candy tin.

![Fig. 3.12: Sample of Possible Yokai Tin](assets/Zazzle-Tin.jpg)

These items will be aimed to attract tourists since, as seen in the Tourist Survey Results, many tourists had expectations to see these types of merchandise and small items. Our observations and results also support that tourists would be willing to buy these items, as seen in Fig 3.3. These items should be sold by various shops, but Hachimanya's Wonder shop will be the central hub that offers all of these branded items.

### Visual Design

#### Tourist Map

We recommend updating the Yokai Street tourist map to include all of the current shops on Yokai Street. Currently, they have an outdated map with only Yokai Street association shop owners on it. Furthermore, there is little to no English, which our team could only use with a heavy dependence on Google Translate. To create a new tourist map we generated two maps, seen in Fig 3.13 and Fig 3.14, which was tested with preliminary user experience testing. Full data from this testing can be found in Appendix C. The most important findings from this user testing is summarized below:

- 3 out of 4 test users preferred a map with a top-down view
- 4 out of 4 test users liked color-coding of assets
- 3 out of 4 test users liked landmarks and geographically accurate streets
- 4 out of 4 test users wanted more visual cues to find where they are

Fig 3.3 shows our revised tourism map, which is compared to one of our tested drafts in Fig 3.14. In response to user feedback, the final draft incorporated a top-down view, color-coded assets, large landmarks such as temples and shrines, geographically accurate shapes of streets, and more visual cues. These visual cues included Japanese shop names that match shop signs in Japanese and illustrations of yokai statues found throughout the street.

![Fig. 3.13: Recommended Tourist map](assets/yokai-tourist-map-v3.png)

![Fig. 3.14: Unused Tourist map Draft](assets/yokai-tourist-map-type-2.jpg)

Within our final tourist map, we aimed to only display information relevant for tourists and visitors coming to Yokai Street. This information included:
- Shops
- Restaurants
- Nearby Transportation Hubs
- Landmarks
- Lodging

We highlighted these points because tourists and visitors who use this map want to know where they can eat, shop, and rest on Yokai Street. Tourists and visitors also need information about nearest transportation hubs to integrate Yokai Street into their other plans. Another feature of this tourist map is a logo to tell tourists where they can buy yokai souvenirs and merchandise because it is currently difficult for tourists to find where to buy yokai related goods.

The purpose of a new tourism map is to address the visibilty and accessibility problems on our street. By updating the tourist map, Yokai street will be easier to navigate and will help tourists find the merchandise and activities within our street. Our tourist map is aimed towards English speaking tourists because Yokai street is trying to attract tourists, but have very few maps and signs with English. Trying to read maps and signs in foreign languages can be stressing and tedious, which is why we recommend updating the tourist map to simplify this process. 

### Dining and Snacks

#### Food Map

We recommend creating a map that solely locates all of the different places that visitors can eat. These places will include the area near and around Yokai Street. On this map, we also recommend highlighting all of the shops with Yokai related food like restaurant Inoue’s Yokai ramen and Yamada’s Deep Fry Shop’s fried yokai curry balls. This will bring visibility to shops with yokai branded food and incentivise other restaurant or food related stores to create Yokai branded food items. This map will become an asset that contributes an educational aspect since it will provide visitors and tourists that are unfamiliar with the area with an asset that requires active participation and absorption of information to find the restaurant that appeals to the user.

#### Restaurant Loyalty Incentives

We recommend convincing shop-owners to work with one another to create incentives that would attract people to come again. Some possible incentives include creating a point system for buying food on Yokai Street, discounts for food should they return, and offering a free drink or meal after buying a certain amount of food or drinks. The purpose of these incentives is to encourage customers to continue coming back to that they could receive the reward or the prize for continued patronage. By offering these incentives, more people will be encouraged to come. Students and young adults will be especially attracted to the thought of receiving "free food for buying food."

### Shop Association Merits
The shop association currently focuses on helping the local community around Yokai street, but we recommend convincing them to help and provide services to shops within the association. In addition, they should change their practices to lower the threshold for new shops to participate in the Yokai Brand. Additionaly, new shop-owners should recieve free benefits when they join instead of being offered costly benefits even after joining the association. Some ways that the shop association implement these practices is through the following recommendations.

<div style="page-break-after: always;"></div>

#### Maintaining the brand
Instead of having all the shops order branded items serparate from each other and in small amounts, the Shop association could buy the merchandise in bulk. This offer is beneficial since many providers offer a discount on bulk orders. After having the order is delivered, the shop association could store and sell these products back to the shops at a lower rate than they would with the provider and not require the shops to by the merchandise in bulk.

#### Providing Free Statues
Currently, to get a yokai statue, shop-owners have to pay for a statue from the yokai association or other sources. We recommend convincing the Yokai association to provide old yokai statues and possible even new ones for free to **every** single shop-owner on the street, regardless of association membership. The reason we recommend creating free yokai statues is because  the street needs more consistant branding and the shop association is a limiting factor to the spread of the yokai brand. As seen in Fig 3.15, the yokai statues are mainly clumped within the area around the center of Yokai Street. While density is immersive, we also want to have yokai statues near the beginning and end of the street. This will lengthen the Yokai Street experience and aid the street visual appeal as seen in Chapter 4.

![Fig. 3.15: Map of all the Yokai's locations](assets/effurt-annotated-yokai-statues.jpg)

<div style="page-break-after: always;"></div>

#### Continue Seasonal Maintenance of LINE Stickers
After creating the first Seasonal LINE stickers, we recommend appealing to the shop association to continue the maintenance of these seasonal stickers. The shop association should maintain these stickers since the yokai LINE sticker are part of Yokai Street's branding. Additionally, to maintain consistancy and cohesion, the shop association will gemerate the LINE stickers so that the stickers art styles every season are not distinctly different. To design the stickers, the association should offer local artists free advertising for that season through the yokai sticker they could provide. Other wise we recommend trying to work with Konno-san to create the seasonal picture of the yokai.

#### Host Short Term Events
Another recommendation that should be offered to the shop assocaition is creating a smaller connecting flea market in conjunction with Kitano-Tenmangu's flea market on the 25th of every month. As discussed in detail in Chapter 4, this flea market will fill the unused public space near Yokai SOHO. To attract vendors, the shop association could offer potential vendors a free trial period for the space, with the stipulation that they must also offer yokai branded items within their stall.

## Conclusion

The yokai brand has begun disappearing in the past few years, despite continued demand for yokai. To strengthen the brand, we have generated various recommendations to help revive the brand and create an immersive experience for visitors to enjoy. Yokai Street must implement change and strengthen their brand in order to create a cohesive, Yokai Street experience for tourists to enjoy.
