# The Aura of Yokai Street: Regeneration through History

> The **Aura** of an object or place is the combination of its cultural and personal significance for a user or group of users. (MacIntyre, Bolter, & Gandi, 2004, Presence and the Aura of Meaningful Places)

Yokai Street is steeped in history: The western entrance of the street originally began where the Daishogun Hachijinja Shrine stands, its eastern entrance was once dominated by the iconic Kitano-Tenmangu Shrine, and Yokai Street is only twenty minutes south by foot of the picturesque Kinkaku-Ji and Ryoan-Ji.

![Fig. 2.1: Graph depicting the common reasons foreigners visit Kyoto, as well as whether or not they were interested in various aspects of Yokai Culture, Yokai, and Yokai Street. Additional information about this survey is available in Appendix B.](assets/figures/tourist-interest-summary.png)

Through our research shown in Fig. 2.1, we learned that many of the foreign tourists in Kyoto are interested in visiting historical landmarks similar to the ones surrounding Yokai Street. Additionally, a majority of the tourists we spoke to were interested in some form of Yokai or Yokai-inspired culture.

![Fig. 2.2: Graph depicting the most commong resources tourists reported using when planning their visits to the Kyoto area. Additional information about this survey is available in Appendix B.](assets/figures/tourist-website-summary.png)

However, aside from Kinkaku-Ji and the Kitano-Tenmangu Shrine, North-Northwestern Kyoto isn't very visible to tourists. This visibility is so poor that our findings, shown in Fig. 2.2, indicate only a fraction of the *least* commonly used trip planning resources even *mention* Yokai Street. 

This is a significant problem because tourists desire to consume history. Popular tourist locations in Kyoto satisfy this desire with large amounts of infrastructure designed specifically for aiding tourists in consuming the history of those sites. This infrastructure includes audio tours, walking tours, multi-lingual plaques and pamphlets. 

In this chapter, we review the **historical assets** in the North-Northwestern Kyoto area surrounding Yokai Street, identifying **opportunities** to leverage those assets through **recommendations** to help tourists consume those assets.

## Historical Assets

North-Northwestern Kyoto contains a range of historical assets which regularly attract large amounts of domestic and foreign tourists. In this section, we review three collections of the most signficant assets we uncovered as a part of our research.

### Collection A: Heian-Kyo, The Original Capital of Kyoto

<div class="ui segment piled figure"><div class="image"><img class="full-width" src="assets/old-kyoto-map.jpg" style="max-height: 75vh"></div><div class="figure-caption"><p><br>Fig. 2.3: A map of Heian Period Kyoto on display in Daishogun Hachijinja Shrine's museum. Daishogun Hachijinja is marked by a yellow square in the top-left, directly on the top-left corner of the large red rectangle.</p>
</div></div>

![Fig. 2.4: A zoomed-in version of the map of Heian Period Kyoto on display in Daishogun Hachijinja Shrine's museum. The approximate location of Yokai Street is circled in teal.](assets/old-kyoto-map-zoomed.jpg)

As mentioned in the introduction, the Taishogun Shotengai --- now known as Yokai Street --- can trace its history back to the **Heian Period** when **Heian-Kyo**, the city that would become Kyoto, was founded.

The rich history of the location of Yokai Street is an important but *intangible* asset that gives Yokai Street ***Aura***. Aura is created from a combination of location, knowledge, and historical artifacts. Yokai Street can leverage history as an asset for tourism and regeneration because Aura will enhance visitors' experience at this location. 

### Collection B: The Shrines and Temples of Yokai Street

![Fig. 2.5: Annotated EFFURT map showing Yokai Street's shrines and temples. #1 is Jizoin Temple, #2 Joganji Temple, #3 is Daishogun Hachijinja Shrine, and #4 is a Buddhist history museum. Yokai Street's western terminus is at the far-left side of the figure; the eastern terminus is not shown.](assets/effurt-annotated-yokai-religion.jpg)

The western half of Yokai Street is dotted with shrines and temples such as the Jizoin and Joganji Buddhist Temples, a Buddhist Museum, and the Daishogun Hachijinja Shrine as shown in Fig. 2.5. 

The most prominent religious site *on* Yokai Street is the **Daishogun Hachijinja Shrine**. In Heian-Kyo's northwestern corner, this shrine was built in 794 A.D. near the ancient Tenmon, which translates to *"Heavenly Gate"*.  This shrine is dedicated to Daishogun, the ancient "God of the Stars" who presided over direction and was highly revered and worshipped by people in power. This shrine represents the original entrance of Taishogun Shotengai.

<div class="ui segment piled figure"><div class="image"><img class="full-width" src="assets/daishogun-statues.jpg" style="max-height: 65vh"></div><div class="figure-caption"><p><br>Fig. 2.6: One of many statues of the Taishogunate within the Daishogun Museum. Photo taken by team members with special permission from the Daishogun Museum.</p>
</div></div>

Unlike many of the shrines and temples in Kyoto, ***the Daishogun Hachijinja Shrine has stood in the same place since it's creation more than a century ago***. This shrine is as old as Heian-Kyo itself, a feat few other shrines in Kyoto can claim. Daishogun Hachijinja contains a full-fledged museum with artifacts that date back to the Heian Period, a rarity among shrines and temples. These artifacts include celestial globes, ancienct manuscripts, and a collection of more than 80 statues of the Taishogun, shown in Fig. 2.6. These artifacts are now designated as *Important Cultural Assets* by the Japanese government.

<div style="page-break-after: always;"></div>

#### Opportunity: Accessibility and Visibility

Daishogun-Hachijinja's museum is a major asset for attracting visitors, but it is vastly under-publicized and admission is costly with a fee of 500JPY for adults and 300JPY for students.  In addition, the museum is only open twice a year:

- May 1st - 5th, 10:00 to 16:00.
- November 1st - 5th, 10:00 to 16:00.

These factors make the museum highly inaccessible to domestic and foreign tourists alike. Daishogun Hachijinja would become a significant anchor point for drawing more visitors into Yokai Street if it were more accessible to the public.

<div style="page-break-after: always;"></div>

### Collection C: North-Northwestern Kyoto Historical Landmarks
![Fig. 2.7: Annotated EFFURT map showing major historical landmarks in North-Northwestern Kyoto. #1 Kitano-Tenmangu Shrine, #2 is Kinkaku-Ji, and #3 is Ryoan-Ji. Distances from the center of Yokai Street to #2 and #3 are shown in kilometers. Approximate two kilometer radius around Yokai Street's center is shaded in blue.](assets/effurt-annotated-major-history.jpg)

Several significant cultural and historical sites are within a two kilometer radius of Yokai Street, as shown in Fig. 2.7.  These sites include Kitano-Tenmangu Shrine, Kinkaku-Ji, and Ryoan-Ji.

![Fig. 2.8: A view of Kitano-Tenmangu's entrance during the monthly flea market.](assets/tenjin-san-fleamarket.jpg)

Just north of Yokai Street's eastern entrance lies **Kitano-Tenmangu Shrine**, often referred to as *"Tenjin-San"*. This shrine was built to appease the angry spirit of the scholar Sugawara no Michizane in 947 A.D. Kitano-Tenmangu is well-known for its large flea market, which is held on the 25th of every month and is shown in Fig. 2.8.

Kitano-Tenmangu's strong association with the spirit of Michizane attracts domestic tourists from all over Japan who pray for academic success. On the 25th of every February, Kitano-Tenmangu hosts a major Plum Blossom Festival which brings in tourists from all over Japan.

![Fig. 2.9: A view of Kinkaku-Ji from across the pond. (Eastman, n.d.)](assets/eastman-e-unsplash.jpg)

A twenty minute walk north of Yokai Street lies **Kinkaku-Ji**, *"The Golden Pavilion"*, seen in Fig. 2.9. Kinkaku-Ji, the second most popular tourist location in Kyoto, can trace its history back to 1397 A.D. when an old villa was purchased by a shogun and transformed into The Golden Pavilion.

![Fig. 2.10: A view of Ryoan-Ji's Zen garden. (Yanase, n.d.)](assets/yanase-m-unsplash.jpg)

Only a thirty minute walk northwest of Yokai Street is **Ryoan-Ji**. Ryoan-Ji is home to world-famous zen gardens, as shown in Fig. 2.10.  The history of Ryoan-ji traces back to 1450 A.D. when a war-lord had it built on the land of an existing temple, Daiju-In. 

<div style="page-break-after: always;"></div>

#### Opportunity: Integration

<div class="ui segment piled figure"><div class="image"><img class="full-width" src="assets/kyoto-planning-map.jpg" style="max-height: 65vh"></div><div class="figure-caption"><p><br>Fig. 2.11: Showa 4 (A.D. 1929) city planning map (Yano, 2016) of the area of North-Northwestern Kyoto before the Imadegawa-Bypass was created, annotated to highlight key points of interest: The deep blue area denotes Kitano-Tenmangu, the orange line denotes the current location of Imadegawa-Dori, the green line denotes the current location of Yokai Street, and the red area denotes a region that, according to locals, Kitano-Tenmangu may have occupied before the creation of the Imadegawa-Bypass.</p>
</div></div>

Our team was surprised to learn that before Imadegawa-Dori street ran in front of Kitano-Tenmangu, the land area of Kitano-Tenmangu extended all the way from its modern-day location to an area directly in front of Yokai Street, as seen in Fig. 2.11.

## Recommendations

### Create a Local Historical Tourism Pamphlet

Yokai Street has numerous opportunities to improve traffic to the street. In addition to the historical assets covered in this chapter, the findings in each chapter of this report point to Yokai Street having the required infrastructure to support large amounts of tourism:

- Kyoto has marketed itself as the historical capital of Japan, and Yokai Street sits at the entrance of the *original* capital of Kyoto, Heian-Kyo.

- Tourists gravitate towards *specific* historical periods of Japan (e.g., the Tokugawa and Heian periods), and Yokai Street and its historical assets fit into these historical periods.

- Domestic and foreign tourists are already traveling to the North-Northwestern Kyoto area for sites like Kinkaku-Ji, making Yokai Street and its nearby historical assets convenient to visit.

We recommend creating a development plan to promote historical and cultural tourism in the *immediate* area surrounding Yokai Street because of this clear infrastructural opportunity. While development plans are multi-faceted and can take different approaches --- as seen in our final chapter on *The North-Northwestern Kyoto Tourism Plan* --- we propose a simple plan that centers on an informal pamphlet.

![Fig. 2.12: An exerpt from "Hidden Gems of Kyoto" showing information for a specific Shrine. (Miyazawa, Miyazawa, & Hokage, n.d.)](assets/miyazawa-hokage-hidden-gems-shrine.jpg)

We suggest modeling this pamphlet after *"Hidden Gems of Kyoto"*, a twenty page informational pamphlet created by the proprietor of Kyoto's Fire Ramen restaraunt, Masamichi Miyazawa. This pamphlet is stocked at major tourism offices in the Kyoto Station Area and is given to every Fire Ramen customer when they visit, giving it a personal touch. 

This pamphlet digests the information for various lesser-known tourism sites in Kyoto, shown in Fig. 2.12. This information includes:

- Operating hours (in the Westerner-familiar 12 hour clock format)
- Entrance fees
- Location via a QR code linking to Google Maps and an embedded map
- Fun anecdotes describing the site with pictures and text

Importantly, all of this information is **accessible**. The author employed a translator to ensure that all of the information was presented in clear and grammatically correct English. When our team visited Daishogun Hachijinja Shrine's museum for a special tour, we were only able to understand the significance of the artifacts inside because we had both our advisor and sponsor actively translating information from the head preist, who was giving the tour *in Japanese*.

![Fig. 2.13: An exerpt from "Hidden Gems of Kyoto" showing the map of all the 'gems' of Kyoto. (Miyazawa, Miyazawa, & Hokage, n.d.)](assets/miyazawa-hokage-hidden-gems-map.jpg)

In the final two pages of the pamphlet in Fig. 2.13, the author includes a color-coded map showing the locations of all of the sites in Kyoto and a personal anecdote. Of particular interest is a message to the reader on the very last page:

> ... It would be an honor if you shared your story here with people you know. Recommend us on Trip Advisor!!

This anecdote calls the reader to action, asking them to review and publicize the author of the map on Trip Advisor, a major tourism website. This is a strong and subtle way to help improve the *accessibility* of the pamphlet while simultaneously promoting and increasing the visbility of Kyoto Fire Ramen.

![Fig. 2.14: The current Yokai Street tourism map.](assets/yokai_map_landscape.jpg)

If Yokai Street were to create a pamphlet similar to "Hidden Gems of Kyoto", they could build off of the aesthetic of the Yokai Street tourism map seen in Fig. 2.14.

Yokai Street has multiple avenues through which this map could be distributed:

- Install pamphlets at major thoroughfares like Kyoto Station or the nearby Kitano-Tenmangu Shrine.
- Collaborate with nearby experience shops like Kyoto Fire Ramen and Golden Moja Hall to give pamphlets out to customers at the end of their visit.
- Offer pamphlets to customers at shops along Yokai Street, encouraging them to explore more of the street during their visit.

A pamphlet that uses "Hidden Gems of Kyoto" as a model and emphasizes accessibility to foreign visitors who do not speak or read Japanese would help raise visibility of the historical assets along Yokai Street and help Yokai Street leverage its significant infrastructure for tourism.

### Integrate with the Kitano-Tenmangu Flea Market

While Yokai Street has an extremely strong historical connection to Kitano-Tenmangu, our team was surprised to learn that Yokai Street does not participate in the monthly Kitano-Tenmangu flea market.

The flea market descends from Kitano-Tenmangu's current location to just outside Yokai Street. We believe that if Yokai Street were to leverage its historical association with Kitano-Tenmangu, it could integrate itself into the flea market. This action would improve Yokai Street's visibility to tourists and out-of-area visitors.

![Fig. 2.15: Annotated EFFURT map showing locations where pop-up shops could easily be supported during a flea market. The blue-shaded region is Gozen-Dori street, and the green-shaded region is a section of under-utilized space at the entrance to Yokai Street.](assets/effurt-annotated-flea-market.png)

From our geographic surveying activities detailed in our chapter *"Urban Assets"*, we know that Yokai Street has significant, under-utilized infrastructure for supporting "pop-up" shops. These shops are similar to stores found at a flea market. We believe that Yokai Street could leverage this infrastructure, as shown in Fig. 2.15, to do one of two things in coordination with the Kitano-Tenmangu flea market:

1. **Join the Flea Market**: Yokai Street could work with Kitano-Tenmangu to set up stalls as a part of the flea market, pulling visitors down from Imadegawa-Dori to Gozen-Dori. 

2. **Run a parallel Flea Market**: Yokai Street could establish a flea market that runs parallel to the one at Kitano-Tenmangu Shrine so that tourists already in the area could easily pour into Yokai Street to visit its flea market. This would be in addition to the flea market held within Daishogun Hachijinja Shrine four times a year. 

In both cases, Yokai Street's section of the flea market could easily be advertised by Yokai Street at the gates of Kitano-Tenmangu during the regular Kitano-Tenmangu flea market. 

We believe that if Yokai Street were to leverage its historical association with Kitano Tenmangu, it could effectively integrate itself with the existing flea market.  As mentioned in our previous recommendation, this would help Yokai Street raise visibility of its historical assets and tourism infrastructure.