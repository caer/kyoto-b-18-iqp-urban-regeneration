## Bibliography

Amirtahmasebi, R., Orloff, M., Wahba, S., & Altman, A. (2016). Regenerating Urban Land - A Practitioner's Guide to Leveraging Private Investment. *World Bank Group.*

> The Book, Regenerating Urban Land, details several cases in which areas of land had been devastaed due to various reasons and enveloped with urban decay. The book includes the details of what caused the decay in those areas and how each area was able to be regenerated and rebuilt.
    
Ang, S. (n.d.). Image. *Unsplash.* Retrieved September 9th, 2018 from [https://unsplash.com/photos/nq4tcJz77r0](https://unsplash.com/photos/nq4tcJz77r0)

Arnold, J. (n.d.). Yokai Street. *Atlas Obscura.* Retrieved September 2nd, 2018, from [https://www.atlasobscura.com/places/yokai-street](https://www.atlasobscura.com/places/yokai-street)

Balsas, C. (2016). Japanese shopping arcades, pinpointing vivacity amidst obsolescence. *The Town Planning Review.* 87(2), 205-232. DOI: 10.3828/tpr.2016.15

> This journal article was invaluable in giving an overview on the background of shotengai, discussing the recent decline of many shotengai across Japan, and providing possible explanations for this decline. The author also discusses several case studies of the decline and subsequent revitalization attempts of several shotengai across Japan which proved helpful for analyzing specific problems shotengai may face in the modern era.

Bergmann, J.V. (2016). Mixing Data. *Mountain Doodles.* Retrieved September 2nd, 2018, from [https://doodles.mountainmath.ca/blog/2016/07/06/mixing-data/](https://doodles.mountainmath.ca/blog/2016/07/06/mixing-data/)

Brasor, P. (2018, May 5). Japan is struggling to deal with the foreign tourism boom. *The Japan Times Online.* Retrieved October 24th, 2018, from [https://www.japantimes.co.jp/news/2018/05/05/national/media-national/japan-struggling-deal-foreign-tourism-boom/#.W87BfmgzY2w](https://www.japantimes.co.jp/news/2018/05/05/national/media-national/japan-struggling-deal-foreign-tourism-boom/#.W87BfmgzY2w)

Braw, E. (2014). Japan's disposable home culture is an environmental and financial headache. *The Guardian.* Retrieved September 9th, 2018, from [https://www.theguardian.com/sustainable-business/disposable-homes-japan-environment-lifespan-sustainability](https://www.theguardian.com/sustainable-business/disposable-homes-japan-environment-lifespan-sustainability)

Breger, G. (1967). The Concept and Causes of Urban Blight. *Land Economics.* 43(4), 369-376. DOI: 10.2307/3145542

Brumann, C. & Schulz, E. (2012). Shrinking cities and liveability in Japan. In *Urban Spaces in Japan: Cultural and Social Perspectives* (Chapter 11). Retrieved September 9th, 2018, from [https://books.google.com/books?id=s5Z-7CYdyN0C&pg=PA208&lpg=PA208&dq=urban%20decay%20in%20japan&source=bl&ots=yqXrZW2M2d&sig=Y1umCC2P2z98p7-pvtcDCJStjoM&hl=en&sa=X&ved=2ahUKEwjz7OfE4qzdAhXDJt8KHSpYBtc4HhDoATACegQIBxAB#v=onepage&q=urban%20decay%20in%20japan&f=false](https://books.google.com/books?id=s5Z-7CYdyN0C&pg=PA208&lpg=PA208&dq=urban%20decay%20in%20japan&source=bl&ots=yqXrZW2M2d&sig=Y1umCC2P2z98p7-pvtcDCJStjoM&hl=en&sa=X&ved=2ahUKEwjz7OfE4qzdAhXDJt8KHSpYBtc4HhDoATACegQIBxAB#v=onepage&q=urban%20decay%20in%20japan&f=false)

> The book, "Shrinking cities and liveability in Japan", discusses issues about urban decay not only across Japan but also worldwide. This was particuarly very useful in understanding Japan's population decline and housing situation.

Cataldo, A. (2018, March 27). Companies try to fit in with millennial's experience economy. *UWIRE Text.* Retrieved October 26, 2018, from [http://link.galegroup.com.ezproxy.wpi.edu/apps/doc/A532507576/AONE?u=mlin_c_worpoly&sid=AONE&xid=61140e78](http://link.galegroup.com.ezproxy.wpi.edu/apps/doc/A532507576/AONE?u=mlin_c_worpoly&sid=AONE&xid=61140e78)

Choi, Y., & Suzuki, T. (2013). Food deserts, activity patterns, & social exclusion: The case of Toyko, Japan. *Applied Geography.*, 43, 87-98. DOI: 10.1016/j.apgeog.2013.05.009

Chavez, A. (2017, August 27). Blame for ‘bad tourists’ to Japan lies with the advice they never receive. *The Japan Times.* Retrieved November 6th, 2018, from
[https://www.japantimes.co.jp/community/2017/08/27/our-lives/blame-bad-tourists-japan-lies-advice-never-receive/#.W9_2K5Mzbb1](https://www.japantimes.co.jp/community/2017/08/27/our-lives/blame-bad-tourists-japan-lies-advice-never-receive/#.W9_2K5Mzbb1)

Constance, K. (n.d.). Image. *Unsplash.* Retrieved September 9th, 2018 from [https://unsplash.com/photos/zIwMVgCY8EA](https://unsplash.com/photos/zIwMVgCY8EA)

Crossley-Baxter, L. (2018). Doomed to depreciate: The limited life span of a Japanese home explained. *Rethink Tokyo.* Retrieved September 9th, 2018, from [https://www.rethinktokyo.com/2018/06/06/depreciate-limited-life-span-japanese-home/1527843245](https://www.rethinktokyo.com/2018/06/06/depreciate-limited-life-span-japanese-home/1527843245)

Destination DifferentVille. (NA). Fun Things to do in Kyoto: Hunt Monsters in Yokai Street. *Destination DifferentVille.* Retrieved November 23rd, 2018, from [https://differentville.com/hunting-monsters-in-kyotos-yokai-street/](https://differentville.com/hunting-monsters-in-kyotos-yokai-street/)

The Economist. (2018). Why Japanese houses have such limited lifespans. *The Economist.* Retrieved September 2nd, 2018, from [https://www.economist.com/finance-and-economics/2018/03/15/why-japanese-houses-have-such-limited-lifespans](https://www.economist.com/finance-and-economics/2018/03/15/why-japanese-houses-have-such-limited-lifespans)

Fukase, Atsuko. (2013, November 23). World News: Japan looks for ways to say it's cool. *Wall Street Journal.* Retrieved October 24th, 2018, from [http://ezproxy.wpi.edu/login?url=https://search-proquest-com.ezproxy.wpi.edu/docview/1460843556?accountid=29120](http://ezproxy.wpi.edu/login?url=https://search-proquest-com.ezproxy.wpi.edu/docview/1460843556?accountid=29120)

GaijinPot Travel. (NA). Yokai (Monster) Street. *GaijinPot Travel.* Retrieved September 23rd, 2018, from [https://travel.gaijinpot.com/yokai-monster-street/](https://travel.gaijinpot.com/yokai-monster-street/)

Google Maps (2018). Ichijo Dori - Kyoto Prefecture. *Google Maps.* Retrieved September 2nd, 2018, from [https://www.google.com/maps/@35.0261649,135.7355765,3a,75y,179.82h,89.68t/data=!3m6!1e1!3m4!1suFORLmTATY-1KhsU6CX9Bg!2e0!7i13312!8i6656](https://www.google.com/maps/@35.0261649,135.7355765,3a,75y,179.82h,89.68t/data=!3m6!1e1!3m4!1suFORLmTATY-1KhsU6CX9Bg!2e0!7i13312!8i6656)

Graham-Smith, D. (2016). The History Of The Tube Map. *Londonist.* Retrieved September 2nd, 2018, from [https://londonist.com/2016/05/the-history-of-the-tube-map](https://londonist.com/2016/05/the-history-of-the-tube-map)

Hajiri, H. (2018). Kyoto shopping street lures visitors with yokai characters. *SFGate.* Retrieved September 2nd, 2018, from [https://www.sfgate.com/lifestyle/article/Kyoto-shopping-street-lures-visitors-with-yokai-12968713.php#photo-15671266](https://www.sfgate.com/lifestyle/article/Kyoto-shopping-street-lures-visitors-with-yokai-12968713.php#photo-15671266)

Hani, Y. (2005). Shotengai. *The Japan Times.* Retrieved September 2nd, 2018, from [https://www.japantimes.co.jp/life/2005/06/12/to-be-sorted/shotengai](https://www.japantimes.co.jp/life/2005/06/12/to-be-sorted/shotengai)

Hays, J. (2009). Urban and Rural Life in Japan. *Facts And Details.* Retrieved September 9th, 2018, from [http://factsanddetails.com/japan/cat19/sub122/item646.html](http://factsanddetails.com/japan/cat19/sub122/item646.html)

Heinricher, D., Kahn, L., Maitland, I., Manor, N. (2013). Ecclesiastical Architecture of Venice: Preserving Convents, Churches, Bells and Bell Towers. *Worcester Polytechnic Institute.* [https://web.wpi.edu/Pubs/E-project/Available/E-project-122013-112739/unrestricted/Venice_B13_Final_IQP_Report.pdf](https://web.wpi.edu/Pubs/E-project/Available/E-project-122013-112739/unrestricted/Venice_B13_Final_IQP_Report.pdf)

> This IQP paper provided an impressive amount of prior art evidence on creating interactive maps based on physical landmarks.

Hendy, R. (2014). Yubari, Japan: a city learns how to die. *The Guardian.* Retrieved September 9th, 2018, from [https://www.theguardian.com/cities/2014/aug/15/yubari-japan-city-learns-die-lost-population-detroit](https://www.theguardian.com/cities/2014/aug/15/yubari-japan-city-learns-die-lost-population-detroit)

Holden, S. (2013). I-turn to the countryside: Pioneering alternative lifestyles. *Sanga Ari.* Retrieved September 2nd, 2018, from [https://samuholden.wordpress.com/2013/04/27/embracing-decline-1-5/](https://samuholden.wordpress.com/2013/04/27/embracing-decline-1-5/)

Holmberg, N. (2018). Leaflet: Make a web map! *Map Time Boston.* Retrieved September 2nd, 2018, from [https://maptimeboston.github.io/leaflet-intro/](https://maptimeboston.github.io/leaflet-intro/)

Horioka, C.Y. (2006). The causes of Japan's ‘lost decade’: The role of household consumption. *Japan and World Economy,* 18(4), 378-400. DOI: 10.1016/j.japwor.2006.03.001

> Horioka gives an overview of Japan's "lost decade" in the context of household consumption rates which was extremely useful in determining effects of Japan's economic struggles on business loss and decline among shotengai districts. The author provides data and graphs on various household consumption and economic trends during the 1990s that give a detailed picture of the effects of the struggling economy on the shopping industry and household opinions towards money-spending.

Hoskin, P. (2015, January 31). How Japan became a pop culture superpower. *The Spectator.* Retrieved October 24th, 2018, from [https://www.spectator.co.uk/2015/01/how-japan-became-a-pop-culture-superpower/](https://www.spectator.co.uk/2015/01/how-japan-became-a-pop-culture-superpower/)
    
Impoco, J. (2008). Life After the Bubble: How Japan Lost a Decade. *The New York Times.* Retrieved September 2nd, 2018, from [https://www.nytimes.com/2008/10/19/weekinreview/19impoco.html](https://www.nytimes.com/2008/10/19/weekinreview/19impoco.html)

Inoue, A. (n.d.). About Taishogun Shopping Street. *Kyoto-Taisyogun.* Retrieved September 2nd, 2018, from [http://kyoto-taisyogun.com/en/about-taishogun-shopping-street/](http://kyoto-taisyogun.com/en/about-taishogun-shopping-street/)

Inquirer.net. (2018, September 9). Japan deals with ‘tourist pollution’ from surges in visitors. *Inquirer.net.* Retrieved November 7th, 2018, from [https://newsinfo.inquirer.net/1030045/japan-deals-with-tourist-pollution-from-surges-in-visitors](https://newsinfo.inquirer.net/1030045/japan-deals-with-tourist-pollution-from-surges-in-visitors)

Introvert Japan. (2015). Why japanese spirituality and religion is, and isn’t, part of life. *Introvert Japan.* Retrieved September 2nd, 2018, from [https://introvertjapan.com/2015/11/02/why-japanese-spirituality-and-religion-is-and-isnt-part-of-life/](https://introvertjapan.com/2015/11/02/why-japanese-spirituality-and-religion-is-and-isnt-part-of-life/)

The Invisible Tourist. (2018, June 25). Overtourism: Is Japan Becoming a Victim of its Own Success? *The Invisible Tourist.* Retrieved November 7th, 2018, from [https://www.theinvisibletourist.com/overtourism-in-japan-tourist-pollution/](https://www.theinvisibletourist.com/overtourism-in-japan-tourist-pollution/)

Ishakawa, M., et al. (2016). Food accessibility and perceptions of shopping difficulty among elderly people living alone in Japan. *J Nurt Health Aging*, 20(9), 904-911. DOI: 10.1007/s12603-015-0694-6

Ishakawa, N. & Fukushige, M. (2015). Dissatisfaction with dwelling environments in an aging society: An empirical analysis of the Kanto area in Japan. *Review Urban &Regional Devel*, 27, 149-176. DOI:10.1111/rurd.12038

> This study presents survey data from households in Japan on reasons for being satisfied or dissatisfied with housing, providing much needed insight into what type of living environments Japanese households prefer. This data helped our group analyze the importance of shotengai presence in neighborhoods, and the effects decaying shotengai may have on their local neighborhood's housing satisfaction.

Ishiguro, K. (2014). Food access among elderly Japanese people. *Asian Social Work and Policy Review*, 8: 275-279. DOI:10.1111/aswp.12032

Ito, M. (n.d.). Shōtengai: Examining the evolution of Tokyo’s venerable shopping districts. *Japan Times.* Retrieved September 2nd, 2018, from [https://www.japantimes.co.jp/life/2018/01/13/lifestyle/shotengai-examining-evolution-tokyos-venerable-shopping-districts](https://www.japantimes.co.jp/life/2018/01/13/lifestyle/shotengai-examining-evolution-tokyos-venerable-shopping-districts)

Iwama, N., et al. (2017). Analysis of the factors that disrupt dietary habits of the elderly: A case study of a Japanese food desert. *Urban Studies*, 54(15), 3560-3578. DOI: 10.1177/0042098016677450

Japan International Cooperation Agency. (2007). Urban land use planning system in Japan. *Japan International Cooperation Agency.* Retrieved September 12, 2018, from [https://jica-net-library.jica.go.jp/library/jn325/UrbanLandUsePlanningSystem_all.pdf](https://jica-net-library.jica.go.jp/library/jn325/UrbanLandUsePlanningSystem_all.pdf)

Japan Property Central. (2014). Understanding the Lifespan of a Japanese Home or Apartment. *Japan Property Central.* Retrieved September 9th, 2018, from [http://japanpropertycentral.com/2014/02/understanding-the-lifespan-of-a-japanese-home-or-apartment/](http://japanpropertycentral.com/2014/02/understanding-the-lifespan-of-a-japanese-home-or-apartment/)

The Japan Times. (2016, May 7). Find an effective tourism strategy. *The Japan Times.* Retrieved November 6th, 2018, from [https://www.japantimes.co.jp/opinion/2016/05/07/editorials/find-effective-tourism-strategy/#.W9-me5Mzbb1](https://www.japantimes.co.jp/opinion/2016/05/07/editorials/find-effective-tourism-strategy/#.W9-me5Mzbb1)

Johnston, E. (2015). Is japan becoming extinct? *The Japan Times Online.* Retrieved September 2nd, 2018, from [https://www.japantimes.co.jp/news/2015/05/16/national/social-issues/japan-becoming-extinct/](https://www.japantimes.co.jp/news/2015/05/16/national/social-issues/japan-becoming-extinct/)

Johnston, E. (2018, September 27). Kyoto lodging tax kicks in on Monday. *The Japan Times Online.* Retrieved October 24th, 2018, from [https://www.japantimes.co.jp/news/2018/09/27/national/kyoto-lodging-tax-kicks-monday/#.W868emgzY2w](https://www.japantimes.co.jp/news/2018/09/27/national/kyoto-lodging-tax-kicks-monday/#.W868emgzY2w)

Kamouni, S. (2016). Ghost Town Rusting theme park rides and rotting streets lined with rubbish: These eerie photos show the abandoned Japanese City where only 9,000 people are left. *The Sun.* Retrieved September 9th, 2018, from [https://www.thesun.co.uk/living/1256110/rusting-theme-park-rides-and-rotting-streets-lined-with-rubbish-these-eerie-photos-show-the-abandoned-japanese-city-where-only-9000-people-are-left/](https://www.thesun.co.uk/living/1256110/rusting-theme-park-rides-and-rotting-streets-lined-with-rubbish-these-eerie-photos-show-the-abandoned-japanese-city-where-only-9000-people-are-left/)

Kim, S. (n.d.). Image. *Unsplash.* Retrieved September 9th, 2018 from [https://unsplash.com/photos/HjsWTyyVDgg](https://unsplash.com/photos/HjsWTyyVDgg)

Kimball, M. A. (2006). London through Rose-Colored Graphics: Visual Rhetoric and Information Graphic Design in Charles Booths Maps of London Poverty. *Journal of Technical Writing and Communication.*

Kohler, C. (2017, June 24). Millennials jump into experience economy. *The Australian,* p.25 Retrieved October 26, 2018, from [http://link.galegroup.com.ezproxy.wpi.edu/apps/doc/A496605858/AONE?u=mlin_c_worpoly&sid=AONE&xid=5080095a](http://link.galegroup.com.ezproxy.wpi.edu/apps/doc/A496605858/AONE?u=mlin_c_worpoly&sid=AONE&xid=5080095a) 

KyotoHyakki.com (NA). Taishogun Shopping Street Yokai Street. *Kyotohyakki.com.* Retrieved November 22nd, 2018, from [http://www.kyotohyakki.com/tai-syo-gung/tai-syo-gung_e.html](http://www.kyotohyakki.com/tai-syo-gung/tai-syo-gung_e.html)

The Kyoto Shimbun. (2018, June 30). Concerns in Kyoto over Tourism Pollution. *The Kyoto Shimbun.* Retrieved November 9th, 2018, from [https://e.kyoto-np.jp/news/20180630/3220.html](https://e.kyoto-np.jp/news/20180630/3220.html)

Larke, R. (1992). Japanese Retailing: Fascinating, but Little Understood. *International Journal of Retail & Distribution Management,* 20(1), 3+. Retrieved from [http://ezproxy.wpi.edu/login?url=https://search.proquest.com/docview/210922751?accountid=29120](http://ezproxy.wpi.edu/login?url=https://search.proquest.com/docview/210922751?accountid=29120)

Lewis, L. & Jacobs, E. (2018, July 14). How insta-bae are you? Unlike their parents, who strove to accumulate things, Japan's millennials are pursuing Instagram-ready moments. *The Financial Times,* p 18. Retrieved October 25th, 2018, from [http://link.galegroup.com.ezproxy.wpi.edu/apps/doc/A546448130/AONE?u=mlin_c_worpoly&sid=AONE&xid=819a1441](http://link.galegroup.com.ezproxy.wpi.edu/apps/doc/A546448130/AONE?u=mlin_c_worpoly&sid=AONE&xid=819a1441)

Li, X. (2017). Visualising Brexit Votes with Leaflet and cartogram. *Xinye Fiddles.* Retrieved September 26th, 2018, from [https://xinye1.github.io/projects/brexit-cartogram-leaflet/](https://xinye1.github.io/projects/brexit-cartogram-leaflet/)

Life & Culture Information Newsletter. (NA). Life In Kyoto. *Life & Culture Information Newsletter.* Retrieved November 22nd, 2018, from [http://lik.kcif.or.jp/archives/1208/08_2012en.htm](http://lik.kcif.or.jp/archives/1208/08_2012en.htm)

London School of Economics and Political Science. (2018). Charles Booth’s London. *London School of Economics and Political Science.* Retrieved August 24, 2018, from [https://booth.lse.ac.uk/map](https://booth.lse.ac.uk/map)

Luhn, A. (2016). Slow-motion wrecks: How thawing permafrost is destroying arctic cities. *The Guardian.* Retrieved September 2nd, 2018, from [https://www.theguardian.com/cities/2016/oct/14/thawing-permafrost-destroying-arctic-cities-norilsk-russia](https://www.theguardian.com/cities/2016/oct/14/thawing-permafrost-destroying-arctic-cities-norilsk-russia)

> The article, Slow-motion wrecks: How thawing permafrost is destroying arctic cities, discusses the issue of melting permafrost destroying entire neighborhood in Norilsk, Russia. This disaster is also causing urban decay to slowly envelope the city and how it is effecting the city as a whole.
    
Matsura, T. & Sunada, M. (2010). Welfare Assessment of New Retail Formats: Evidence from Japan's Retail Industry. *Keio/Kyoto Joint Global COE Discussion Paper Series 2010-018*. Retrieved from [https://ideas.repec.org/p/kei/dpaper/2010-018.html](https://ideas.repec.org/p/kei/dpaper/2010-018.html)

McConchie, A., & Schnechter, B. (n.d.). Anatomy of a Web Map. *Map Time.* Retrieved September 2nd, 2018, from [http://maptime.io/anatomy-of-a-web-map](http://maptime.io/anatomy-of-a-web-map)

McCurry, J. (2018, June 15). 'Tourism pollution': Japanese crackdown costs Airbnb $10m. *The Guardian*. Retrieved October 24th, 2018, from [https://www.theguardian.com/world/2018/jun/15/tourism-pollution-backlash-japan-crackdown-costs-airbnb-10m-kyoto](https://www.theguardian.com/world/2018/jun/15/tourism-pollution-backlash-japan-crackdown-costs-airbnb-10m-kyoto)

McGray, D. (2002, May). Japan's gross national cool. *Foreign Policy,* 44-54. Retrieved October 24th, 2018, from [http://ezproxy.wpi.edu/login?url=https://search-proquest-com.ezproxy.wpi.edu/docview/224036374?accountid=29120](http://ezproxy.wpi.edu/login?url=https://search-proquest-com.ezproxy.wpi.edu/docview/224036374?accountid=29120)

Murakami, K. & Yoshikura, A. (2015). Yokai Street. *The Kyoto Project.* Retrieved September 2nd, 2018, from [http://thekyotoproject.org/english/yokai-street/](http://thekyotoproject.org/english/yokai-street/)

Nashima, M. (1997, Aug.). Old-style shopping arcades struggling to survive. *Japan Times*. Retrieved September 9th, 2018 from [https://search-proquest-com.ezproxy.wpi.edu/docview/218920603?rfr_id=info%3Axri%2Fsid%3Aprimo](https://search-proquest-com.ezproxy.wpi.edu/docview/218920603?rfr_id=info%3Axri%2Fsid%3Aprimo)

> This newspaper article from 1997 provides a snapshot of the struggles of shotengai in 1997. The article describes the effects of declining business in a couple example shotengai, as well as a revitalization project to strengthen business at a shotengai in Osaka. The article provides insight into Japanese opinions on shotengai revitalization as it includes several valuable interview quotes from a local businessman as well as from Japanese ministry officials.

OPEN Glasgow. (2014). Urban Asset Mapping. *OPEN Glasgow.* Retrieved September 2nd, 2018, from [http://open.glasgow.gov.uk/datastories/urbanassetmapping/](http://open.glasgow.gov.uk/datastories/urbanassetmapping/)

Pine, J. & Gilmore, J. (1998). Welcome to the Experience Economy. *Harvard Business Review.* Retrieved October 29th, 2018, from [https://hbr.org/1998/07/welcome-to-the-experience-economy](https://hbr.org/1998/07/welcome-to-the-experience-economy)

Pizam, A. (2010). Creating memorable experiences. *International Journal of Hospitality Management,* 29(3), 343. DOI: 10.1016/j.ijhm.2010.04.003

Ryall, J. (2017, September 9). 'Pollution by tourism': How Japan fell out of love with visitors from China and beyond. *South China Morning Post.* Retrieved October 24th, 2018, from [https://www.scmp.com/week-asia/society/article/2110388/pollution-tourism-how-japan-fell-out-love-visitors-china-and](https://www.scmp.com/week-asia/society/article/2110388/pollution-tourism-how-japan-fell-out-love-visitors-china-and)

Ryall, J. (2018, September 11). Japan tourism: The polite Japanese are finally getting sick of tourists. *Traveller.* Retrieved November 6th, 2018, from [http://www.traveller.com.au/japan-tourism-the-polite-japanese-are-finally-getting-sick-of-tourists-h157ik](http://www.traveller.com.au/japan-tourism-the-polite-japanese-are-finally-getting-sick-of-tourists-h157ik)

Sakamaki, S. (1997). Japan's shopping revolution: out-of-town malls make their mark. *International Journal of Retail & Distribution Management*, 25(4-5), 168. Retrieved from [http://link.galegroup.com/apps/doc/A19737334/AONE?u=mlin_c_worpoly&sid=AONE&xid=40817b88](http://link.galegroup.com/apps/doc/A19737334/AONE?u=mlin_c_worpoly&sid=AONE&xid=40817b88)

Sanchanta, M. (2006, March 29). A farewell to corner shops? RETAIL: Mariko Sanchanta reports on the arrival of mammoth US-style malls that have led to fears of a 'hollowing out' of city centres. *Financial Times*, p. 2. Retrieved from [http://link.galegroup.com/apps/doc/A143802652/AONE?u=mlin_c_worpoly&sid=AONE&xid=32fc2c73](http://link.galegroup.com/apps/doc/A143802652/AONE?u=mlin_c_worpoly&sid=AONE&xid=32fc2c73)

Sharing Kyoto. (2018, January 22). Ichijo Yokai Street. *Sharing Kyoto.* Retrieved November 23rd, 2018, from [http://sharing-kyoto.com/see_yokai-street](http://sharing-kyoto.com/see_yokai-street)

Shim, C. & Santos, C. A. (2014). Tourism, place and placelessness in the phenomenological experience of shopping malls in Seoul. *Tourism Management,* 45, 106-114. DOI: 10.1016/j.tourman.2014.03.001

Social Value Lab. (2012). Maybole Community Assets Mapping Study. *Social Value Lab.* Retrieved September 1st, 2018, from [http://www.socialvaluelab.org.uk/wp-content/uploads/2013/05/Maybole-Community-Assets-Final-report.pdf](http://www.socialvaluelab.org.uk/wp-content/uploads/2013/05/Maybole-Community-Assets-Final-report.pdf)
    
> When looking at urban asset mapping, this report published by the Social Value Lab was invaluable in understanding the methodologies used for data collection, and the ways data can be presented to stakeholders.

Sorensen, A. (2006). Liveable cities in Japan: Population ageing and decline as vectors of change. *International Planning Studies,* 11(3), 225-242. DOI: 10.1080/13563470701231703
    
South Seeds. (2017). Development of a community asset on Glasgow’s southside. *South Seeds.* Retrieved September 2nd, 2018, from [http://southseeds.org/wp-content/uploads/2017/09/Developing-a-community-asset-on-Glasgows-southside.pdf](http://southseeds.org/wp-content/uploads/2017/09/Developing-a-community-asset-on-Glasgows-southside.pdf)

Statistics Bureau. (2009). *Statistical Handbook of Japan 2009*. Tokyo: Ministry of Internal Affairs and Communcication. 

Statistics Bureau. (2018). *Statistical Handbook of Japan 2018*. Tokyo: Ministry of Internal Affairs and Communication. 

Taylor, B. (2016). Brexit — a story in maps. *Medium*. Retrieved September 26th, 2018, from [https://medium.com/@jakeybob/brexit-maps-d70caab7315e](https://medium.com/@jakeybob/brexit-maps-d70caab7315e)

Tomasso, P. (n.d.). Image. *Unsplash.* Retrieved September 9th, 2018 from [https://unsplash.com/photos/uThLDG9Y4ms](https://unsplash.com/photos/uThLDG9Y4ms)

Tondo, L. (2018). 50 years since sicily's earthquake, an urban disaster of a different kind. *The Guardian.* Retrieved September 2nd, 2018, from [https://www.theguardian.com/cities/2018/jan/15/sicily-earthquake-1968-50-years-belice-valley-poggioreale](https://www.theguardian.com/cities/2018/jan/15/sicily-earthquake-1968-50-years-belice-valley-poggioreale)

> 50 years since Sicily's earthquake, an urban disaster of a different kind, is an article that details what happened after an earthquake in Sicily devastated the old village, Poggioreale. In response to the catastrophe, the government rebuilt a modern city for the victims. However, the villagers were accustomed to their previous living situation and could not settle down. This resulted in the town becoming abandoned and left to decay.
    
Townsend, A. (2013). Why Japan is Crazy About Housing. *Arch Daily.* Retrieved September 9th, 2018, from [https://www.archdaily.com/450212/why-japan-is-crazy-about-housing](https://www.archdaily.com/450212/why-japan-is-crazy-about-housing)
    
Treist, P. (2017) Build an Interactive Game Of Thrones Map (Part II) - Leaflet.js & WebPack. *Patrick Triest.* Retrieved September 2nd, 2018, from [https://blog.patricktriest.com/game-of-thrones-leaflet-webpack](https://blog.patricktriest.com/game-of-thrones-leaflet-webpack)

Wikimedia Commons. (2014). London Underground Overground DLR Crossrail map zone. *Wikimedia Commons.* Retrieved September 2nd, 2018, from [https://commons.wikimedia.org/wiki/File:London_Underground_Overground_DLR_Crossrail_map_zone.svg](https://commons.wikimedia.org/wiki/File:London_Underground_Overground_DLR_Crossrail_map_zone.svg)

Wikipedia. (NA). Japanese Haunted Towns. *Wikipedia.* Retrieved November 22nd, 2018, from [https://en.wikipedia.org/wiki/Japanese_haunted_towns](https://en.wikipedia.org/wiki/Japanese_haunted_towns)

Yang, Y., & Diez-Roux, A. V. (2012). Walking Distance by Trip Purpose and Population Subgroups. *American Journal of Preventive Medicine*, 43(1), 11–19. DOI: 10.1016/j.amepre.2012.03.015

Yano, K., Nakaya, T., & Kato, H. (2001). A new social atlas of Kyoto based on cartogram systems for small areal (sic.) units. *Department of Geography, Ritsumeikan University*

Zaikei News. (2017, June 19). Tourist Pollution? in Kyoto. *Zaikei News.* Retrieved November 9th, 2018, from [http://www.zaikeinews.com/articles/2344/20170619/tourist-pollution-kyoto.htm](http://www.zaikeinews.com/articles/2344/20170619/tourist-pollution-kyoto.htm)