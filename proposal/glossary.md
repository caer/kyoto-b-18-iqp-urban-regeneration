## Glossary

**Aesthetics**: The visual appearance of an urban asset, often subjective.

**Aesthetic Damage**: Any damage to an urban asset that could affect its aesthetics. Structural damage is not always aesthetic damage, and vice-versa.

**Structural Damage**: Any damage to an urban asset that could affect its safety or functionality for occupants. Aesthetic damage is not always structural damage, and vice-versa.