## 2. Background

### 2.1 Case Studies of Urban Decay and Regeneration

Urban decay is characterized by the depreciation of a functioning city or a section of a city (Breger, 1967). In this section, we walk through several case studies in order to understand the most common indicators of urban decay, and subsequent strategies for reversing that decay via urban regeneration.

#### 2.1.1 Case Study: Seoul

According to Amirtahmasebi R., Orloff M., Wahba S., and Altman A., the city of Seoul, South Korea, experienced a 52% population decrease between 1975 to 1995 due to poor urban planning and suburbanization. In order to improve transportation, a highway overpass was built over the downtown area, however the buildings placed underneath the overpass were mainly rentals with sub-standard conditions which caused a large number of people to move out to the suburbs. As a result, the local population consisted of people with poor educational backgrounds and squatters. Due to these factors, the downtown area of Seoul became decrepit and decayed. (Amirtahmasebi R., Orloff M., Wahba S., & Altman A., 2016)

In order to restore Seoul’s Downtown area, the city removed the highway overpass and revitalized the nearby Cheonggyecheon stream. The restoration project cost about 25.2 billion USD, but the result was outstanding. The project accomplished the creation of new public spaces and a pleasant natural environment for the public to enjoy. Downtown Seoul became completely revitalized and began attracting many people and businesses. Thanks to the project, Downtown Seoul became a popular place to live and a highly desired place for businesses. The revitalization brought people and jobs to the area and successfully regenerated the area. (Amirtahmasebi R., Orloff M., Wahba S., & Altman A., 2016)

**Indications of decay in this study**:
- Dilapidated buildings
- High crime rates
- Increased noise level
- Pollution
- Poor urban planning
- Suburbanization

**Regeneration strategies used in this study**:
- Cleaned up pollution (Cheonggyecheon stream)
- Re-urban planning (removed highway overpass)
- Revitalized public spaces

#### 2.1.2 Case Study: Santiago

The municipal district in Santiago, Chile lost 50% of its population between 1952 to 1992 due to suburbanization, high crime rates, pollution, and lack of investment in the area, as reported by Amirtahmasebi R. et al. The district began to decline further in 1985 when an earthquake caused major damage to buildings, prompting the private sector to withdraw investment from the area. The continued decline of the district was fueled by suburbanization and rising crime rates in the area. (Amirtahmasebi R., Orloff M., Wahba S., & Altman A., 2016)

To regenerate Santiago’s Municipal District (SMD), the Mayor of Santiago created the Santiago Development Corporation (SDC). The SDC began working on restoring SMD by finding and negotiating prices for all of the property within the SMD. After collecting all of the land, they talked to potential land developers and sold the properties to them. The SDC bought up land that was meant for housing and began development to specifically attract middle class residents. In addition, the SDC started designing large building complexes to meet the housing demand for other demographics. While developing the SMD, the SDC worked directly with the private developers so the demands of the future residents would be met. As a result of the SDC work, the SMD is now a bustling neighborhood with little urban decay. There have been some complaints in regards to the aesthetic design of the housing development, but nevertheless, it fulfills its purpose. (Amirtahmasebi R., Orloff M., Wahba S., & Altman A., 2016)

**Indications of decay in this study**:
- Earthquake damage
- High crime rates
- Pollution
- Lack of investment
- Suburbanization

**Regeneration strategies used in this study**:
- Allocating responsibility to an organization to focus on issue
- Allocating land to potential buyers and to shops once development is finished
- Developing land into housing nearby shops and business

#### 2.1.3 Case Study: Buenos Aires

Amirtahmasebi R., et al, state that Puerto Madres was Buenos Aires’ first port, completed in 1889, but the port was rendered useless and was abandoned due to faulty engineering. As a result a large number of residents left and the area began to decline. In 1918, the city tried to revitalize the area by building a promenade that was named Costanera Sur. This revitalization project brought color back into the area, but water pollution in the 1950’s reversed the progress. This decline caused urban decay in Puerto Madres due to many abandoned and unmaintained houses, deserted factories, and port facilities. (Amirtahmasebi R., Orloff M., Wahba S., & Altman A., 2016)

Puerto Madres was rejuvenated by reconstructing and redesigning the entire port to become an industrial and domestic sector. The project was assigned to Corporation Antiguo Puerto Madero Sociedad Anónima (CAPMSA). In the first stage of the plan, CAMPSA bought and repaired the sixteen port warehouses on the west side of the port. These warehouses were turned into mixed-use houses and were immediately rented out so the project could progress with the income generated by the buildings. After collecting and securing the rest of Puerto Madres, CAMPSA mapped out and began planning how they would reconstruct the rest of the area.(Amirtahmasebi R., Orloff M., Wahba S., & Altman A., 2016)

In the second stage of CAPMSA’s plan, they sold off the west side of Puerto Madres and began selling off the east side. The west side of Puerto Madres was designed to be the industrial side of the district and the east side was to become the domestic side—CAMPSA hoped to create a fairgrounds, exhibition, convention center, and hotel there. In the third stage of CAPMSA’s plan, they sold off the rest of the land on the East side of Puerto Madres and invested into the city to repair and create infrastructure throughout Puerto Madres, including a potable water system, sewage pipes, storm pipes, electricity, telephone, cable television, data transmission, and fifteen kilometers of new roads and street lighting. By the end of these installations, Puerto Madres was reintegrated into the city and officially declared a district once again. (Amirtahmasebi R., Orloff M., Wahba S., & Altman A., 2016)

**Indications of decay in this study**:
- Pollution
- Major business going under(loss of jobs)

**Regeneration strategies used in this study**:
- Allocating responsibility to an organization to focus on issue
- Replanning land usage
- Creating industrial area by selling plots of land and commercial buildings to businesses
- Creating residential district by working with private sector
- Reinstalling infrastructure to improve area

#### 2.1.4 Case Study: Johannesburg

The inner city district of Johannesburg is another city district that has undergone urban decay and regeneration. Amirtahmasebi R., et al, provide that this district is currently the center of economic activity, but was a center of turmoil in the 1950s when the City Council moved to another area of the city. People who worked for the city council followed, bringing many residents out of the Inner City. The City Council also changed parking regulations without making improvements to public transportation to balance out the consequences. The new parking regulations made running businesses in the inner city difficult and many of them moved. Again, the employees of these businesses followed and the inner city lost even more residents. When South Africa established their democracy, a large number of immigrants began to flow into Johannesburg. Upon arrival, the immigrants needed housing and squatted in the abandoned buildings within the inner city. Following this chain of events, the inner city continued to experience urban decay due to depreciating buildings, lack of services, high crime rates, and lack of investment into the district. (Amirtahmasebi R., Orloff M., Wahba S., & Altman A., 2016)

Johannesburg’s inner city was redeveloped by cooperating with a variety of organizations that each focused on one problem throughout the district. The project was developed and run by a local committee that eventually became named as the Johannesburg Development Committee (JDA), and the Central Johannesburg Partnership (CJP). The JDA worked with many different groups to solve a myriad of problems such as lack of residents, decrepit public areas, and high crime rates. (Amirtahmasebi R., Orloff M., Wahba S., & Altman A., 2016)

In order to deal with the lack of residents, the JDA invested in various businesses throughout the inner city to produce jobs and attract more residents. The JDA also collaborated with the private sector to renew public spaces and fix greenery, lighting, and sidewalks. (Amirtahmasebi R., Orloff M., Wahba S., & Altman A., 2016) 

To improve living spaces and buildings, the ministry of finance helped by promising to reduce taxes for residents of the inner city if they created or improved their properties in any way possible. The CJP helped improve the city by introducing City Improvement Districts (CID). A CID is an area in which landowners agree to pay for additional services such as security, urban improvement, litter collection, and design and upkeep of public space.(Amirtahmasebi R., Orloff M., Wahba S., & Altman A., 2016) 

To decrease crime rates, the JDP created a multi-agency crime prevention system that was funded by the private sector and run by local business leaders called Business Against Crime South America. The CJP also worked with the Metropolitan Trading Company to manage informal trading within the CIDs. Since CIDs also help to improve partnerships and marketing, but could not delve too deep into the businesses to help them, they started this joint venture. The Trust for the Urban Housing Foundation (TUHF) was created to provide loans to residents since banks discriminated against residents in the area due to the previous Apartheid Regime. The Better Building Program was also created to sell derelict buildings in the inner city to private sector groups that could afford to run such projects. (Amirtahmasebi R., Orloff M., Wahba S., & Altman A., 2016) 

After the JDA’s and CJP’s efforts, the Inner city of Johannesburg is now a bustling city with a strong property market. The introduction of CIDs greatly helped this process as areas managed by CIDs are in the best condition and some residents now believe the entire city should become one huge CID. Overall, the project was a major success. (Amirtahmasebi R., Orloff M., Wahba S., & Altman A., 2016)

**Indications of decay in this study**:
- City council moving out of area
- Loss of jobs
- Increased crime
- Disrupted transportation system
- Squatters
- High crime rates

**Regeneration strategies used in this study**:
- Allocating responsibility to an organization to focus on issue
- Increased security
- City Improvement Districts (CIDs); increased security, managed public areas, gave basic services like mail and trash removal, and basic improvement of the local area the CID was designated.
- Tax cuts for residential improvements

#### 2.1.5 Summary of Case Studies

In this section, we took a look at four different case studies of how urban decay was identified and then reversed. In these case studies, two of the most common *causes* for urban decay included:

- **Poor Urban Planning:** If land utilization and transportation are negatively impacted by the layout of a region, large companies may leave (or no longer be attracted to) that region and take their employees with them, lowering the local population and decreasing job opportunities.
- **Poverty and Suburbanization**: Poverty decreases property values, increases drug and gang activity, and fuels suburbanization. Suburbanization, in turn, displaces economically stable residents - who many have been small business owners - leading higher unemployment rates and even more poverty.

Additionally, racial discrimination is frequently cited as a cause of urban decay in case studies and other literature related to urban decay. However, based on feedback from our project advisors, we do not believe that racial discrimination is prevalent in contemporary Japan, nor relevant to our research, so we do not emphasize it in this paper.

Conversely, two of the most common *cures* for urban decay included:

- **Redistribution of land to the private sector:** If undeveloped land is given to companies with enough capital to develop the land into functional and sustainable areas or assets, then the land will naturally attract more jobs and people, driving urban regeneration.
- **Revitalization of public spaces:** If public spaces are revitalized privately or through government efforts, they will naturally attract more people and companies who will live in or invest in the area, again driving urban regeneration.

### 2.2 Urban Decay and Regeneration in Japan

This section talks about urban decay in Japan in general and discusses several contributing factors.

Like many other countries, Japan suffers from urban decay. Although one may think otherwise by simply looking at metropolises like Tokyo, there are other already ‘rotten’ cities that are now merely shells. One such example is Yubari, the infamous now-rusted city once known as the capital of Japan’s coal mining industry, which has lost 90% of its population in the course of 50 years. Yubari was pronounced officially bankrupt from exponential debts in 2007 after its once-thriving mining industry collapsed following a series of tumultuous mining mishaps. Houses, shops and amusement parks can be seen abandoned all over Yubari. The city’s population is said to be Japan’s oldest and stands currently at 9,000. It is estimated that by the year 2020, more of its inhabitants will be over 80 than under 40. (The Guardian, 2014)

![Figure 1: The dilapidated remains of Adventure Slider Kilimanjaro, part of the Coal History Village theme park that closed in 2006. (Hendy, 2014)](assets/hendy-2014-yubari.jpg)

![Figure 2: Yubari in decay. (Kamouni, 2016)](assets/kamouni-2016-yubari.jpg)

Brumann and Schulz discuss in their book *"Urban Spaces in Japan: Cultural and Social Perspectives”* that shrinking population seems to be one of the major contributing factprs to urban decay in Japan. Over a period of almost four decades, starting in the 1960s, there has been a major population shift from high-density urban areas to the suburbs. This pattern of migration is called ‘decentralization’, and is considered to be still ongoing. Consequently, the inner areas of these old urban cities have witnessed a very sharp population decline. This type of migration is particularly prevalent in younger families, as they start to move to the suburbs in hunt for cheaper, newer and less-crowded residential areas as opposed to the overcrowdeded and dramatically high-priced urban residential areas.

As more and more of these families moved from the metropolises to the suburbs, the average ages in central cities started to rise whereas the average ages in the suburbs started to fall. This transformation has left urban cities with the oldest population in the metropolitan areas. Due to the outflow of population, a lot of older shopping areas started to decay; stores have become vacant and prospects have started to decline. The authors, Brumann and Schulz, further explain this in detail, *“Many of the stores that remain are small-scale establishments run by elderly proprietors. It is common for such stores to remain vacant for long periods when they finally close, if no successors are interested in taking on the family business. Many such families are reluctant to sell the property, even if unable to continue the enterprise, so the stores stay vacant and contribute to the decline of the district.”*

The old shopping streets (shōtengai) of such districts have been struggling with decay for decades. The shops started to lose long-time patrons, gradually becoming empty, and the shop owners aged with no successors to pick up their much-valued businesses. The authors believe this is primarily a problem of an ageing society and urban deconcentration. It is a conundrum to revitalize those inner old urban areas where *"plots are small, roads very narrow and the old wooden housing made very much vulnerable to natural disasters, such as earthquakes,"* Brumann and Schulz assert. The authors also state that this conundrum will only enlarge as the issue of population ageing clashes head-to-head with the nationwide population decline.

#### 2.2.1 Spirituality and Religion

The following section discusses the cultural and religious perspectives of the Japanese on urban life. We do acknowledge the fact that the following section tackles controversial topics. We also understand that the content presented is not factual, but rather an insight into a particular point of view and its take on why these matters might be potentially related.

An article from *Introvert Japan* argues that in Japan, religion and culture are often intertwined; the Japanese tend to visit shrines on a regular basis, holding frequent and significant ceremonies involving life and death. For the Japanese, life is largely about meaning and finding purpose, leading to an emphasis on the afterlife.

In his article *"I-Turn to the Countryside"*, Sam Holden notes that, during the 90s, many Japanese citizens migrated to rural areas from metropolises like Kyoto, Tokyo, and Osaka. Despite the modernity of these cities, the Japanese—particularly the elderly—found little meaning in urban life (which is often filled with corruption, crime, arrogance, etc.), which led many of these people to migrate to Japan's rural roots. This migration created three major divisions within Japanese society: U-turners, J-turners, and I-turners (Holden, 2013). 

In his 2009 article about Japanese rural life, Hays provided the meaning behind each of these terms:

- **U-turners:** people who were initially brought up in the country and returned to their homes in the countryside after working in the cities for a certain period of time. 

- **J-turners:** people who fled to different rural areas after becoming fed up with urban life. 

- **I-turners:** people who were originally brought up in the cities who moved to the country immediately after college. A lot of I-turners have learned through their experiences that not enough people are recognizing the underlying flaw of the logic behind urbanization: life loses purpose in a world full of puppets led by civilization and material wealth, where money becomes the ultimate solution to fulfilling the desperate needs of people.

Holden further explains in the article: a lot of I-turners have learned through their experiences that not enough people are recognizing the underlying flaw of the logic behind urbanization—life loses purpose in a world full of puppets led by civilization and material wealth, where money becomes the ultimate solution to fulfilling the desperate needs of people. (Holden, 2013)

*"Many young city dwellers also feel a sense of meaninglessness and insecurity because many of them were born into an era of prosperity and material abundance, so as adults, they show little or no interest in ordinary economic growth or making lots of money relative to other values. Some young people discouraged by dismal job prospects are also turning to rural areas to establish possible future career paths”*, Holden explains. Hence, Japan’s megacities (Tokyo, Kyoto, Osaka etc.) are gradually losing steam as many of these people turn to the countryside as a source of meaning and spirituality. 

#### 2.2.2 Short Lifespans of Japanese Houses

The following section talks about the decpreciation of houses and the causes of expensive land prices in Japan.

In Japan, it is said that the value of an average house depreciates to zero within 20 ~ 25 years. One of the reasons why Japanese houses have such short lifespans is tied to the fact that these houses get torn down and rebuilt every 20 years or so. The reason behind this trend is believed to be closely tied to tradition. (The Economist, 2018)

After WWII, Japan rushed to the decision to reincarnate cities annihilated by the war. This resulted in the mass building of houses that are unsafe and vulnerable to natural disasters. The houses were sloppily built with very low quality, often with wooden frames and lacking proper insulation and seismic protection. Townsend, the author of the article, *"Why Japan is crazy about housing"*, believes that many people consider older homes from this particular period to be abysmal and substandard, and so rather than investing in the maintenance or upgrade of these houses, they started to simply tear them down. This became a trend and possibly the key factor to why people started to develop less and less trust towards homes, especially the second-hand ones. (Townsend, 2013)

![Figure 3: Older, Traditional Homes in Japan. (Crossley-Baxter, 2018)](assets/crossley-baxter-2018-homes.jpg)

However, buildings are still being torn down today even though they may be long-lasting and they are not necessarily uninhabitable. The main reason why these houses are frequently being rebuilt is for security reasons—in a country with frequent natural disasters, the houses are being rebuilt to meet the ever-changing safety regulations to prevent from earthquakes and other natural disasters. (Johnston, 2015). *“The government updates the building code every 10 years due to the earthquake risk. Rather than spending money on expensive retrofitting, people just build new homes.”* as explained in Braw’s article. 

Japanese people started to develop the opinion that brand new homes are safer, less risky and more trustworthy than pre-owned homes. However, a house or a building becomes ‘used’ immediately when the owner moves in. It is said that in some cases, its value plummets by up to 20% at at this time. However, the price of the land on which the house stands often holds its value or gradually increases in time. As a result, in urban regions such as Tokyo where vacant land is extremely scarce, people start looking at old houses with decent blocks of land so that they can demolish the houses and build their dream homes on top of the land they prefer. Therefore, land often gets passed on to families or sold at a good price instead of the house itself, leading to low maintenance of the buildings, and, in turn, lowering the resale value of homes due to flaws and unattractiveness. The scarcity and expensiveness of vacant land leads back to young families looking to start out moving to the suburbs and hence, forming the vicious circle of urban depopulation and suburbanization. (Japan Property Central, 2014)

#### 2.2.3 Economic Growth and the "Lost Decade" in Japan

The following section provides an overview of Japan's rapid economic expansion following World War II, the creation of Japan's bubble economy in the 1980's, the collapse of the bubble economy in 1991, and its effect on household consumption trends.

In the years following the end of World War II, Japan’s economy flourished. Throughout the 1960's, the economy grew at a rate of over 10% yearly (Statistics Bureau, 2018). This growth was made possible through several factors, including expansion of private investment, a growing labor force due to population growth, and an increase in productivity through improving technology (Statistics Bureau, 2018). By the 1980’s, the Japanese economy had become the second largest economy in the world, second only to the United States. In the mid-1980’s, Japan and the United States dominated global markets, accounting for 40% of the global economy combined. In 1986, Japanese economic development began to accelerate even further—from 1986-1991, Japan’s gross domestic product (GDP) increased by $956 billion, about the equivalent of France’s entire GDP at the time. (Impoco, 2008)

Unfortunately Japan’s economic boom did not last. While Japanese manufacturing was skyrocketing, stock prices and real estate prices were also increasing at an alarming rate, creating an economic bubble. Real estate prices were so high in the early 1990s that families were forced to take out multigenerational loans simply to afford housing. Jim Impoco (2008) shares his favorite, and perhaps an even more shocking example of this phenomenon: the Tokyo Imperial Palace property was estimated to be worth the same as the entire state of California. By 1990, Japan's national wealth had reached 3,531 trillion yen as a result of exorbitant stock and land prices, which was equal to a staggering 8 times the GDP at the time (Statistics Bureau, 2018). In 1991, the manufacturing and real estate bubble burst at the same time, resulting in stagnation of the Japanese economy for the next decade, referred to as Japan’s “lost decade” (Impoco, 2008).

![Figure 4: GDP and consumption growth rate trends in Japan over 1983 - 2013. (data via Horioka, 2006)](assets/team-data-via-horioka-2006-2.png)

During the “lost decade” period (approximately 1991-2003), household consumption rates and the GDP growth rate roughly mimicked each other, as shown in figure 4. During this period of time, Charles Horioka (2006) reports that Japanese households spent less on clothing, footwear, transport, miscellaneous goods and services, food, and education. Horioka argues that stagnation in household disposable income and declines in household wealth were the major contributing factors to these trends. Decreased household consumption is likely a contributing factor to the stagnation of business among shōtengai in the 1990’s and 2000’s, especially when coupled with the eruption of many large, competitively priced shopping centers around Japan. (Horioka, 2006)

#### 2.2.4 Transformation of Shopping Culture and the Decay of Shōtengai

This section discusses the Japanese shōtengai and how they have been affected by Japan's fluctuating economy and changes in consumer trends. 

Shōtengai, also known as “shopping streets” or “shopping arcades” are Japanese commercial streets that have long been an integral component of urban Japanese communities. Shōtengai are ubiquitous in Japanese society; rough estimates suggest there are between 14,000-16,000 shōtengai across Japan (Larke, 1992), while a 1997 survey suggested there are over 18,000 shōtengai across Japan (Balsas, 2016).  The concept of the shōtengai dates back to pre-modern Japan, before the invention of the automobile, when shopping was a daily necessity and everyday life required shops located within walking distance (Balsas, 2016). The origins of shōtengai may date back as far as the late 1500’s, when new free-market policies in Japan evolved commercial districts into linear shopping streets (Hani, 2005).

![Figure 5: Togoshi Ginza shōtengai in Tokyo, Japan, one of the largest and most popular shōtengai in Japan. (Ito, 2018)](assets/ito-2018.png)

The shōtengai is a uniquely Japanese commercial district, and differs from a Western-style shopping mall in several key characteristics (Balsas, 2016):

- The shops are typically built separately from one another, leading to an organic diversity of storefront appearances.
- Although the shops are owned and managed separately, shop owners form associations to manage the shōtengai as a whole.
- Shōtengai associations allow the shōtengai to decide on a unified theme or feature for the entire street, including paving, lighting, and even roofs  over the street which unifies all of the shops as a single shopping destination. An example of a shōtengai with an archway and lighting can be seen in figure 5.

##### 2.2.4.1 Shōtengai and Changes in Consumer Trends

In parallel with the economy, shōtengai have grown, changed, and adapted over the past century. Until recently, shōtengai outranked even department stores as the principal type of commercial district in Japan. Many current shōtengai were either built or adapted in the years of the economic boom following the second world war, developing into rich entertainment districts that served equally vibrant communities in the surrounding area (Balsas, 2016). 

Overall, the amount of retail locations, and in particular indendepently operated establishments, which include shops found in shotengai, have been steadily decreasing since the Japanese census conducted in 1985 (Statistics Bureau, 2009). From 1999 to 2007, the number of retail establishments in Japan dropped from 1.4 million to 1.14 million (Statistics Bureau, 2009). As of 2016, that number had fallen to 990,246 (Statistics Bureau, 2018). The proportion of indenpendent retail establishments as opposed to corporate retail establishments has also been falling in tandem to the decrease of retail establishments. In 1958, during the golden age of the shotengai, 90.1% of retail establishments were sole proprieterships. In 2007, this percentage had fallen to 50.3% (Statistics Bureau, 2009), and by 2016 it had fallen even further to 39.2% (Statistics Bureau, 2018). These statistics reflect the reality that many shotengai have been facing: declining business has forced many shops within shotengai to close their doors. 

Charles Balsas (2016) describes how many shōtengai have started to shrink and decay over the past few decades, in response to the economic crash in 1991 and changes in shopping culture influenced by Western nations. A study conducted by the Japanese Ministry of Economy, Trade, and Industry found that 70.3% of shōtengai stores would describe their business as weakening or stagnated. Decreasing household consumption rates during the “lost decade” was not beneficial to shōtengai, and the introduction of large retail stores such as supermarkets and department stores are also to blame for this downward trend in shōtengai business (Balsas 2016). When the first large retail stores started to populate Japan in the 1960s-1980s, a regulatory law called the Large Scale Retail Store (LSRS) Law was put in place to protect shōtengai and the small businesses inside them. However due to pressure from other nations such as the United States, regulations were relaxed in the 1990's and the law was completely repealed in 2000 (Matsure & Sunada, 2010). The following boom of domestic and international chain stores began to draw business away from shōtengai (Balsas, 2016).

Throughout the 1990's, large supermarkets and shopping malls expanded their business across Japan, popping up on the outskirts of towns and cities. From 1992-1997, over 100 large out-of-town shopping centers were built every year in Japan. During this period, retail sales at these types of stores increased 40% while overall retail sales increased only 0.7% (Sakamaki, 1997). These large scale retail companies that expanded in Japan during this period include Daiei, Jusco, Aeon, Ito-Tokado, in addition to US developers such as Koll, World Premiere Investment, Trammel Crow Group, and Western Development which planned to bring giant shopping malls and US brands to Japan (Sakamaki, 1997, Sanchanta, 2006). The growth of large retail corporations in Japan has continued, and corporate managed retail locations currently account for 60.6% of retail locations in Japan as of 2016 (Statistics Bureau, 2018).

The popularity of large supermarkets and shopping centers may stem from the same reasons shōtengai became popular. In a 2015 survey of housing preferences in the Kanto region of Japan conducted by Noriko Ishakawa and Mototsugu Fukushige, 55% of respondants that were considering moving to a new place of residence cited wanting to be closer to shops as a significant factor in that decision. Similarly, 54% of respondants that were content with their current residence cited being close to shops as a reason for not wanting to move (Ishakawa & Fukushige, 2015). This survey data demonstrates that many Japanese residents value convenient, local shopping locations due to the busy, fast-paced nature of urban life. The shōtengai has long effectively fulfilled this niche market within Japanese communities. However, with increasing automobile ownership nationwide and growing rates of women in the workforce, large supermarkets selling a wide variety of goods have become more attractive to busy families. Many new shopping venues also entice customers by providing entertainment services such as movie theaters and arcades in addition to retail, which associates shopping with leisure. New shopping malls, supermarkets, and department stores have provided one-stop shopping services at discount prices to the busy Japanese family, resulting in the stagnation of business in the traditional shōtengai (Balsas, 2016).

##### 2.2.4.2 Shōtengai, Communities, and Culture

Although new supermarkets and large shopping centers are attractive and convenient to the modern family, shōtengai still offer significant cultural value to their neighborhoods and to Japanese society as a whole. Shōtengai are important for the social stability of Japanese neighborhoods, and are still seen as a unique, valuable aspect of Japanese culture (Balsas, 2016). In larger urban areas, shōtengai serve not only as retail locations, but also as centers of community and culture. Shōtengai retailers often function as leaders in community activities such as seasonal festivals (Hani, 2005). They also provide valuable services that a supermarket or larger shopping centers cannot such as small local restaurants that provide a friendly neighborhood atmosphere. Daily patronage of shōtengai can also create a sense of community among consumers and shop owners, creating a sense of place and belonging in a neighborhood that is absent in larger centralized shopping centers (Balsas, 2016). 

Placelessness, or the standardization of landscapes, is an epidemic that has been plaguing urban areas in recent years. Commercialization and industrialization has led to the rise of urban features that are inauthentic and nonrepresentative of the local culture. For example, most large shopping malls contain a configuration of the same set of international chain stores. Any one of these malls could simply be cut and pasted into another landscape, as there is no definitive sense of place associated with the standardized mall (Shim & Santos, 2014). The shotengai, unlike a shopping mall or department store, consists mainly of sole proprieterships. Many shotengais have developed and changed gradually and organically over a number years in contrast to a shopping mall, giving shotengai a sense of place that is uniquely Japanese. Shotengai offer a sense of place to urban areas that can become lost with the rise of placeless, urban landscapes.

In addition to adding color to the local neighborhood, shotengai often have particular significance to the elderly population. The small businesses within shōtengai cater to the needs of local citizens, including elderly citizens that may not have the means to travel to department stores or supermarkets father away. Studies have shown that the elderly in Japan may find shopping, and in particular food shopping, to be difficult. A main cause of this hardship is food deserts, residential areas that are devoid of local food markets (Ishakawa et al., 2016; Iwama et al., 2017, Choi & Suzuki, 2013). When elderly residents have difficulty shopping, they are vunerable to suffer from poor nutrition that negatively affects their health (Choi & Suzuki, 2013, Iwama et al., 2017). A local shotengai within easy walking distance provides elderly residents with access to general and specialty food markets, whereas larger supermarkets in centralized locations may be difficult for older citizens to travel to.

#### 2.2.5 Japanese Case Studies

The following section examines a case study of shotengai decay and revitalization in Osaka prefecture and introduces the Taishogun shotengai that will be surveyed.

##### 2.2.5.1 Shopping Districts in Izumisano, Osaka Prefecture

Mitsuko Nashima (1997) describes the fates of two shopping streets in the Izumisano shopping district in Osaka Prefecture that perfectly illustrate the plight that has befallen many shōtengai in the modern world. 

In 1997, Izumisano was home to 7 shopping cooperatives, which grew alongside the economic expansion of the local textile industry following World War II. However, three successive events instigated the region's decline; a supermarket was built near the local train station in the late 1960's, followed by the oil crisis of 1973 which left the local textile industry reeling. Then the land market inflation of the 1980s didn't help matters when high real estate prices forced many local residents to move to the suburbs, shrinking the customer base of local shops. (Nashima 1997)

In light of these events and the general trends among shōtengai nationwide, a shuttered shopping street adjacent to the Izumisano train station exemplifies the possible fate awaiting shōtengai that fail to adapt to recent trends and maintain a customer base. Shuzu Kurumano, a director of one of the 7 shopping cooperatives, recalled the street was once "...so crowded you had to elbow your way through." (Nashima, 1997, paragraph 3). Now all that remains of this once vibrant street are empty, shuttered storefronts. (Nashima, 1997)

In contrast, another shopping street in the area, Tsubasa Dori, launched a revitalization project to avoid the fate of their unfortunate neighbor. The project, costing $2.6 million, reconstructed an 180-yard long arcade, hoping to attract shoppers traveling through Kansai International Airport which is a 10-minute train ride away. While the project has been a success, project directors remain skeptical about the long-term benefits of the revitalization, citing a lack of successors as a possible reason for future shop closures within the next 15 years. (Nashima, 1997)

Shopping districts nationwide are hoping to avoid the same fate as the shuttered Izumisano shōtengai, however changing demographics and consumer trends are making it increasingly difficult to carry out successful revitalization campaigns. Many shop owners are becoming older and find it difficult to implement change, and Japan's growing suburbanization and aging population have left many downtown areas mostly devoid of young people (Nashima 1997). Nashima reports that enthusiasm and cooperation are necessary for the change and renewal of these shopping districts. A Ministry of International Trade and Industry official interviewed by Nashima sums it up best: "There are a total of 3,300 cities, towns and villages in Japan, and we cannot save them all. What we can do is to give them a dream by making a few truly successful examples." (1997, paragraph 38).

##### 2.2.5.2 The Taishogun Shōtengai and Yōkai Street

The shōtengai that will be surveyed, Taishogun shopping street (also known as “Yōkai street”) is an example of a shōtengai that has suffered business loss due to changes in consumer trends. The Taishogun shōtengai spans 400m along Taishogun in northwestern Kyoto, and is currently home to approximately 30 stores. A number of the shops cater to a niche market, including a cycling shop, a tropical fish supply store, and a clothing store for the elderly, but the street is also home to general goods stores, restaurants, bakeries, and specialty food markets, among others (About Taishogun Shopping Street, 2018). 

Like many shōtengai in modern Japan, Taishogun shōtengai has suffered its share of business stagnation and store closure. One of the shops that was located on the street, Fujiwara kimono store, has recently closed and the shop has been torn down. The empty lot has not been rebuilt as of March 2018 (Google maps, 2018). In response to stagnation of business, the shop owners association has attempted to take action to prevent the deterioration of the shōtengai through several regeneration methods that have yielded limited success.

In 2005, the shop owners association of Taishogun shōtengai launched a campaign to rebrand Taishogun shopping street as “Yōkai Street” in an attempt to revitalize business in the area. Yōkai are monsters or demons told of in Japanese folklore dating from the Heian period of Japanese history. The shop owners tapped into these legends to decorate their storefronts, each constructing a yōkai that sits outside their shop to greet customers, an example of which is seen in figure 6. Some shops even sell yōkai-themed products, such as yōkai ramen - purple ramen noodles in black broth - and yōkai korokke, a monster-themed version of a popular Japanese street food (Murakami & Yoshikura, 2015).

![Figure 6: Two yōkai greet customers outside Nishiki-kagetudo Tea shop on Yōkai Street. (Arnold, n.d.)](assets/arnold-nd.jpg)

In tandem with the Yōkai decorations, the shōtengai began hosting an annual community event, the Yōkai Parade. This parade occurs the 3rd Saturday of October each year and reenacts the legend of Hyakki Yagyō, or “March of one thousand demons”. On this night, over one hundred participants dress up as yōkai and march down the street, as seen in figure 7. The event is the largest annual event of the area and draws over one thousand visitors each year who come to see the yōkai and patronize pop-up shops that sell food and drinks during the event. The shōtengai also hosts a popular yōkai-themed flea market several times a year that sells unique monster-themed goods (Hajiri, 2018). 

![Figure 7: Parade participants dressed up as yōkai outside one of the shops on the street. (Inoue, n.d.)](assets/inoue-nd.jpg)

The launch of “Yōkai Street” has brought more visibility to the area in terms of tourism. An internet search for “Yōkai Street” yields articles advertising the shōtengai to tourists who want to experience destinations off the beaten tourist path. The store owners association also created a website to promote the shōtengai, of which an english version was launched in 2015, and have created a small social media presence through a Facebook, Twitter, and Youtube page. 

Despite efforts to revitalize commerce within the shōtengai, business is still struggling, and further regeneration efforts may be necessary to prevent more shops from closing their doors. 

#### 2.2.6 Pop Culture in Japan

In order to understand how to boost traffic in the Taishogun shotengai shopping area, it is necessary to understand Japanese youth culture and pop culture. The following section explores how an explosion of pop culture and "Cool Japan" has influenced Japanese culture.

From Hello Kitty to Pokemon, Power Rangers, and Nintendo- nearly every American child in the past 30 years has been influenced by Japanese pop culture exports (Hoskin, 2015). Japanese pop culture influence is so ubiquitous in the United States that many may not even realize its presence. Since the collapse of Japan's economic superpower in 1991, Japan has slowly, quietly, been rising to prominence in through a new 'national cool' argues Douglas McGray (2002). National cool refers to the idea that a country's trends and commercial products can exert influence in political and economic spheres, much like the influence of Hollywood on international audiences (McGray, 2002). 

##### 2.2.6.1 Cool Japan

In 2013, the "Cool Japan Fund" was launched, a project intended to assist worldwide promotion of Japanese culture. The fund intends to help companies with potential to launch into global markets to promote Japanese culture worldwide. The project was inspired by South Korea, whose recent K-pop globalization has overshadowed Japanese culture, and hope to spotlight Japanese culture ahead of the 2020 Tokyo Olympic Games (Fukase, 2013).

#### 2.2.7 Tourism and Kyoto

Tourism is one of the biggest industries in Kyoto. As such, it is necessary to understand Kyoto's tourism industry in order to promote the Taishogun shopping area to locals, as well as tourists. The following section explores the effect of Japan's tourism boom on Kyoto. 

Foreign tourists visiting Japan have increased dramatically in the past decade. In 2003, approximately 5 million tourists visited Japan; to boost the tourism industry, the government launched a widely successful campiagn called "Visit Japan" that same year. As of 2016, the number of foreign tourists visiting Japan had exceeded 20 million, a number that is projected to reach 40 million by the Tokyo 2020 Summer Olympics (Ryall, 2017). The influx of foreign tourism has provided a much-needed boost to the economy, however, many cities such as Kyoto have found themselves unprepared to support the tourism boom, resulting in a phenomenon the media has coined as "tourism pollution" (Brasor, 2018, Ryall, 2017, McCurry, 2018). 

##### 2.2.7.1 Kyoto and the Tourist Boom

Residents in Kyoto have become increasingly frustrated with the changes in quality of life that has accompanied the tourism boom. Buses have become overcrowded, restaurants and hotels are often booked full, and many tourists are inconsiderate, rude, and uninformed of the local customs. Julian Ryall points out that Japan is a deeply conservative nation. Japanese people are very much set in their traditions and social norms, and changes to norms are very slow (2017). Many Kyoto residents have become understandably frustrated with the invasion of too many foreign tourists that do not understand Japanese customs. 

In response to the overflow of tourists, the city of Kyoto and the Japanese government has implemented new policies to ease the pressure tourist crowds have been placing on infrastructure. Starting in January of 2019, the Japanese government is planning to implement a ¥1,000 exit tax on those leaving the country. The funds are earmarked for supporting tourism infrastructure (Brasor, 2018). In addition, the city of Kyoto has introduced a lodging tax on tourists staying in the city, which is expected to bring in ¥4.56 billion annually and will be used to relieve transportation congestion experienced by crowded city buses (Johnston, 2018). 

New policies regarding private rentals to tourists, specifically Airbnb rentals have also been implemented to reduce tourism pollution in residential neighborhoods that has left residents complaining about late-night noise and improper garbage disposal (Brasor, 2018, McCurry, 2018). Property owners are now required to register their property with the Japanese government to legally rent through services such as Airbnb. Under the law, short term rental properties can only be rented out a maximum of 180 days per year, and local governments have been given the authority to impose additional restrictions as seen fit (McCurry, 2018). 

Additional efforts by the Kyoto Tourism Bureau hope to to reduce friction between the local culture and foreign tourists. A partnership with the site TripAdvisor has produced a pamphlet to educate visitors on social taboos, and the bureau is encouraging events to be scheduled in the morning or evening to reduce congestion. In addition, the bureau also hopes to reduce congestion through promoting travel to Kyoto during non-peak months. Through these initiatives, officials from the tourism bureau hope to achieve harmony between tourists and local residents, in order to continue promoting Kyoto as a tourism destination without ignoring the well-being of residents (Ryall, 2017).

#### 2.2.8 The Experience Economy

Traditionally, businesses in commercial districts offer commodities, goods, or services. In the traditional shotengai, produce shops sell produce; clothing shops sell clothes; restaurants serve food, and so on. Recently however, more and more businesses have been marketing something different: the experience.

An experience is something distinct from products and services- it offers a memorable experience for the consumer (Pine & Gilmore, 1998). The production of an experience for the consumer drives consumers to pay more for a service that could be obtained elsewhere—it’s the reason customers are willing to pay exponentially higher prices at Tokyo’s Aragawa Steakhouse for the same service they could receive at an Outback Steakhouse (Pizam, 2010). 

The concept of selling an experience to the consumer is not a new concept, but is a type of commerce that has traditionally been relegated to the entertainment industry. Theme parks such as DisneyLand have been selling the Disney experience to visitors for years, and children’s entertainment centers such as Chuck-E-Cheese offer birthday packages complete with a cake and singing characters. However, recently the experience economy has been cropping up in other industries—restaurants, retailers, and even corporations have started marketing the experience over the goods and services to attract business. Themed restaurants such as the Rainforest Cafe and stores such as Nike’s Niketown have popped up, offering a multisensory dining or shopping experience to set them apart from their traditional counterparts (Pine & Gilmore, 1998). 

Japan is no stranger to the experience economy. Themed cafes have popped up all over Tokyo and other metropolitan areas to serve customers looking for a memorable encounter, and they deliver. Appointments are made months ahead of time to visit the Ikefukurou Cafe in Tokyo where guests can enjoy their coffee while petting the cafe’s collection of 37 owls (Lewis & Jacobs, 2018). The driving force behind this phenomenon, many believe, is millennials (Lewis & Jacobs, 2018, Cataldo, 2018, Kohler, 2017). Research has shown that the majority of the millennial generation prefer spending money on experiences than desirable objects (Lewis & Jacobs, 2018, Kohler, 2017). 

The key to attracting young people to an area is likely to relate to the experiences an area offers. If an area does not offer unique experiences, it logically follows that millennials will be less likely to visit these areas, and in a country with a rapidly-aging population like Japan, attracting young consumers to an area is more important than ever (Sorensen, 2006). 

### 2.3 Mapping for Analytics

>  "As in photographing a crowd, the details of the picture change continually, but the general effect is much the same, whatever moment is chosen. I have attempted to produce an instantaneous picture, fixing the facts on my negative as they appear at a given moment, and the imagination of my readers must add the movement, the constant changes, the whirl and turmoil of life." (Booth, as cited by Kimball, 2006)

#### 2.3.1 Example Applications of Mapping for Analytics

The applications of maps extend beyond their ability to represent the criss-crossing roads of cities: Maps can be used for displaying points of interest, for conveying geospatial statistics, and even for depicting the relationships between objects. This diverse utility makes maps exceptional tools for exploring problems that rest at the intersection of space, time, assets, and people.

In this section, we review a selection of maps that have done more than just provide directions.

##### 2.3.1.1 Charles Booth’s Poverty Maps

![Figure 8: Charles Booth’s map of London depicting lower-income (black) and higher-income (red) areas within the city. (London School of Economics and Political Science, 2018)](assets/lseps-2018.jpg)

Between 1889 and 1902, a shipping magnate named Charles Booth released a series of maps that visualized poverty within the city of London (Fig. 8). This seemingly objective “bird’s eye” view of poverty was disruptive to the contemporary visual rhetoric of the time; a rhetoric which emphasized grotesque images of the “monsters” and horrors that plagued the lower class of the time. While this map was not without faults in its clarity - particularly in regards to the consistency and ambiguity of the color scales used to differentiate middle-class from lower and upper-class income - it is emblematic of how maps can be used to disrupt cultural visual rhetoric and visualize complex problems. (Kimball, 2006)

##### 2.3.1.2 An Abstract Map

![Figure 9: A map of the London Underground and Overground depicting the relationships between different stations. (Wikimedia Commons, 2014)](assets/wikimedia-commons-2014.png)

One of the most famous non-geographic maps is the map of the London Underground (Fig. 9). This map traces its roots to 1926, when map-maker Fred Stingemore created the first map of the London Underground that didn’t conform to London’s true shape (Graham-Smith, 2016). By ignoring geographic accuracy, this map was able to accurately depict the relationships between stations in a condensed and easily distributable format. This map is another perfect example of how maps can be used to transform complex problems into digestible images.

##### 2.3.1.3 An Interactive Map

![Figure 10: An interactive map displaying information about points of interest within the fictional world of “Game of Thrones.” (Treist, 2017)](assets/treist-2017.jpg)

With the widespread adoption of the internet and a myriad of web-based technologies, interactive maps that contain clickable elements and rich content have become more common. One such example of these interactive maps is a map of the fictional world of “Game of Thrones” (Fig. 10) which allows users to click on various elements of the map to learn more about their history and significance.

##### 2.3.1.4 A Mixed-Data Map

![Figure 11: A mixed-data map that displays data about restaurants (blue circles), liquor establishments (red circles), and median age (heatmap). (Bergmann, 2016)](assets/bergmann-2016.png)

In addition to making maps interactive, web-based technologies have made it incredibly easy to combine statistical data from multiple sources to create mixed-data maps. For example, this mixed-data map (Fig. 11) combines information about restaurants, liquor-serving establishments and age demographics to help statisticians perform location analysis based on a combination of asset and census data. 

#### 2.3.2 Urban Asset Mapping

While there are many kinds of maps (and techniques for creating those maps), as seen in the prior section, there is a form of mapping that is of particular interest when trying to understand urban decay and regeneration: Urban Asset Mapping. As its name implies, urban asset mapping visualizes the different assets within an urban context by using a combination of interactive and mixed-data mapping concepts. Surprisingly, there is no *rigid* definition of what constitutes an "urban asset." Based on information compiled from various sources (Social Value Lab, 2012, OPEN Glasgow, 2014, and South Seeds, 2017), we are able to glean a partial list of typical assets:

- Transportation (including parking, bus stops, train stations, bikeshares, etc.)
- Clinics
- Schools and libraries
- Recreational centers (e.g., parks, gardens, gymnasiums, playgrounds)
- Restaurants
- Churches
- Post offices
- Specialty stores and services
- Houses and property

In addition to the assets above - which are common in European countries - common assets in Japan could include:

- Shrines
- Temples
- Public bathouses
- Network cafes

![Figure 12: One of the composite assets maps produced as a result of the Social Value Lab’s survey of the Scottish town of Maybole. Blue dots indicate libraries, orange dots indicate educational assets, light green dots indicate leisure assets, and dark green dots indicate transportation assets. (Social Value Lab, 2012)](assets/social-value-lab-2012.jpg)

One prominent example of urban asset mapping can be seen in a 2012 report on the Scottish town of Maybole published by the Social Value Lab. We find it important to note that some of the maps in the Social Value Lab's full report used a combination of asset mapping and on-site surveys to describe issues with accessing or utilizing assets; an asset-map is not doomed, therefore, to be *just* a map of physical assets. The Social Value Lab was able to make extensive use of these composite asset maps (Fig. 12) - generated in part from community surveys - to create a detailed list of recommendations to improve the community’s health and safety. (Social Value Lab, 2012)

![Figure 13: An interactive map of prominent bell towers in Venice. Some of bell towers are clickable, allowing users to hear the sound of the bell and see a picture of the tower. The colors of the towers indicate how much data was collected for each tower. (Heinricher, Kahn, Maitland, and Manor, 2013)](assets/heinricher-kahn-maitland-manor-2013.png)

A slightly more recent example of urban asset mapping can be seen in a 2013 Interactive Qualifying Project (IQP) which mapped bell towers across Venice (Heinricher, Kahn, Maitland, and Manor, 2013). Their team was able to leverage open-source mapping technology (and a fair amount of legwork) to produce an interactive map of Venice's bell towers (Fig. 13) which can be used by members of the public to learn more about local history.

#### 2.3.3 Creating Interactive and Mixed-Data Maps

In the previous sections, we have seen a wide variety of maps and applications for those maps. It used to be that creating maps like these - which combine a myriad of software technologies, analytics, and a good deal of legwork - would require significant funding and technical expertise. 

Fortunately, creating interactive mixed-data maps is no longer a job that only expert cartographers can perform. While both the map of poverty by Charles Booth and the map of the London Underground undoubtedly took years of work to reach completion, the other maps are easier to execute due to their reliance on modern free and open-source technologies:

- The interactive “Game of Thrones” map can be built in an afternoon by following along with a tutorial (Treist, 2017) created by its author.

- The mixed-data map of restaurants, liquor establishments, and ages (Bergmann, 2016), the mixed-data map of properties of interest in Glasgow’s southside (South Seeds, 2017), and the interactive map of Venice's bell towers (Heinricher, Kahn, Maitland, and Manor, 2013) were created using a free and open source technology Leaflet.

- The composite asset maps of the town of Maybole (Social Value Lab, 2012) were created by annotating publicly available map images using vector graphics software. While this method is cumbersome - since each map is generated by hand without any machine automation - it is effective and inexpensive to execute.