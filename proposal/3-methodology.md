## 3. Proposed Methodology

![Figure 14: Overview of our proposed methodology.](assets/the-plan.png)

The goal of our project is to answer a single research question: *What actions can Taishogun take to reverse or prevent its decay, while still providing value to local residents?* 

Given the broad scope of this question, we divided it into three sub-questions:

1. What *assets* (e.g., services, utilities, buildings, and public spaces), *demographics*, and *events* exist within the greater Kamigyo ward area (the area surrounding Taishogun)?

2. What factors contribute to a shotengai's success or failure?

3. Within individual shotengai---and Taishougn in particular---are there physical signs of imminent or ongoing damage, decay, or depopulation?

Because our project sponsor ultimately wants our findings delivered as a pair of maps - one meant to draw tourists to Taishogun, and the other meant to help identify possible markers of urban decay in Taishogun - we intend to build our methodology around a three sequential mapping activities, beginning with a geographic survey of the greater Kamigyo ward area, and ending with on-the-ground surveys of individual shōtengai:

1. **Regional Survey** To create a wealth of "thick" data about the entire Kamigyo ward (the area surrounding Taishogun), we will leverage open data from geographic information systems (**GIS**), censuses, cultural centers, travel agencies, and tourism areas.

2. **Categorizing Shōtengai**: To bridge the gap between our regional surveys and local surveys of specific shōtengai, we intend to employ a novel method that looks for shotengai in areas of potentially high and low traffic (urban ley-lines).

3. **Local Survey**: Once we identify, categorize, and create a list of shōtengai, we will visit each shōtengai and perform geographic surveys of the condition and activity within these shōtengai. This will allow us to cross-reference the data we receive from GIS, curate up-to-date information about the shōtengai, and identify common markers of decay within the shōtengai.

When this process was completed, we summarized our findings with a pair of maps:

1. A **tourism** map designed to provide visibility for activities in and around Taishogun.
2. A **decay** map designed to aid in the identification of urban decay within Taishogun.

In this section, we review the methods used throughout each of these processes, as well as the rationale for each of them.

### 3.1 The Electronic Field Form for Urban RegeneraTion

![Figure 15: ***Mockup*** of an interactive urban asset and anchor map of the Taishogun Shotengai. In this mockup, a user has just finished editing an entry for Yokai Soho (the marker highlighted in red, center) into EFFURT.](assets/urban-map.png)

To ensure the data gathered during each phase of our methodology could be normalized and stored in one location, we intend to create a novel mapping platform called the Electronic Field Form for Urban RegeneraTion (**EFFURT**).

We elected to create a custom application—EFFURT—because there are few (if any) existing tools that would allow our team to quickly collect, store, and visualize data for urban assets and decay without paying exorbitant sums for a professional GIS tool suite. The EFFURT application, its source code, and its documentation, will be made available on a publicly-hosted source-code repository (GitHub or GitLab) for peer-review and reuse by future research teams.

#### 3.1.1 Proposed Features

EFFURT will allow our team to quickly collect, aggregate, and visualize a wide range of data, including:

- **Urban Assets and Events:** EFFURT will be able to import data from GIS systems to plot the locations of urban assets (e.g., buildings, services, and utilities within an urban region). By combining data storage and automated data analytics, EFFURT will allow users to quickly build physical representations of regional and local survey findings, and rapidly apply those findings to understanding possibly reasons for decay within an area.

- **Urban Anchors:** By using data from GIS systems, EFFURT will be able to automatically calculate the locations of potential **urban anchors**, which are urban assets or clusters of urban assets that are of significant value to communities (e.g., shopping districts, schools, hospitals, etc.).

- **Census Data**: EFFURT will be able to parse column-separated values (CSV) census data to generate cartograms (see section 3.2.2) of a region, allowing users to visualize census data for a region of interest.

- **Shōtengai Information**: EFFURT will have a mark-and-annotate feature inspired by the Social Value Lab's asset mapping survey of Maybole (Social Value Lab, 2012). This feature will allow us to place a marker anywhere on the map, define a bounding box for the marker (the approximate area on the map that the marker represents), and then fill in information about the marker (fig. 15). This allows us to quickly collect and visualize information about the individual shops and assets within a shōtengai.

#### 3.1.2 Proposed Method

To create EFFURT, we plan to use the following "stack" of technologies:

1. **JavaScript**: EFFURT will be written using the JavaScript programming language, allowing users to run EFFURT directly from web browsers on computers and smart phones. For local surveying activities that are conducted in areas with no internet connectivity, we will include a feature to download EFFURT to a user's smartphone and then unpload collected data when they can connect to the internet again.

2. **PouchDB** (https://pouchdb.com/): EFFURT will store collected data inside a database called PouchDB. There are a myriad of options to choose from, but we chose PouchDB because:

  - It is lightweight and compatible with JavaScript.
  - It has a built-in feature to store data locally and then synchronize it later, which is essential for offline functionality of EFFURT.

3. **Leaflet** (https://leafletjs.com/): To render and annotate maps within the browser, EFFURT will use Leaflet, an open-source web-based mapping platform that has been proven by multiple different research groups (Bergmann, 2016, South Seeds, 2017, Heinricher, Kahn, Maitland, and Manor, 2013). Leaflet allows us to display maps as *layers* on top of one another; for example, we can have a layer for a map of Kyoto, a layer for urban assets in Kyoto, and then a top layer showing urban anchors in Kyoto. This makes it very easy to create customized maps and reports depending on our project's needs.

4. **OpenStreetMap** (https://www.openstreetmap.org/): By itself, Leaflet does not provide any **map tiles** (images of an actual two-dimensional map). As a result, we intend to use OpenStreetMap, a freely available repository of maps that are constantly updated by communities worldwide, to display map tiles within EFFURT.

5. **Overpass** (http://overpass-api.de/): OpenStreetMap provides a large amount of data beyond just map tiles—data like urban asset locations. To filter through all of this data and help our team quickly visualize urban assets within a region, EFFURT will use Overpass—a search tool for OpenStreetMap—to rapidly query and display urban assets that we are interested in on maps.

6. **Overpass Turbo** (http://overpass-turbo.eu/): Overpass is a powerful tool, but it is incredibly difficult for non-programmers to use. Therefore, we intend to leverage Overpass Turbo, a graphical user interface for Overpass built with Leaflet, to allow our team to manually perform Overpass queries without using the EFFURT application.

7. **Protoviz** (http://mbostock.github.io/protovis/ex/cartogram.html): Protoviz is a data visualization toolkit for JavaScript that will allow us to easily generate cartograms (see section 3..2.2) of a region within EFFURT using census data.

### 3.2 Regional Survey using Open data

The purpose of the regional survey is to identify the urban assets, demographics, activities, and events within an area that may contain multiple different shōtengai. To do this, we leveraged publicly available ("open") data from:

- Geographic information systems (**GIS**)

- Census data provided by the Japanese government

- Cultural centers and tourism agencies

This data allowed us to identify the *assets*, *demographics*, and *events* that exist within the greater Kamigyo ward area, in turn helping us identify bigger-picture signs and symptoms of urban decay:

- Are there small clusters of urban assets that are detached from urban anchors, and if so, are they residential or commercial? 

- Do the services of urban assets within the region align with the perceived needs of the predominant demographics within a region? 
  
- Are there sufficient events within the region to attract people? 

#### 3.2.1 GIS Data

To gain a preliminary understanding of the urban assets in the greater Kamigyo ward area, we used publicly available data from and Overpass Turbo (which provides a database of urban assets and features on top of open street maps). We define an urban asset as any urban feature that serves a purpose to the public, draws foot traffic, and adds value to an area. By this definition, an urban asset can refer to many different urban features such as shops, parks, hospitals, community centers, parking lots, public transportation, and even public toilets. We are interested in mapping urban assets because they can provide information on the vitality of an urban area. 

The Overpass Turbo search engine allows users to search for specific urban assets using keywords. When a search is performed, it overlays circles or rectangles on top of locations of that type of asset within the area searched. For example, a search for “tourism” yields a map highlighting locations a tourist might be interested in sightseeing (fig. 16), and a search for “shop” yields a map highlighting shops in the area (fig. 17). It should be noted that the Overpass search engine may not provide a completely comprehensive map of all urban assets searched for, as it relies on geographic data input by communities, however it will provide us with a valuable overview of the geography of Kamigyo Ward, and any insufficiencies in the data provided will be supplemented by our local survey (section 3.4).

When combined with EFFURT, this data allows for the quick visualization and analysis of the many different assets within a region. Locations on overpass will be cross-referenced with information available on open street maps and google maps, and assets of interest will be overlayed as a map layer on EFFURT to keep track of the information. The following is a list of urban asset search terms we will use to identify locations and clusters of urban assets in the greater Kamigyo ward area. The images of maps generated through searches will be saved for later reference.

- “Tourism”
- “Shop”
- “Bus stop”
- “Train station”
- “Parking”
- “Museum”
- “Park”
- “Place of worship” (for shrines, temples, and churches)
- “Toilets”
- “Community center”
- “Restaurant
- “Hospital”; “Clinic”
- “Bank”
- “Dentist”
- “School”;”University”
- Etc. (fill in later)

![Figure 16: An overview of tourism destinations and areas (denoted by colored circles and rectangles) within Kyoto's Kamigyo ward. Note that the neighborhood of Taishogun, a shōtengai we suspect is at risk of urban decay, is in a "dead area." Data from Overpass via the Overpass Turbo query for "tourism."](assets/overpass-turbo-tourism-2018.PNG)

![Figure 17: An overview of the shops (denoted by colored circles) within Kyoto's Kamigyo ward. Data from Overpass via the Overpass Turbo query for "shop."](assets/overpass-turbo-shop-2018.PNG)

#### 3.2.2 Census Data

The Japanese government conducts routine censuses of their population, publishing this data on the internet via their statistics bureau. We will use data from their most recent census, conducted in 2015, to compile information on the current demographics of the Kyoto region. We will also look at previous census data from 1995-2010 (1995 is the oldest census data available on the Statistic Bureau’s website) to ascertain whether there have been any significant demographic shifts in the area over the past two decades.

Unfortunately - and understandably - most of the published data contains significant Japanese language, which our team does not specialize in. Therefore, we will conservatively employ Google's translation services to gain a rough understanding of the content of the census data before visualizing it. We will also pursue aid from our sponsor and, if possible, a translator, in translating the Japanese language in the census data files if deemed necessary to be able to analyze the data. 

Due to the language barrier, we are not completely certain what types of demographic data is available and the geographic specificity of the data. If possible, we would like to gather demographic data on Kamigyo ward specifically, however data may only be available on Kyoto city and/or prefecture as whole, which may not accurately depict the demographics of the neighborhood around Taishogun shotengai.
The demographic data we are interested in extracting from the census includes:

- Population
- Age distribution
- Occupation demographics
- Household demographics
- Anything else??

To visualize shifts in population and age distribution over time, we will use excel to generate graphs of these demographics from the 1995, 2000, 2005, 2010, and 2015 censuses. 

![Figure 18: Choropleth of the United Kingdom's votes on Brexit (lighter colors indicate higher preference for remaining). (Taylor, 2016)](assets/taylor-2016-brexit-choropleth.png)

There are multiple different methods to choose from when visualizing census - and more specifically, demographics - data. One of the most well-known methods (although possibly not by name) is the **choropleth** map (fig. 18), which shades the jurisdictions or census regions of a map varying shades of a color to indicate the concentration of some quantitative value (e.g., age, occupation, or income) in an area.

![Figure 19: Cartogram of the United Kingdom's votes on Brexit (bluer colors indicate higher preference for remaining, redder colors indicate lower preference). (Li, 2017)](assets/li-2017-brexit-cartogram.png)

However, choropleth maps - particularly of dense urban areas like Kyoto - only visualize relative *quantity*, not *density*: small parcels of land with very high values (and thus density) can disappear amongst many other small parcels of land with moderate values (and thus lower densities). This can make identifying the density of specific populations difficult, as noted by Yano, Nakaya, & Kato in their 2001 paper which aimed to create a new social atlas of Kyoto via a different method of visualizing census data: the **cartogram** (fig. 19).

Unlike choropleth maps, which remain faithful to jurisdictional maps, cartograms distort the jurisdictional demarcations of maps to emphasize the density of a demographic of interest within a region. As a result, for our analysis of demographics, we have included a feature in EFFURT to intake the column-separated value (CSV) data from the Japanese censuses and present them as a cartogram of an area of interest using methods similar to Xinye Li's 2017 cartogram of "Brexit" votes in the United Kingdom.

#### 3.2.3 Events Data

We believe that the typical healthy shōtengai regularly participates in the events that occur in its neighborhood (Hani, 2005, and Balsas, 2016). As a result, we hypothesize that Taishogun's decay may be partially caused by event droughts (periods of time with little or no events). A cursory survey of Taishogun's website during our background research showed that the only events the small shōtengai participates in is a yearly "Yokai Parade" that draws about a thousand people to the area for one night (Hajiri, 2018), and a Yokai themed flea market that is held sporadically, with no other well-advertised events or event participation.

To gain a better understanding of the role events and activities may play in Taishogun's decay and the decay or success of other shōtengai, we conducted an informal online search of cultural centers, events, and activities occurring within the greater Kamigyo Ward area, and the areas of other shōtengai that are similar to Taishogun.

This search leveraged the following data to ultimately generate a listing of cultural centers, events, activities, and their geographic locations:

- GIS survey data for museums, libraries, and other cultural centers.
- Open web searches via Google and DuckDuckGo. We used two different search engines in order to increase the variety of results we gather and stymie the biases within different search algorithms.
- Informal conversations with local stakeholders.

The survey will first involve identifying and gathering a list of cultural centers that may hold seasonal events and activities via data gathered from Overpass during the GIS data phase. We will then use open web searches to search for information about events at each of these cultural centers. Both Google and DuckDuckGo will be utilized as search engines to gain a comprehensive picture of the information available since search engines may bias results. Utilizing multiple search engines will eliminate bias introduced by any individual search engine. The information gathered may come from cultural center official websites, official facebook pages, or travel information sites geared towards tourists. These events will be visualized using both an event calendar and an events map to gain a sense of the frequency of these events as well as their location in relation to shotengai of interest. 

The event calendar will be created using Google calendars. Each event will be entered into a Google calendar to keep track of the temporal frequency of events in a particular area. In addition, the location of each event will be recorded on a map using EFFURT to easily visualize areas that are rich in events and areas that are not. 


### 3.3 Using Urban Ley-lines to Categorize Shōtengai

To bridge the gap between our regional survey which will gather massive amounts of data for a region, and our local survey which assesses the state of specific streets, we require a method for categorizing shōtengai in order to identify which ones we will visit for local surveys.

Creating a system for categorizing and ranking shōtengai is complicated. Dring our initial planning meetings, we considered several methods that emphasized *at-risk* shōtengai; however, these methods proved problematic since we do not have enough information to accurately predict a shōtengai's success.

Therefore, we narrowed our categorization criteria to a single hypothesis which relies on a novel mapping method created by our team: **Urban ley-lines.** We define urban ley-lines as *compulsory paths of traffic* between significant urban assets or clusters of urban assets called **urban anchors**, a term we borrow from the concept of "anchor stores" in malls.

For the purposes of this paper, we consider all of the following assets urban anchors:

- Transportation hubs (train stations, bus stops, bikeshares, etc.)
- Heritage sites (shrines, temples, cultural centers, seasonal events, etc.)
- Religious centers (shrines, temples, churches, etc.)
- Healthcare facilities (hospitals and specialist services; clinics are too small unless there are no hospitals  nearby.)
- Educational complexes (libraries, schools, universities, etc.)
- Large clusters of assets (e.g., shopping malls, large shōtengai)

We hypothesize **Shōtengai that lie on urban ley-lines are more likely to succeed, while shōtengai that lie outside of urban ley-lines are more likely to fail.** By visualizing urban-ley lines on top of our regional data, we can apply this hypothesis to create two lists of "interesting" shōtengai for our team to visit:

1. A list of *potentially* successful shōtengai (those shōtengai that lie on ley-lines or in anchors).

2. A list of *potentially* failling shōtengai (those shōtengai that lie off of ley-lines and outside of anchors).

#### 3.3.1 Proposed Method

![Figure 20: Urban ley-line map of Kyoto's Kamigyo ward based on an Overpass Turbo map of `tourism` assets, manually created using Inkscape. The green circles represent clusters of urban assets (urban anchors). The lines between the anchors indicate the shortest path (not accounting for street layout) between anchors. The Taishogun shōtengai is marked by a magenta star.](assets/ley-lines.png)

A map of urban ley-lines (fig. 20) will show urban anchors (using methods similar to those used by South Seeds in their 2012 survey of Maybole), along with visible connections between those anchors (ley-lines) that are within a reasonable walking distance of 0.8km (based on Yang's findings in 2013, which indicated a walking distance of 0.5 miles was reasonable for day-to-day activities). 

This map can be created manually or automatically:

1. **Manually**: Using screenshots of maps from Overpass Turbo, we can manually annotate the maps using vector graphics software (e.g., Inkscape or Adobe Illustrator). We would place semi-transparent circles around the bounds of large clusters of urban assets to depict anchors, and draw semi-transparent lines between all of the placed circles to depict ley-lines.

2. **Automatically**: We can modify EFFURT to use its existing map layer data from the regional survey to generate a new map layer of urban ley-lines. This map layer will plot vector lines between any urban anchors whose centers are within 0.8km of one another, resulting in a complete map of urban ley-lines for a region.

The manual method requires less technical work to execute, but the automatic effort is faster and significantly more consistent, as it will rely on EFFURT's built-in generation of urban anchor data and precise distance measurements between urban anchors.

### 3.4 Local Survey using EFFURT

The objective of our land survey is to crosscheck online information, enter buildings around Taishogun street into EFFURT, and to determine if Taishogun Street is in decay or may imminently begin decaying. By recording this information our group will be able to analyze Taishogun street’s present situation more accurately.

In addition to analyzing Taishogun shotengai, we will be analyzing surrounding shotengai and neighborhoods so our group can compare Taishogun shotengai with other shotengai and other commercial districts. After collecting more data on the other streets and Shotengai, we will identify factors that contribute to a shotengai's success based upon the data collected. After defining contributing factors, we will create an appropriate scale to quantify a rating to predict a shotengai's potential for success. Shotengai that have more factors that may contribute to success will be given a higher rating than shotengai with less factors. This will allow us to group shotengai into categories based upon similar potentials for success to determine if Taishogun shotengai is in a state of decay or approching a state of decay, and identify sustainable improvements that could be implemented to reverse Taishogun shotengai's imminent or present decay.

#### 3.4.1 Proposed Method

To complete the land survey, group members in pairs will head out through the different areas that we have marked as points of interest. They include but are not limited to:

- Taishogun Shotengai
- Nishioji-dori
- Senbon Dori Street
- Demachimasugata Shopping street
- Sanjo-kai Shotengai

While going through the different areas, the groups will use mobile devices to fill out EFFURT and record data about the state of buildings using a 5-point Likert scale:

- **5:** Building has no visible damage
- **4:** Building only has nonstructural damage 
- **3:** Plot of land is empty or is functioning, but has some structural damage
- **2:** Building has considerable structural damage
- **1:** Building is closed and/or dilapidated

For the purposes of the survey, **nonstructural damage** will be considered any damage that does not affect the functionality of a building, while **structural damage** will be considered any damage that *does* affect the functionaltiy or safety of a building. 

In addition to observice features about the buildings, the groups will record the time of their visits to different buildings and the average amount of traffic that appears to be present in order to measure overall foot traffic.

##### 3.4.1.1 Decay Factors
Throughout our land survey, we will be considering a variety of factors that could influence a shotengai or individual shop's success:

- Are the shops franchised?
- Does shops with furniture or other large products offer delivery?
- What are the hours of operation?
- Are there any public spaces within 2 blocks of the point of interest and what is their condition?
- Is there a sense of cohesive place? 
- Is the purpose of the buildings obvious to tourists?
- Are specialty shops close to one another

In order to answer the previously stated questions, the groups will observe buildings deemed as public areas, such as shops and restaurants, and write down any relevant observations. The group will also record the building's condition according to the scale above, what is the building's purpose, and take a picture of the shop from the street. This picture is to have an outer view of the shop as reference for later times if needed. 

### 3.5 Tourism Map

![Figure 20: ***Mockup artist's impression*** of a simplified map of Taishogun that displays high-level information about shops and services.](assets/tourist-map-mock.png)

For the tourism map, our goal is to entice tourists and members of the region to go to Yokai Street (fig. 20) by leveraging a variety of visual-rhetorical methods:

1. Warped proportions. By distorting the depicted physical relationships between different landmarks to make them appear closer, causing Yokai Street to appear less "out of the way" for people traveling along Nishioji Dori to the west of Yokai Street and through Kitano Tenmangu to the North.

2. Simplified visualization of Yokai Street and its shops using inviting colors and simple iconography. By reducing the visual clutter found on the current map of Yokai Street (which attempts to remain visually accurate), we can make shops stand out and appear more inviting to visitors.

3. A legend depicting "metadata" about the street (background, history, shops, and big seasonal events).

To create more visibility for Taishogun, we will create a map that contains different activities and events throughout our street and around it to promote the activities and travel to our area. The tourism map will be created using mapping softwares such as ArcGIS, MapTive, and SuperGIS etc. For our street, we will be filtering out the activities, shops, and events that are not particularly relevant to tourists, for example furniture shops or school uniform shops, whereas shops such as restaurants, bakeries, kimono stores etc. will be put on the tourism map. 

On the map, we will also be informing tourists of seasonal events that take place on the street, such as the annual Yokai Parade, and when it will be occurring. In doing so, tourists can easily view all of the information about the street in one location. 

In a similar manner, we will highlight connecting paths and streets from our shotengai to tourist attractions in the surrounding neighborhood (within 0.8 km) such as shrines, temples, eateries, supermarkets, malls, transportation, and lodging to make these locations appear closer and connected to Taishogun shotengai.

To reduce visual clutter, we will be using visual aesthetics such as icons and colors to represent each shop/attraction/event on the map, accompanied by a legend to provide further details about individual shops and events.

### 3.6 Decay Map

By leveraging all of the data stored in EFFURT, we produced a composite digital map to aid in the analysis of urban decay along the Taishogun shōtengai. This map visualizes the following data collected throughout our regional (section 3.2), ley-line (section 3.3), and local (section 3.4) surveys:

1. Estimated demographic data for the census region(s) that Taishogun falls within. Because census data is only precise to 500m grids (Yano, Nakaya, and Kato, 2001), we do not plan to use a choropleth of the individual homes within the neighborhood. Instead, we will list the *mean and median* age of the area as a note.
2. Daily pedestrian traffic compared to similar shōtengai in the greater Kamigyo ward area. Traffic will be presented as a weighted gradient color along the street from clear to solid, with more traffic depicted as a darker (solid) color, and less traffic depicted as a brighter (clear) color.
3. Locations of seasonal events in and around Taishogun.
4. Geographic survey data. We will add functionality to the digital map which will allow us to directly show data from our Urban Ley Line and EFFURT surveys on the map.

The decay map will be implemented to gain a more in-depth understanding of the urban decay in Taishogun shotengai. The decay map will be added to EFFURT as another layer. This map will be an overlay map of all of the different data that we gathered in our methodology so our findings can be visually presented in a single concise map. 

For comparison, we will be using data gathered from visiting similar nearby shotengai (greater Kamigyo Ward area) and in doing so, we will be looking at several aspects:

- If a shotengai is close to major attractions and landmarks
- If a shotengai is easily accessible (i.e. near transportation)
- If a shotengai participates in cultural and seasonal events
- If a shotengai has shops that are significant to tourists
- If a shotengai is well-maintained (no buildings falling apart)
- If a shotengai has lots of visitors

For evaluation, with the help of our sponsor, we will be filtering out the relevant data from different surveys to put on the decay map to help identify and reverse the decay of Taishogun Shotengai.

In all cases, all of this data can be shown together or separately as individual "overlays" on the same digital map, allowing us to quickly customize the map to look at how two or more factors are related in guaging urban decay across the shōtengai. 