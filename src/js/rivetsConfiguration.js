// This code will not execute within the global scope.
(function() {

    // Formatter to emulate boolean NOT.
    rivets.formatters['not'] = val => !val;

    // Formatter to check if a string is empty.
    rivets.formatters['hasText'] = val => !(!val || /^\s*$/.test(val));

    // Formatter for converting milliseconds to dates.
    rivets.formatters['humanizeUnixTime'] = val => (new Date(val)).toLocaleString();

    // Formatter for prettifying an asset's congestion rating.
    rivets.formatters['prettifyAssetCongestionRating'] = val => {

        // The '+' forces 'val' to be interpreted as a number
        // instead of a string or other similar data type.
        switch(+ val) {
            case 1: return "Less than 10% full";
            case 2: return "~25% full";
            case 3: return "~50% full";
            case 4: return "~75% full";
            case 5: return "More than 90% full";
            default: return "Invalid Data";
        }
    };

    // Formatter for prettifying an asset's decay rating.
    rivets.formatters['prettifyAssetDecayRating'] = val => {

        // The '+' forces 'val' to be interpreted as a number
        // instead of a string or other similar data type.
        switch(+ val) {
            case 1: return "Empty Lot";
            case 2: return "Significant Structural Damage";
            case 3: return "Slight Structural Damage or Construction";
            case 4: return "Slight Aesthetic Damage";
            case 5: return "Completely New";
            default: return "Invalid Data";
        }
    };

    // Formatter for marshalling comma-separated lists in and out of arrays.
    rivets.formatters['list'] = {
        read: function(value) {

            // Convert array to comma-separated string for display.
            return value.join(", ");
        },
        publish: function(value) {

            // Convert string to array and trim leading/trailing whitespace.
            return value
                .toLowerCase() // Convert string to lowercase.
                .trim() // Trim trailing whitespace.
                .split(",") // Split on commas into an array.
                .filter(val => !(!val || /^\s*$/.test(val))) // Remove elements that are blank.
                .map(str => str.trim()); // Remove trailing whitespace from elements.
        }
    };

    // Value binder that only executes when an input field changes focus.
    rivets.binders['value-on-blur'] = {
        publishes: true,
        priority: 2000,
        bind: function(el) {
            this.event = 'blur';
            rivets._.Util.bindEvent(el, this.event, this.publish);
        },
        unbind : function() {
            rivets.binders.value.unbind.apply(this, arguments);
        },
        routine : function() {
            rivets.binders.value.routine.apply(this, arguments);
        }
    }
})();