/**
 * Creates and returns a new object denoting an urban asset
 * on a Leaflet map.
 *
 * @param urbanAsset Optional argument. If provided, the returned asset will
 *                   be a copy of the provided asset.
 *
 * @return A new Urban Asset object.
 */
var UrbanAsset = function (urbanAsset) {
    if (urbanAsset) {
        Object.assign(this, urbanAsset);

        // Migrate assets to new format.
        // TODO: Remove after some more testing.
        if (typeof urbanAsset.assets === 'string' || urbanAsset.assets instanceof String) {
            this.assets = urbanAsset.assets.toLowerCase().split(",").map(str => str.trim());
        }

        if (!urbanAsset.opensAt || !urbanAsset.closesAt) {
            this.opensAt = 0.0;
            this.closesAt = 24.0;
        }

    } else {
        this._id = uuidv4();

        this.ll = null;

        this.name = "";
        this.assets = [];
        this.congestionRating = 3;
        this.decayRating = 4;
        this.hasClearPurpose = false;
        this.supportsForeignLanguages = false;
        this.isFranchised = false;
        this.hasPotentialExperience = false;
        this.reviewerNotes = "";
        this.opensAt = 0.0;
        this.closesAt = 24.0;

        this.lastUpdateTimestamp = + new Date();
        this.lastCongestionRatingTimestamp = + new Date();
    }
};

UrbanAsset.prototype = {

    /**
     * Save this Urban Asset to the database.
     */
    save: function () {
        var thisAsset = this;

        // Check if there is a file to save.
        var img = null;
        if ($("#asset-marker-image")[0].files.length > 0) {
            img = $("#asset-marker-image")[0].files[0];
        }

        return urbanAssetDb.get(this._id, {attachments: true})
            .then(function (doc) {
                // If the congestion rating has changed, change the congestion time stamp.
                if (doc.congestionRating != thisAsset.congestionRating) {
                    thisAsset.lastCongestionRatingTimestamp = + new Date();
                }

                // Update the asset timestamp.
                thisAsset.lastUpdateTimestamp = + new Date();

                // Assign basic document data.
                Object.assign(doc, thisAsset);

                // If an image was uploaded, assign that as well.
                if (img) {

                    // Compress the image then insert the document.
                    return compressImage(img, maxImageWidth, maxImageHeight)
                    .then(function(blob) {
                        doc._attachments = doc._attachments || {};
                        doc._attachments.image = {
                            data: blob,
                            content_type: blob.type
                        };

                        return urbanAssetDb.put(doc);
                    });
                } else {
                    return urbanAssetDb.put(doc);
                }
            })
            .catch(function (err) {
                console.log(err);
            });
    },

    /**
     * Remove this Urban Asset from the database.
     */
    remove: function () {
        var thisAsset = this;
        urbanAssetDb.get(this._id)
            .then(function (doc) {
                map.removeLayer(urbanAssetMarkers[thisAsset._id]);
                urbanAssetMarkers[thisAsset._id] = null;
                return urbanAssetDb.remove(doc);
            })
            .catch(function (err) {
                console.log(err);
            });
    }
};