/**
  * Conserve aspect ratio of the original region. Useful when shrinking/enlarging
  * images to fit into a certain area.
  *
  * @param {Number} srcWidth width of source image
  * @param {Number} srcHeight height of source image
  * @param {Number} maxWidth maximum available width
  * @param {Number} maxHeight maximum available height
  * @return {Object} { width, height }
  *
  * https://stackoverflow.com/questions/3971841/how-to-resize-images-proportionally-keeping-the-aspect-ratio
  */
function calculateAspectRatioFit(srcWidth, srcHeight, maxWidth, maxHeight) {

    var ratio = Math.min(maxWidth / srcWidth, maxHeight / srcHeight);

    return { width: srcWidth*ratio, height: srcHeight*ratio };
}

/**
 * Compresses an image blob and trims it to a maximum width and height.
 *
 * @param {Blob} The blob to compress.
 * @param {Number} maxWidth The maximum width of the compressed image (in pixels).
 * @param {Number} maxHeight The maximum height of the compressed image (in pixels).
 *
 * @return {Promise} A promise which will be resolved with the compressed image blob.
 */
function compressImage(blob, maxWidth, maxHeight) {
    return new Promise(function(resolve, reject) {

        // Prepare pica instance.
        var picaResizer = new pica();

        // Convert image blob to Base64.
        var reader = new FileReader();
        reader.onloadend = function() {

            // Create image asset (reader.result contains the base64 img data).
            var img = new Image();
            img.onload = function() {

                // If both image dimensions are within the max dimensions,
                // resolve with the original blob.
                if (img.width <= maxWidth && img.height <= maxHeight) {
                    resolve(blob);
                    return;
                }

                // Prepare canvas for resizing image to.
                var canvas = document.createElement("canvas");
                var newDimensions = calculateAspectRatioFit(img.width, img.height, maxWidth, maxHeight);
                canvas.width = newDimensions.width;
                canvas.height = newDimensions.height;

                // Compress the image asset.
                picaResizer.resize(img, canvas, {
                  unsharpAmount: 80,
                  unsharpRadius: 0.6,
                  unsharpThreshold: 2
                })
                .then(result => {
                    result.toBlob(function(compressedBlob) {
                        resolve(compressedBlob);
                    }, 'image/jpeg');
                });
            };

            img.src = reader.result;
        };

        reader.readAsDataURL(blob);
    });
}

/**
 * @return A string containing a randomly generated version 4 UUID.
 */
function uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0,
            v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

/**
 * Builds a full Overpass API query for a map viewport given a simple query.
 *
 * @param map Map whose viewport the Overpass query will be constrained to.
 * @param overpassQuery Simple query to Overpass to build a full query from. Examples:
 *                      - "shop"
 *                      - "tourism"
 *                      - "leisure"="fitness_centre"
 *
 * @return A complete Overpass API URL that can be invoked.
 */
function buildOverpassApiUrl(map, overpassQuery) {
    var bounds = map.getBounds().getSouth() + ',' + map.getBounds().getWest() + ',' + map.getBounds().getNorth() + ',' + map.getBounds().getEast();
    var nodeQuery = 'node[' + overpassQuery + '](' + bounds + ');';
    var wayQuery = 'way[' + overpassQuery + '](' + bounds + ');';
    var relationQuery = 'relation[' + overpassQuery + '](' + bounds + ');';
    var query = '?data=[out:json][timeout:15];(' + nodeQuery + wayQuery + relationQuery + ');out body geom;';
    var baseUrl = 'https://overpass-api.de/api/interpreter';
    var resultUrl = baseUrl + query;
    return resultUrl;
}

/**
 * Cleans an array of latitude/longitude coordinates from a Leaflet event
 * by extracting only the coordinates from the objects and returning a clean,
 * serializable object that contains no methods and only coordinates.
 *
 * @param coords Array of lat-lng coordinates to clean. This array takes the form:
 *               [
 *               [
 *                 {lat: n, lng: n}, ..., {lat: n, lng: n}
 *               ]
 *               ]
 *
 * @return A cleaned array of lat-lng coordinates.
 */
function cleanLeafletCoordinates(coords) {
    var retval = [[]];

    for (var i = 0; i < coords[0].length; i++) {
        retval[0].push({
            lat: coords[0][i].lat,
            lng: coords[0][i].lng
        });
    }

    return retval;
}