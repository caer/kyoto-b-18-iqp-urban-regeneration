class: animation-fade
layout: true

.bottom-bar[
  Regenerating Yokai Street
]

---
class: impact

# Regenerating Yokai Street
Brandon Sanders, Jonathan Lee, Olivia Hunker, and Shine Linn-Thant

---
class: overlay
background-image: url(assets/dera-nd.jpg)

# Agenda

.cul[
1. Goals Review .right[**Shine**]
2. Why Tourists are Visiting .right[**Jonathan**]
3. How Tourists are Planning .right[**Olivia**]
4. Tourist Interest in Yokai Pop Culture .right[**Shine**]
5. EFFURT Data .right[**Brandon**]
6. The Tourist Map Draft .right[**Olivia**]
7. Preliminary Recommendations .right[**Brandon**]
]

.bottom-bar[
Background Image: Albert Dera via [Unsplash](https://unsplash.com/photos/3A4YNbpn9Sc)
]

---
class: overlay
background-image: url(assets/dera-nd.jpg)

# Our .pop[Goals]

.row.center-v[
.col-6[
.responsive.pic[![](assets/yokai-soho-nd.jpg)]
]

.col-6.cul[
1. **Survey** Activities in Yokai Street
2. **Map** Activities in Yokai Street
3. **Recommend** Tourism Improvements
]
]

.bottom-bar[
Image: https://yokaisoho.com/building/ | Background Image: Albert Dera via [Unsplash](https://unsplash.com/photos/3A4YNbpn9Sc)
]

---

# Why tourists are .pop[Visiting]

.row.center-v[
.col-6[
.responsive.pic[![](assets/Japan_visit_piechart.png)]
]
.col-6[
.responsive.pic[![](assets/Kyoto_visit_piechart.png)]
]
]

---

# How tourists are .pop[Planning]

.row.center-v[
.col-6[
.responsive.pic[![](assets/planning_resources_piechart.png)]
]
.col-6[
.responsive.pic[![](assets/online-dot-matrix.png)]
]
]

---

# Tourist Interest in Yokai .pop[Pop Culture]

.responsive-height.center-h[![](assets/pop_culture_interest.png)]

---
class: impact

# The State of .pop[EFFURT]

---

<iframe src="https://urban-regen.alicorn.io/effurt" style="position: absolute; top: 0; left: 0; display:block; width: 100%; height: 93%;" frameborder="0"></iframe>

.bottom-bar[
Data via https://urban-regen.alicorn.io/effurt
]

---
class: impact

# The .pop[Tourist Map] Draft

---
class: overlay
background-image: url(assets/dera-nd.jpg)

# Preliminary .pop[Recommendations]

Recommendation | Justification
---|---
Accssible Map Board | Make Yokai Street more navigable.
Clearer Entrance | Make Yokai Street more welcoming.
Deeper Brand Integration | Make Yokai Street more experiential.
Brand Merchandising | Make Yokai Street more profitable.

.bottom-bar[
Background Image: Albert Dera via [Unsplash](https://unsplash.com/photos/3A4YNbpn9Sc)
]

---
class: impact

# Q&A