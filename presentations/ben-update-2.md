class: animation-fade
layout: true

.bottom-bar[
  Sprint Review
]

---
class: impact

# Sprint Review
Brandon Sanders, Jonathan Lee, Olivia Hunker, and Shine Linn-Thant

---
class: overlay
background-image: url(assets/dera-nd.jpg)

# Agenda

.cul[
1. Review of Goals and Stakeholders .right[**All**]
2. Online Reviews of Yokai Street .right[**Jonathan**]
3. Ongoing Research .right[**Olivia**]
4. Updated Building Forms .right[**Shine**]
5. EFFURT MSI #2 .right[**Brandon**]
6. Physical Maps .right[**All**]
]

---
class: overlay
background-image: url(assets/dera-nd.jpg)

.col-6[
## Goals
.cul[
1. **Survey** experiences `< 800m` from Yokai-SOHO.
2. **Create** a map of experiences.
3. **Recommend** tourism improvements.
]   
]

.col-6[
## Stakeholders
.cul[
1. Residents
2. Tourists
3. Government
4. "Daily" Shops
5. "Experiential" Shops
]   
]

---
class: overlay
background-image: url(assets/dera-nd.jpg)

# Online .pop[Reviews] of Yokai Street

.col-6[
.cul[
- Visibility
- Attractions
- Conclusions
]
]

.col-6[
.responsive-width[![](assets/arnold-nd.jpg)]
]

---
class: overlay
background-image: url(assets/dera-nd.jpg)

# Ongoing .pop[Research]

.cul[
- The "Experience Economy"
- Methods for assessing experiences
- New internal workflow for distilling findings
]

---
class: overlay
background-image: url(assets/dera-nd.jpg)

# Updated Building .pop[Form] (part 1)

Field | Purpose
---|---
Visit Time | Did the visit occur during opening hours?
In Use | Are there clusters of unused buildings?
Primary Asset | What kind of building is it?
Hours | What hours is the building open?
Clear Purpose | Are buildings purposes clear to non-locals?
Clear English Service | Are buildings catering to foreigners?

---
class: overlay
background-image: url(assets/dera-nd.jpg)

# Updated Building .pop[Form] (part 2)

Field | Purpose
---|---
Franchised | Are franchises supporting development in the area?
Traffic | Are buildings attracting visitors?
Aesthetic Condition | Are buildings being maintained?
Transit Proximity | Are buildings easily accessible?
Experience | Does the building offer an "Experience"?
Sense of Place | Does the building conform to any local themes?

---

<iframe src="https://urban-regen.alicorn.io/effurt" style="position: absolute; top: 0; left: 0; display:block; width: 100%; height: 93%;" frameborder="0"></iframe>

.bottom-bar[
https://urban-regen.alicorn.io/effurt
]

---
class: impact

# New .pop[Maps]