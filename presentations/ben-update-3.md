class: animation-fade
layout: true

.bottom-bar[
  Sprint Review
]

---
class: impact

# Sprint Review
Brandon Sanders, Jonathan Lee, Olivia Hunker, and Shine Linn-Thant

---
class: overlay
background-image: url(assets/dera-nd.jpg)

# Agenda

.cul[
1. Online Reviews of Yokai Street .right[**Olivia**]
2. Local Surveying Status Updates .right[**Jonathan**]
3. Stakeholders Improving Tourism .right[**Shine**]
4. Critical Path .right[**Brandon**]
]

---
# Online .pop[Reviews] of Yokai Street

.responsive-width[![](assets/online-pro-con.png)]

---
# Online .pop[Reviews] of Yokai Street

.responsive-width[![](assets/online-dot-matrix.png)]

---
# Online .pop[Reviews] of Yokai Street

.responsive-width[![](assets/local-dot-matrix.png)]

---
class: overlay
background-image: url(assets/dera-nd.jpg)

# Local .pop[Surveying] Updates

.cul[
- Cafe Frosh English Night
- Nishiki Market
- Teramachi
- [Experience assessment form completed](https://docs.google.com/forms/d/1tHGnqfLdb8I0S25K2U3hEnNXZhRt3HObbYSNfRscTgM/edit)
- Creating Japanese survey for residents in Yokai Street
- [Creating survey to guage tourist interest in Yokai Street](https://docs.google.com/document/d/10osvLdEZZeK-BKYkb6NHU_3UGncCKocmPfl-hIHLoyw/edit)
- **Collaborate with bike tours to guage tourist interests?**
]

---
class: overlay
background-image: url(assets/dera-nd.jpg)

# .pop[Stakeholders] Improving Tourism

Stakeholder | Biggest Role
---|---
Tourists | Understand local culture and customs.
Residents | Collaborate with stakeholders.
Government | Support investors and tourists.
Experience Shops | Provide mutlilingual service.
Retail Shops | Integrate with Shotengai themes.
Investors | Revitalize and develop public spaces.

---
# Critical Path

.responsive[![](assets/critical-path-1.png)]
